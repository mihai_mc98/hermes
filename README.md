# Project: Hermes

A volunteering project implementing a new User Interface for the Centre of International Relations of The University Centre of Pitești

## Build Process

### Requirements

The project relies on the following dependencies:
* Java 8
* Maven 3.8.1
* Spring Boot 2.5.0

### Local development

``` sh
    mvn clean install
    echo "Run IntelliJ IDEA Build"
```

### Production Release

``` sh
    mvn clean package -Pprod
```

## Webapp Server Deployment

``` sh
    ./deploy.sh <username@domain>
```

