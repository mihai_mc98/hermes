#!/bin/bash

set -e

# Command-line input
SERVER_USERNAME=$1

SERVER_PASSWORD=$(security find-generic-password -w -s 'International Relations Server' -a "${SERVER_USERNAME}")
TOMCAT_PATH="/var/lib/tomcat9/webapps"

# FIXME: Fix deployment on Erasmus application
# shellcheck disable=SC2087
ssh -t "${SERVER_USERNAME}" << EOF
   tmux kill-session -t erasmus-application
   tmux new-session -t erasmus-application -d
   tmux send-keys -t erasmus-application "export HISTIGNORE='*sudo -S*'" C-m
   tmux send-keys -t erasmus-application "cd /home/inter/upit_erasmus" C-m
   tmux send-keys -t erasmus-application "echo ${SERVER_PASSWORD} | sudo -S -v" C-m
   tmux send-keys -t erasmus-application "clear" C-m
   tmux send-keys -t erasmus-application "history -c" C-m
   tmux send-keys -t erasmus-application "sudo php artisan serve --host=international.upit.ro --port=8816" C-m
EOF

# Deployment
mvn clean package -Pprod
scp target/Hermes.war "${SERVER_USERNAME}":~/
ssh -t "${SERVER_USERNAME}" "echo ${SERVER_PASSWORD} | sudo -S systemctl stop tomcat9.service"
ssh -t "${SERVER_USERNAME}" "echo ${SERVER_PASSWORD} | sudo -S rm -rf ${TOMCAT_PATH}/Hermes* ${TOMCAT_PATH}/ROOT*"
ssh -t "${SERVER_USERNAME}" "echo ${SERVER_PASSWORD} | sudo -S mv ~/Hermes.war ${TOMCAT_PATH}"
ssh -t "${SERVER_USERNAME}" "echo ${SERVER_PASSWORD} | sudo -S systemctl start tomcat9.service"