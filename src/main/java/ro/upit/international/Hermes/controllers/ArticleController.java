package ro.upit.international.Hermes.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ro.upit.international.Hermes.dao.page.PageService;
import ro.upit.international.Hermes.entities.Article;
import ro.upit.international.Hermes.entities.Page;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import ro.upit.international.Hermes.utils.Locales;

import static ro.upit.international.Hermes.utils.Locales.LOCALES_MAP;

// NOTE: These should be removed once the database has been created
import org.springframework.core.env.Environment;
import ro.upit.international.Hermes.utils.PageConfigHandler;

import static ro.upit.international.Hermes.dao.InitialDataLoader.loadPage;
import static ro.upit.international.Hermes.dao.InitialDataLoader.DATABASE_PATH;

@Controller
public class ArticleController {

    @Autowired
    private PageService page_service;

    // NOTE: This should be removed once the database has been created
    @Autowired
    private Environment environment;

    // FIXME – BIG ISSUE – This is NOT synchronised with anything else !!! – TEMPORARY FIX !!!
    @GetMapping(value = {"", "/about-us/**", "/admissions/**",
                         "/campus/**", "/collaborations/**", "/courses/**",
                         "/erasmus/**",
                         "/student-life/**"})
    public String admissionChildren(HttpServletRequest request, @NonNull Model model) throws JsonProcessingException {

        // FIXME – Once the testing is over, link this into one single command !
        // Find Page

        // Remove trailing slash
        String permalink = request.getServletPath().replaceAll("/$", "");

        // NOTE: Hot Reload for development purposes
        Page page;
        if(Arrays.asList(environment.getActiveProfiles()).contains("dev"))
            page = loadPage(DATABASE_PATH.resolve(permalink.replaceAll("^/", "")));
        else
            page = page_service.findByPermalink(permalink);
        
        // Deserialize the options and add them to the model
        JsonNode options = PageConfigHandler.mapper.readValue(page.getOptions(), new TypeReference<>() {});
        for (Iterator<String> it = options.fieldNames(); it.hasNext(); ) {
            String field = it.next();
            model.addAttribute(field, options.get(field));
        }

        // Get Article in `locale`'s language (if available)
        Locale locale = LOCALES_MAP.getOrDefault(LocaleContextHolder.getLocale().getLanguage(), Locales.DEFAULT_LOCALE);
        Article article = page.getOneArticle(locale);

        // Handle Cookie consent
        Cookie consent_cookie = new Cookie("cookie_consent", "not-accepted");
        try {
            for(Cookie cookie: request.getCookies())
                if(cookie.getName().equals(consent_cookie.getName())) {
                    consent_cookie = cookie;
                    break;
                }
        }
        catch (NullPointerException ignored) {} // NOTE: For first-ever visitors, the cookies Array is null

        model.addAttribute(consent_cookie.getName(), consent_cookie);
        model.addAttribute("article", article);
        return "article";
    }
}
