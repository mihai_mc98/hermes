package ro.upit.international.Hermes.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@ControllerAdvice
public class GlobalController {

    @ModelAttribute("menus")
    @SneakyThrows
    public LinkedHashMap<String, Map.Entry<String, LinkedHashMap<String, String>>> headerNavigationMenus() {
        return new ObjectMapper().readValue(ArticleController.class.getResource("/config/NavigationMenus.json"),
                new TypeReference<>() {});
    }
}
