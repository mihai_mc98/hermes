package ro.upit.international.Hermes.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HermesErrorController implements ErrorController {

    @GetMapping(value = "/error")
    public String errorPage() {
        return "error";
    }
}
