package ro.upit.international.Hermes.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ro.upit.international.Hermes.dao.article.ArticleService;
import ro.upit.international.Hermes.dao.page.PageService;
import ro.upit.international.Hermes.entities.Article;
import ro.upit.international.Hermes.entities.Page;
import ro.upit.international.Hermes.utils.HermesConstants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static ro.upit.international.Hermes.utils.Locales.LOCALES_MAP;
import static ro.upit.international.Hermes.entities.Page.ROOT_PAGE;
import static ro.upit.international.Hermes.utils.PageConfigHandler.loadPageConfig;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private PageService pageService;

    @Autowired
    private ArticleService articleService;

    private static final ObjectMapper mapper = new ObjectMapper();

    public static final Path DATABASE_PATH = Path.of(InitialDataLoader.class.getResource("/pages").getPath());

    public static Page loadPage(Path path) {

        // Handle ROOT_PAGE
        Page page = path.equals(DATABASE_PATH) ? ROOT_PAGE : new Page();

        // Set Slug
        String[] slugs = path.toString().split("/");
        page.setSlug(slugs[slugs.length - 1]);

        // Load Config
        JsonNode config = loadPageConfig(path.resolve("config.json"));

        // Set Languages
        Map<String, String> titles = mapper.convertValue(config.get("title"), new TypeReference<>(){});
        titles.forEach((locale_lang, title) -> page.setOneArticle(
            LOCALES_MAP.get(locale_lang),
            new Article(LOCALES_MAP.get(locale_lang), title, HermesConstants.read(path.resolve(locale_lang + ".md")))
        ));

        // Set Options (as serialized String)
        JsonNode options = config.get("options");
        page.setOptions(options.toString());

        return page;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        try {
            TreeMap<Path, Page> pageMap = new TreeMap<>();
            Files.find(DATABASE_PATH,
                       Integer.MAX_VALUE,
                       (file, file_attr) -> file_attr.isDirectory())
                 .forEach(path -> {
                         Page page = loadPage(path);

                         if(!path.equals(DATABASE_PATH)) // Set Relationships
                             pageMap.get(path.getParent()).setOneChild(page);

                         pageMap.put(path, page);
                     }
                 );
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new Error("ERROR – Something happened while trying to load the directory database !");
        }

        /** Save the {@link ROOT_PAGE} and all its children */
        pageService.save(ROOT_PAGE);

        // FIXME – Delete this once you know that everything is set up
        // FIXME – Maybe it's time to have some tests too ?!?!
        ArrayList<Page> all_pages = new ArrayList<>();
        pageService.findAll().forEach(all_pages::add);
    }
}
