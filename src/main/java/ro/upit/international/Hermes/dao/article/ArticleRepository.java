package ro.upit.international.Hermes.dao.article;

import org.springframework.data.repository.CrudRepository;
import ro.upit.international.Hermes.entities.Article;

public interface ArticleRepository extends CrudRepository<Article, Long> {

    // Some queries here

}
