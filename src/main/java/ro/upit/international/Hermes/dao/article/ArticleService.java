package ro.upit.international.Hermes.dao.article;

import ro.upit.international.Hermes.entities.Article;

import java.util.Optional;

public interface ArticleService {

    public long count();

    public Iterable<Article> findAll();

    public <A extends Article> A save(A article);

    public void deleteById(long id);

    public Optional<Article> findOne(long id);

}
