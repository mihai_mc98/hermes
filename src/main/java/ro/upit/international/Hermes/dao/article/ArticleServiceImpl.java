package ro.upit.international.Hermes.dao.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upit.international.Hermes.entities.Article;

import java.util.Optional;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository repository;

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public Iterable<Article> findAll() {
        return repository.findAll();
    }

    @Override
    public <A extends Article> A save(A article) {
        return repository.save(article);
    }

    // FIXME – this should be handled properly to avoid deleting a whole range of things !
    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Article> findOne(long id) {
        return repository.findById(id);
    }
}
