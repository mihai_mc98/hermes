package ro.upit.international.Hermes.dao.page;

import org.springframework.data.repository.CrudRepository;
import ro.upit.international.Hermes.entities.Page;

public interface PageRepository extends CrudRepository<Page, Long> {

    // Some queries here
    Page findFirstByPermalinkOrderByPermalinkAsc(String permalink);

}
