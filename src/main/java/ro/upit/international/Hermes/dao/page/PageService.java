package ro.upit.international.Hermes.dao.page;

import ro.upit.international.Hermes.entities.Page;

import java.util.Optional;

public interface PageService {

    public long count();

    public Iterable<Page> findAll();

    public <P extends Page> P save(P page);

    public void deleteById(long id);

    public Optional<Page> findOne(long id);

    public Page findByPermalink(String permalink);
}
