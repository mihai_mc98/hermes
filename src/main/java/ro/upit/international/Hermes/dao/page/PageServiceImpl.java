package ro.upit.international.Hermes.dao.page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.upit.international.Hermes.entities.Page;

import java.util.Optional;

@Service
public class PageServiceImpl implements PageService {

    // FIXME – why is field injection not recommended ??
    @Autowired
    private PageRepository repository;

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public Iterable<Page> findAll() {
        return repository.findAll();
    }

    @Override
    public <P extends Page> P save(P page) {
        return repository.save(page);
    }

    // FIXME – this should be handled properly to avoid deleting a whole range of things !
    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Page> findOne(long id) {
        return repository.findById(id);
    }

    // FIXME – Need to handle how to search for a certain page from link (controller-wise)
    @Override
    public Page findByPermalink(String permalink) {
        return repository.findFirstByPermalinkOrderByPermalinkAsc(permalink);
    }
}
