package ro.upit.international.Hermes.entities;

import lombok.*;
import ro.upit.international.Hermes.utils.HermesConstants;

import javax.persistence.*;
import java.util.Locale;
import java.util.Map;
import ro.upit.international.Hermes.utils.Locales;

import static ro.upit.international.Hermes.entities.Page.ROOT_PAGE;
import static ro.upit.international.Hermes.utils.Locales.LOCALES;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Article {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;

    @NonNull
    private String title = "";

    private final static Map<Locale, String> default_titles = generateDefaultTitles();

    @NonNull
    @Lob
    private String content = "";

    private final static Map<Locale, String> default_content = generateDefaultContent();

    @NonNull
    @Lob
    @Setter(value = AccessLevel.NONE) /**  NOTE – This field is derived from {@link content} */
    private String content_html = "";

    @NonNull
    @Setter
    @Getter
    private Locale locale = Locales.DEFAULT_LOCALE;

    /**
     *   NOTE: The following properties should set by the {@link owner} parent
     */
    @NonNull
    @Setter(AccessLevel.PACKAGE)
    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private Page owner = ROOT_PAGE;

    public Article(Locale locale) {
        this.locale = locale;
        setTitle(this.title);
        setContent(this.content);
    }

    public Article(Locale locale, String title, String content) {
        this.locale = locale;
        setTitle(title);
        setContent(content);
    }

    public void setTitle(String title) {
        if(title == null || title.isEmpty())
            this.title = default_titles.get(this.locale);
        else
            this.title = title;
    }

    // NOTE: Whenever the content is changed, the HTML cache is updated as well.
    public void setContent(String content) {

       if(content == null || content.isEmpty())
           this.content = default_content.get(this.locale);
       else
           this.content = content;

       // Update the HTML page
       this.content_html = HermesConstants.processMarkdown(this.content);
    }

    private static Map<Locale, String> generateDefaultTitles() {
        Map<Locale, String> title_placeholders = Map.of(
            Locales.ENGLISH, "Untitled Article",
            Locales.FRENCH, "Article sans titre",
            Locales.ROMANIAN, "Articol fără titlu"
        );

        // Synchronise condition
        if(title_placeholders.size() != LOCALES.size())
            throw new Error("FATAL ERROR – Not all languages have a default title !");

        return title_placeholders;
    }

    private static Map<Locale, String> generateDefaultContent() {
        Map<Locale, String> content_placeholder = Map.of(
            Locales.ENGLISH, "This page is empty for now. Please switch the language to [French](?lang=fr) or [Romanian](?lang=ro).",
            Locales.FRENCH, "Cet espace est couramment vide. Veuillez changer la langue d'affichage en [anglais](?lang=en) ou [roumaine](?lang=ro).",
            Locales.ROMANIAN, "Pagina este în curs de actualizare. Vă rugăm să vizualizați pagina în [engleză](?lang=en) sau în [franceză](?lang=fr)."
        );

        // Synchronise condition
        if(content_placeholder.size() != LOCALES.size())
            throw new Error("FATAL ERROR – Not all languages have a default content placeholder !");

        return content_placeholder;
    }
}
