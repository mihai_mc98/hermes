package ro.upit.international.Hermes.entities;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

import static ro.upit.international.Hermes.utils.Locales.LOCALES;

@Getter
@Entity
public class Page {

    @ManyToOne
    public static final Page ROOT_PAGE = new Page("");

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
    @MapKey(name = "locale")
    @NonNull
    private Map<Locale, Article> page;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Page parent;

    // FIXME – The CascadeType must be checked !
    @OneToMany(mappedBy = "parent", cascade = CascadeType.PERSIST)
    private List<Page> children;

    @NonNull
    private String slug;

    @NonNull // FIXME – it's annoying having to call setPermalink ON EVERY OBJECT !!! Also needs to be hyphenated !!!
    // NOTE – See how the `Article` has defined the `content_html` !
    private String permalink;

    @Setter
    @Lob
    private String options;

    // FIXME: Think about what happens when updating a permalink/slug and the resulting "new" slug/permalink clashes with an existing one !

    /*
        Constructors
     */

    public Page() {
        this.slug = "untitled-article"; // FIXME – Slugs cannot contain trailing "/" !
        this.parent = ROOT_PAGE;
        this.children = new ArrayList<>();

        setPage(new HashMap<>() {{
            for(Locale locale: LOCALES)
                put(locale, new Article(locale));
        }});
    }

    // NOTE – THIS CONSTRUCTOR SHOULD NEVER BE CALLED EXCEPT FOR WHEN CREATING THE `ROOT_PAGE`
    private Page(String slug) {
        // FIXME – The ROOT's article is the homepage !!!
        this.slug = slug;
        this.permalink = this.slug;
        this.parent = null; // NOTE: This is the top of the tree. There is nowhere next to go up
        this.page = new HashMap<>();
        this.children = new ArrayList<>();
    }

    // NOTE: Whenever the slug is changed, the permalink is updated as well.
    public void setSlug(String slug) {
        if(!this.slug.equals(slug)) {
            this.slug = slug;
            setPermalink();
        }
    }

    // NOTE: Whenever the parent is changed, the permalink is updated as well.
    public void setParent(Page parent) {
        if(this.parent != parent) {
            this.parent = parent;
            setPermalink();
        }
    }

    /*
        Recursively update all permalink's

        NOTE: ROOT_PAGE.permalink must never be updated !
     */
    private void setPermalink() {
        // FIXME – Needs a better way of handling this, honestly!
        if(this != ROOT_PAGE)
            this.permalink = this.parent.permalink + "/" + this.slug;

        for(Page child: children)
            child.setPermalink();
    }


    // FIXME – Tree-like CRUD operations – what happens ?!

    /*
     *  ARTICLES
     *    - get/set all articles
     *    - get/set one specific articles
     *
     *  Setter methods also set the slave.master relationships and locale
     */

    public void setPage(HashMap<Locale, Article> page) {
        this.page = page;
        for (Locale locale : page.keySet()) {
            Article article = page.get(locale);
            article.setOwner(this);
        }
    }

    public Map<Locale, Article> getPage() {
        return page;
    }

    public void setOneArticle(Locale locale, Article newArticle) {
        page.put(locale, newArticle);
        newArticle.setOwner(this);
    }

    public Article getOneArticle(Locale locale) {
        return page.get(locale);
    }

    /*
     *  Children `Page`'s
     *    - get/set all children
     *    - get/set one specific child
     *
     *  Setter methods also sets the slave.master relationship
     */

    public void setChildren(ArrayList<Page> children) {
        this.children = children;
        for (Page child : children)
            child.setParent(this);
    }

    public List<Page> getChildren() {
        return children;
    }

    public void setOneChild(Page newPage) {
        // FIXME – slugs should be unique for different children of the same parent !!!
        children.add(newPage);
        newPage.setParent(this);
    }

    public Page getOneChild(int i) {
        return children.get(i);
    }

}
