package ro.upit.international.Hermes.utils;

import org.commonmark.Extension;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public final class HermesConstants {

    public static String read(Path path) {
        String content = "";
        try {
            content = Files.readString(path);
        } catch (IOException ignored) {} // NOTE: Files will be missing if no translation was available

        return content;
    }

    private static void assign_header_ids(Element document) {
        Elements headers = new Elements();
        headers.addAll(document.select("h1,h2,h3,h4,h5,h6"));

        HashSet<String> ids = new HashSet<>(headers.size());
        for(Element header: headers) {
            String header_id = header.text().replaceAll(" ", "-");

            if(!ids.add(header_id)) {  // Resolve duplicates
                int duplicate_count = 1;
                while(!ids.add(header_id + "-" + duplicate_count))
                    duplicate_count++;

                header_id = header_id + "-" + duplicate_count;
            }

            header.id(header_id);
        }
    }

    private static void table_of_contents(Element document) {

        // Check that the tag exists
        Element toc_tag = document.getElementsByTag("table-of-contents").first();
        if(toc_tag == null || toc_tag.parent() == null)
            return;

        // Header Lang
        String lang = toc_tag.attr("lang");

        // Header Depth
        int depth = Math.max(1, Math.min(3, Integer.parseInt(toc_tag.attr("depth"))));
        StringBuilder header_select = new StringBuilder("body > h1");
        for(int i = 2; i <= depth; i++)
            header_select.append(String.format(", body > h%d", i));

        // Get all relevant headers within `depth`
        Elements headers = new Elements();
        headers.addAll(document.select(header_select.toString()));

        // Headers to Markdown
        // FIXME – See if there is a way to get as a Thymeleaf Message !
        Map<String, String> header_title_variants = Map.of(
                "ro", "Conținut",
                "fr", "Contenu",
                "en", "Contents"
        );
        StringBuilder markdown = new StringBuilder("### " + header_title_variants.get(lang) + "\n");
        for(Element header : headers) {
            String header_tag = header.tagName();

            String fString = ""; // NOTE: This is a hack, but it works, so I'm happy ! :)
            switch (header_tag) {
                case "h1": fString = "1. ";
                           break;
                case "h2": fString = "\t* ";
                           break;
                case "h3": fString = "\t\t- ";
                           break;
            }

            markdown.append(String.format(fString + "[%s](#%s)\n", header.text(), header.id()));
        }

        // Insert Table of Contents
        Elements parsed = Jsoup.parse(markdownToHTML(markdown.toString())).body().children();
        toc_tag = toc_tag.parent(); // Handles trailing <p> . . . </p> creation
        for(Element e: parsed)
            toc_tag.before(e);
        toc_tag.remove();
    }

    private static void encapsulate_table(Element document) {
        Elements tables = document.select("body > table");
        for(Element table: tables) {
            Element table_wrapper = new Element("div");
            table_wrapper.addClass("table_wrapper");

            // Add the `table_swiper`
            Element table_swiper = new Element("div");
            table_swiper.addClass("table_swiper");
            table_swiper.html("<i class=\"fa-solid fa-arrows-left-right\"></i>");

            // Encapsulate the table
            table.replaceWith(table_wrapper); // NOTE: The order matters !
            table_wrapper.appendChild(table_swiper);
            table_wrapper.appendChild(table);
        }
    }

    public static String markdownToHTML(String markdown) {

        List<Extension> extensions = List.of(TablesExtension.create());

        Parser parser = Parser
                .builder()
                .extensions(extensions)
                .build();

        Node parsedMarkdown = parser.parse(markdown);

        HtmlRenderer renderer = HtmlRenderer
                .builder()
                .extensions(extensions)
                .build();

        return renderer.render(parsedMarkdown);
    }

    public static String processMarkdown(String markdown) {
        String raw_html = markdownToHTML(markdown);

        // Parse to JSoup
        Element document = Jsoup.parse(raw_html).body();
        assign_header_ids(document);
        table_of_contents(document);
        encapsulate_table(document);

        // Only return the body.innerHTML
        return document.html();
    }

    private HermesConstants() {
        throw new UnsupportedOperationException("This class must NEVER be instantiated !");
    }
}
