package ro.upit.international.Hermes.utils;

import java.util.*;

public class Locales {
    public static Locale ENGLISH = Locale.ENGLISH;
    public static Locale FRENCH = Locale.FRENCH;
    public static Locale ROMANIAN = new Locale("ro", "");

    // Single point of control
    public static Locale DEFAULT_LOCALE = ENGLISH;

    public static final HashSet<Locale> LOCALES = new HashSet<>(Arrays.asList(ENGLISH, FRENCH, ROMANIAN));

    public static final Map<String, Locale> LOCALES_MAP = new HashMap<>() {{
       for(Locale locale: LOCALES)
           put(locale.getLanguage(), locale);
    }};
}
