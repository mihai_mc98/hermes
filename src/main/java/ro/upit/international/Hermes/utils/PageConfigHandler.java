package ro.upit.international.Hermes.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.nio.file.Path;
import java.util.Iterator;

public final class PageConfigHandler {

    public static final ObjectMapper mapper = new ObjectMapper();

    private static final Path default_config_path = Path.of(PageConfigHandler.class.getResource("/config/config.default.json").getPath());

    private static final JsonNode default_config = loadPageConfig(default_config_path);

    private static JsonNode setDefaults(JsonNode node, JsonNode default_node) {

        Iterator<String> default_fields_it = default_node.fieldNames();
        while(default_fields_it.hasNext()) {
            String key = default_fields_it.next();
            JsonNode default_value = default_node.get(key);
            JsonNode node_value = node.get(key);

            // If the key does not exist at all, all the whole Entry<String, JsonNode>
            if(node_value == null)
                ((ObjectNode) node).set(key, default_value);
            else
                setDefaults(node_value, default_value);
        }

        return node;
    }

    public static JsonNode loadPageConfig(Path path) {
        JsonNode config;
        try {
            config = mapper.readValue(HermesConstants.read(path), new TypeReference<>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new Error("FATAL ERROR – Could not load from JSON at Path: " + path); // FIXME – NEVER DO THIS !!!
        }

        if(path.equals(default_config_path))
            return config; // There is no need to handle the Default config
        else
            return setDefaults(config, default_config);
    }

    private PageConfigHandler() {
        throw new UnsupportedOperationException("This class must NEVER be instantiated !");
    }
}
