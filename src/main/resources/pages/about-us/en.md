# Welcome to CUPIT !

Founded in 1962, The University Centre of Pitești is an important pillar of the Romanian Higher Education system, as well as a globally-connected university centre.

We are part of the National University of Science and Technology POLITEHNICA Bucharest (UNSTPB), an Accredited Public University with a **High Confidence** ranking awarded by <abbr title="Romanian Agency for Quality Assurance in Higher Education">ARACIS</abbr> – the **gold standard** in Romanian Higher Education.

Our wide pool of Degrees as well as our strategic geographical position (less than 100 km away from the Capital), have allowed us to grow and develop our services for over 8500 students.

# Comprehensive Academic Research Programmes

At The University Centre of Pitești, we bring together Academic Research with Science and Technology through:

* **100+** training programmes for various specialisations and degrees
* **6** Faculties
* **4** Doctoral Schools
* **18** Research Centres, including a Research-Development-Innovation and Technological Transfer Centre

# International Cooperation

Our partnerships with businesses and Public Institutions provide our students with excellent career and personal development opportunities. Having a long-standing tradition of welcoming students from over 30 countries, we are highly-committed to further developing international exchanges and mobilities.

We support international students all the way: from applying with us to arriving in the City of Pitești.