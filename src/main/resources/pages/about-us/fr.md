# Bienvenue au CUPIT !

Fondé en 1962, le Centre Universitaire de Pitești est un pilier important du système d’enseignement supérieur roumain, ainsi qu’un centre universitaire connecté au niveau global.

Nous faisons partie de l’Université Nationale de Science et Technologie POLITEHNICA Bucarest (UNSTPB), un établissement public accrédité avec la mention **Haute Confiance** par <abbr title="L’Agence Roumaine de l’Assurance de la Qualité dans l’Enseignement Supérieur">ARACIS</abbr> – la **plus haute mention** dans l’enseignement supérieur roumain.

Notre offre diversifiée de formations et notre position géographique stratégique (moins de 100 km de distance par rapport à la capitale) nous ont permis de nous développer et d’offrir nos services à plus de 8500 étudiants.

# Diversité des programmes de recherche scientifique

Au Centre Universitaire de Pitești, nous associons la recherche académique à la technologie par :

* **100+** programmes de formation dans diverses spécialisations et à différents niveaux
* **6** facultés
* **4** écoles doctorales
* **18** centres de recherche, dont le Centre de Recherche-Développement-Innovation et Transfert Technologique

# Coopération Internationale

Nos partenariats avec des entreprises et des institutions publiques offrent à nos étudiants d’excellentes opportunités de carrière et de développement personnel. Avec une longue tradition d’accueil des étudiants provenant de plus de 30 pays, nous sommes impliqués dans le développement continu de nos mobilités et échanges internationaux. Nous soutenons nos étudiants internationaux dans tout leur parcours : depuis leur demande d’admission jusqu’à leur arrivée dans la ville de Pitești.