# Bine ați venit la CUPIT !

Fondat în 1962, Centrul Universitar Pitești este nu numai un reper important în cadrul sistemului românesc de învățământ superior, dar și un centru universitar conectat pe plan internațional.

Facem parte din Universitatea Națională de Știință și Tehnologie POLITEHNICA București (UNSTPB), o universitate de stat acreditată cu calificativul national **maxim** ("**încredere ridicată**") de către <abbr title="Agenția Națională de Asigurare a Calității pentru Învățământul Superior">ARACIS</abbr>.

Gama largă de programme de studii, precum și poziționarea strategică din punct de vedere geographic (la aproximativ 100 km de capitala României), ne-au permis să ne dezvoltăm pentru comunitatea de peste 8500 de studenți care aleg să-și modeleze viitorul alături de noi.

# Educație și Cercetare integrate

La Centrul Universitar Pitești, educația, cercetarea, știința și tehnologia se îmbină armonios prin:

* **100+** de programe de studii, atât pentru licență, cât și pentru master
* **6** facultăți
* **4** Școli Doctorale
* **18** centre de cercetare, inclusiv un Centru Regional de Cercetare-Dezvoltare pentru Materiale, Procese și Produse Inovative Destinate Industriei de Automobile (Crc&D-Auto)

# Cooperare Internațională

Parteneriatele noastre cu instituții publice, dar și cu firme private oferă studenților oportunități excelente de dezvoltare personală și profesională.

Cu o lungă tradiție de pregătire a studenților străini din peste 30 de țări, UPIT este implicată în tot ceea ce înseamnă schimburi și mobilități internaționale. Susținem demersurile studenților internaționali pe tot parcursul aplicației, de la momentul în care aceștia candidează în cadrul UPIT și până când ajung în Pitești astfel încât să se integreze ușor în comunitatea academică.

