All <abbr title="European Union">EU</abbr>/<abbr title="European Economic Area">EEA</abbr>/<abbr title="Confœderatio Helvetica (Switzerland)">CH</abbr> citizens benefit from the same advantages as Romanian citizens. These include:

1. Paying the same tuition fees as a Romanian citizen
2. Foreign Degree Recognition

Regardless of degree, all EU candidates need to have their Study Documents recognised by <abbr title="National Center for the Recognition and Equivalence of Diplomas">NCRED</abbr>. This can be achieved:

* independently, by lodging a request on [NCRED's website](https://cnred.edu.ro/)
* via our University Centre, as part of the admission process

<span class="note">Note:</span> While both routes bear **identical** costs, we can only support candidates who applied for Degree Recognition via our University Centre.

<div class="boxes">
    <a href="/admissions/EU-students/recognition-dossier">Recognition Dossier</a>
    <a href="/admissions/EU-students/how-to-apply">How to Apply</a>
    <a href="/admissions/preparatory-year">Romanian Language Preparatory Year</a>
    <a href="/admissions/tuition-fees#Higher-Education-Degrees">Tuition Fees</a>
    <a href="/admissions/visa-and-residence-permit#EU-Citizens">Residence Registration</a>
    <a href="/admissions/EU-students/regulations">Regulations</a>
</div>

# Contact

<div class="paragraph">
Ms. LEMNARU Ana Cristina, International Students' Office Clerk

Phone: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>