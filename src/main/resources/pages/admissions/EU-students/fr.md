Les citoyens de <abbr title="l’Union Européenne">UE</abbr>/<abbr title="l’Espace  Économique Européen">SEE</abbr>/<abbr title="la Confédération Suisse">CH</abbr> bénéficient des mêmes avantages que les citoyens roumains, à savoir: 

1. Les mêmes frais de scolarité qu’un citoyen roumain 
2. La reconnaissance des diplômes étrangers 

Quelle que soit la spécialisation choisie, les étudiants doivent solliciter l’équivalence de leurs documents d’études au <abbr title="Centre National pour la Reconnaissance et l’Équivalence des Diplômes">CNRED</abbr>. Ces démarches peuvent être faites : 

* indépendamment, en déposant une demande sur le [site CNRED](https://cnred.edu.ro/)
* par l’intermédiaire de notre centre universitaire, en tant que partie du processus d’admission 

<span class="note">Note :</span> Même si les deux modalités impliquent des frais **identiques**, nous ne pouvons appuyer dans ces démarches que les candidats qui ont sollicité la reconnaissance de leurs diplômes par l’intermédiaire de notre centre universitaire. 

<div class="boxes">
    <a href="/admissions/EU-students/recognition-dossier">Dossier de reconnaissance </a>
    <a href="/admissions/EU-students/how-to-apply">Comment postuler ?</a>
    <a href="/admissions/preparatory-year">Année préparatoire de langue roumaine</a>
    <a href="/admissions/tuition-fees#Enseignement-supérieur">Frais de scolarité</a>
    <a href="/admissions/visa-and-residence-permit#Citoyens-de-l’UE">Enregistrement résidence</a>
    <a href="/admissions/EU-students/regulations">Réglementations</a>
</div>

# Contact

<div class="paragraph">
Mme. LEMNARU Ana Cristina, Référent du Bureau des étudiants internationaux 

Tél.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
