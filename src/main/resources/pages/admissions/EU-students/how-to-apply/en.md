# Apply for Study Documents Recognition

Candidates may submit the [Recognition Dossier](/admissions/EU-students/recognition-dossier), either:
- via e-mail, to: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
- in person or by post, to: **Str. Târgu din Vale, Nr. 1, Pitești, Argeș, 110040, România**

# Next Steps

If your Degree of choice is taught in Romanian, then you must enrol in the [Romanian Language Preparatory Year](/admissions/preparatory-year) first.

Upon having all Study Documents recognised, please [contact us](/admissions/EU-students#Contact) to apply for your Degree of choice.

## Residence Permit

Please visit the [Residence Registration](/admissions/visa-and-residence-permit#EU-Citizens) section for further information, guidance and support.

## Enrolment

Each Faculty might have its own enrolment procedures. Please [contact us](/admissions/EU-students#Contact) to find out more about enrolling at your Degree of choice.

### Location

The [International Student's Office](https://g.page/official_upit) is located at [Room I.33, Rectorate, Building B, 1 Târgu din Vale Street, Pitești, Argeș, 110440, România](https://g.page/official_upit).
