# Demande de la reconnaissance des études

Les candidats peuvent déposer le [Dossier de reconnaissance](/admissions/EU-students/recognition-dossier), soit :
- par courriel, à l’adresse : [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
- en se présentant eux-mêmes ou bien par la poste, à l’adresse : **Nº 1, Rue Târgu din Vale, Pitești, Argeș, 110040, România**

# Pas à suivre :

S’il s’agit d’un programme d’études dispensé en roumain, vous devez vous inscrire d’abord à l’[Année préparatoire de langue roumaine](/admissions/preparatory-year). 

Après la finalisation de la procédure de reconnaissance des études, veuillez [nous contacter](/admissions/EU-students#Contact) pour postuler au programme d’études choisi.

## Permis de séjour

Veuillez accéder à la section [Enregistrement résidence](/admissions/visa-and-residence-permit#Citoyens-de-l’UE) pour bénéficier de renseignements, encadrement et appui supplémentaire.

## Immatriculation

Chaque faculté dispose de ses propres procédures d’immatriculation des étudiants. Veuillez [nous contacter](/admissions/EU-students#Contact) pour des détails sur l’inscription au programme choisi. 

### Bureau

Le bureau des étudiants internationaux se trouve à la [Chambre I.33, Rectorat, Bâtiment B, Nº 1, Rue Târgu din Vale, Pitești, Argeș, 110440, Roumanie](https://g.page/official_upit).

