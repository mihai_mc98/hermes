# Cerere pentru recunoașterea studiilor

Candidații pot depune [Dosarul de recunoaștere](/admissions/EU-students/recognition-dossier), fie:
- prin e-mail, la adresa: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
- în persoană sau prin poștă, la adresa: **Str. Târgu din Vale nr. 1, Pitești, Argeș, 110040, România**

# Pașii următori

Dacă specializarea este predată în limba română, atunci trebuie să vă înscrieți mai întâi la [Anul pregătitor de limba română](/admissions/preparatory-year). 

După ce procedure de recunoaștere a studiilor se încheie, vă rugăm să [ne contactați](/admissions/EU-students#Contact) pentru a aplica pentru specializarea dorită.

## Permisul de ședere

Vă rugăm să accesați secțiunea [Înregistrare rezidență](/admissions/visa-and-residence-permit#Cetățeni-UE) pentru informații, îndrumare și sprijin suplimentar.

## Înmatriculare

Fiecare facultate are propriile sale proceduri de înmatriculare a studenților. Vă rugăm să [ne contactați](/admissions/EU-students#Contact) pentru a afla mai multe despre înscrierea la specializarea dorită.

### Locație

Biroul studenților internaționali este situat la [Camera I.33, Rectorat, Clădirea B, Str. Târgu din Vale nr. 1, Pitești, Argeș, 110440, România](https://g.page/official_upit).

