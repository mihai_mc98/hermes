The Recognition Dossier consists of a number of documents, grouped by type:

<table-of-contents depth="1" lang="en"></table-of-contents>

In the interest of avoiding delays, please double-check that all your documents adhere to the following guidelines **before** submitting your dossier.

If unsure of the requirements, please contact our [International Students' Office](/admissions#Contact).

# Identity Documents

Please include **scanned copies** after the following documents:

1. Passport
2. National Identity Card (if applicable)
3. Proof of Name Change (if applicable)

<span class="note">Note:</span> A **certified translation** into Romanian is needed if the Proof of Name Change is not in Romanian, English, French, Spanish, nor Italian.

# Study Documents

Please observe the <span class="note">different</span> requirements for each Study Document, depending on whether it had already been Recognised by NCRED or not.

## Already-Recognised Study Documents

Students who had already had their Study Documents recognised by NCRED need include:
1. Copies after all Recognised Studies
2. Copied after all Recognition Certificates

Otherwise, please read on to learn more about the NCRED Recognition.

## NCRED Recognition

<div class="info-box">

## What you need to do depends on the language in which the document was issued

* For documents issued in Romanian, French, English, Spanish or Italian, please include a <span class="note">scanned copy</span>
* For documents issued in any other languages, please include a <span class="note">certified translation</span> into Romanian

</div>

<span class="note-important">Important:</span> Please refer to the [List of Legalisation Requirements](https://www.upit.ro/_document/28154/lista_statelor_pentru_care_se_solicita_apostilarea_sau_supralegalizarea.pdf) to check if your Study Document must have **The Hague Apostille** or be **legalised**.

1. [Recognition Request Form](https://www.upit.ro/_document/113090/11a_cerere-recunoastere-studii-cetateni-europeni-admitere-licenta_2021_en.doc)
2. Study Document
3. Diploma Supplements (**only** if the Major is not mentioned on the Study Document)
4. Academic Records
5. All other relevant documents (if necessary)

<span class="note">Note:</span> For Degrees issued in the Republic of Moldova before 2008, an **Authenticity Certificate** issued by the Moldovan Ministry of Education is also required.

If your Study Document is for a regulated profession **and** was issued outside of the EU, please also include:

1. Full Description of all Courses in the Degree
2. All Course's Curriculum
3. Any relevant documents of graduate-level Studies

# NCRED Recognition Fees

The dossier processing fee is: **100 RON**.

This may be paid:

* in person, at the NCRED Cashier's Office
* in person, via Bank Payment Order or by Postal Order 
* online, via Bank Transfer

## Payment Details

All payment receipts must display the following features:

1. The IBAN of Ministry of Education
2. The Candidate's name
3. The name of Employer making the Recognition Request (if applicable)

### RON Account

| Category    | Payment Details                                                                      |
|-------------|--------------------------------------------------------------------------------------|
| Payee Name  | Ministerul Educației                                                                 |
| IBAN        | RO86 TREZ 7002 0E33 0500 XXXX                                                        |
| SWIFT       | TREZROBU                                                                             |
| Bank's Name | Activitatea de Trezorerie şi Contabilitate Publică a Municipiului Bucureşti - ATCPMB |
| TIN (CUI)   | 13729380                                                                             |

### EUR Account

<span class="note">Note:</span> Payments in EUR are converted to RON at the arrival date's <abbr title="National Bank of Romania">NBR</abbr> exchange rate.

| Category    | Payment Details               |
|-------------|-------------------------------|
| Payee Name  | Ministerul Educației          |
| IBAN        | RO35 RNCB 0080 0056 3030 0077 |
| SWIFT       | RNCBROBU                      |
| Bank's Name | Banca Comercială Română       |
| TIN (CUI)   | 13729380                      |
