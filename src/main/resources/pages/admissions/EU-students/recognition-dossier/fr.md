Le dossier de reconnaissance des études comprend plusieurs catégories de documents : 

<table-of-contents depth="1" lang="fr"></table-of-contents>

Pour éviter les retards, veuillez vous assurer que vos documents respectent les conditions suivantes **avant** d’envoyer le dossier. 

Pour adresser des questions, veuillez vous adresser au [Bureau des étudiants internationaux](/admissions#Contact).

# Pièces d’identité

Veuillez inclure des **copies scannées** des documents suivants :

1. Passeport
2. Carte d’identité (si c’est le cas)
3. La preuve du changement du nom (si c’est le cas)

<span class="note">Note :</span> Vous avez besoin d’une **traduction légalisée** en roumain si la preuve du changement du nom n’est pas en roumain, anglais, français, espagnol ou italien.

# Documents d’études

Veuillez prêter une attention particulière aux conditions <span class="note">différentes</span> prévues pour chaque document d’études, selon leur reconnaissance antérieure par le CNRED.

## Documents d’études déjà reconnus

Les étudiants dont les documents d’études ont déjà été reconnus par le CNRED doivent inclure :
1. Les copies de tous les documents d’études reconnus 
2. Les copies des attestations de reconnaissance

Dans le cas contraire, veuillez poursuivre la lecture pour vous renseigner sur la procédure de Reconnaissance par le CNRED.

## Reconnaissance par le CNRED

<div class="info-box">

## Attention ! Les conditions diffèrent selon la langue dans laquelle le document a été émis !  

* Pour les documents émis en roumain, français, anglais, espagnol ou italien, veuillez inclure une <span class="note">copie scannée</span>
* Pour les documents émis dans toute autre langue, veuillez inclure une <span class="note">traduction légalisée</span> en roumain

</div>

<span class="note-important">Important :</span> Veuillez consulter la [Liste de conditions de légalisation](https://www.upit.ro/_document/28154/liste_des_états_pour_lesquels_on_sollicite_l’apostille_ou_la_surlégalisation.pdf) pour vérifier si le document doit avoir l’**Apostille de la Haye** ou bien s’il doit être **surlégalisé**.

1. [Demande de reconnaissance des études](https://www.upit.ro/_document/113090/11a_demande-reconnaissance-études-citoyens-européens-admission-licence_2021_en.doc)
2. Diplôme d’études 
3. Le supplément au diplôme (**seulement** si la spécialisation n’est pas mentionnée sur le document d’études)
4. Les relevés de notes 
5. Autres documents pertinents (si c’est le cas).

<span class="note">Note :</span> Pour les diplômes délivrés par la République de Moldova avant 2008, il est nécessaire de présenter également une **Attestation d’authenticité** délivrée par le Ministère de l’Éducation de la République de Moldova.

Si le document d’études concerne une profession réglementée en Roumanie **et** s’il a été obtenu à l’extérieur de l’UE, veuillez inclure également :

1. La description complète de tous les cours de la spécialisation 
2. Le programme analytique/Le plan d’enseignement de tous les cours
3. Tout autre document pertinent 

# Frais de reconnaissance par le CNRED

Les frais de traitement du dossier sont de : **100 RON**.

Ces frais peuvent être réglés :

* personnellement, à la caisse du CNRED
* personnellement, par ordre de paiement bancaire ou par mandat postal
* en-ligne, par virement

## Coordonnées bancaires

La preuve de paiement doit inclure les informations suivantes :

1. IBAN du Ministère de l’Éducation
2. Nom du candidat
3. Nom de l’employeur qui dépose la demande de reconnaissance (si c’est le cas)

### Compte en RON

| Catégorie           | Coordonnées bancaire                                                                 |
|---------------------|--------------------------------------------------------------------------------------|
| Nom du bénéficiaire | Ministerul Educației                                                                 |
| IBAN                | RO86 TREZ 7002 0E33 0500 XXXX                                                        |
| SWIFT               | TREZROBU                                                                             |
| Nom de la banque    | Activitatea de Trezorerie şi Contabilitate Publică a Municipiului Bucureşti - ATCPMB |
| NIF (CUI)           | 13729380                                                                             |

### Compte en EUR

<span class="note">Note :</span> Les paiements en EUR sont recalculés en RON au taux de change de la <abbr title="Banque Nationale de la Roumanie">BNR</abbr> à la date en question.

| Catégorie           | Coordonnées bancaire          |
|---------------------|-------------------------------|
| Nom du bénéficiaire | Ministerul Educației          |
| IBAN                | RO35 RNCB 0080 0056 3030 0077 |
| SWIFT               | RNCBROBU                      |
| Nom de la banque    | Banca Comercială Română       |
| NIF (CUI)           | 13729380                      |

