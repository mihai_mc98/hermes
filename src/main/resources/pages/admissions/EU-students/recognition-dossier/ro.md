Dosarul de recunoaștere cuprinde un număr de documente, grupate în categorii:

<table-of-contents depth="1" lang="ro"></table-of-contents>

Pentru a evita întârzierile, vă rugăm să verificați că toate documentele respectă următoarele cerințe **înainte** de a trimite dosarul.

Dacă nu sunteți sigur de cerințe, vă rugăm să contactați [Biroul studenților internaționali](/admissions#Contact).

# Acte de identitate

Vă rugăm să includeți **copii scanate** ale următoarelor documente:

1. Pașaport
2. Cartea de identitate (dacă este cazul)
3. Dovada schimbării numelui (dacă este cazul)

<span class="note">Note:</span> Aveți nevoie de o **traducere legalizată** în limba română dacă dovada schimbării numelui nu este în limba română, engleză, franceză, spaniolă sau italiană.

# Documente de studiu

Vă rugăm să acordați o atenție sporită cerințelor <span class="note">diferite</span> pentru fiecare act de studiu, în funcție de recunoașterea anterioară a acestora de către CNRED.

## Acte de studii deja recunoscute

Studenții ale căror acte de studii au fost deja recunoscute de către CNRED trebuie să includă:
1. Copii după toate studiile recunoscute
2. Copie după adeverințele de recunoaștere

În caz contrar, vă rugăm să citiți mai departe pentru a afla mai multe informații despre procedura de Recunoașterea CNRED.

## Recunoașterea CNRED

<div class="info-box">

## Atenție ! Cerințe diferă în funcție de limba în care a fost emis documentul !

* Pentru documentele emise într-una din limbile română, franceză, engleză, spaniolă sau italiană, vă rugăm să includeți o <span class="note">copie scanată</span>
* Pentru documentele emise în orice altă limbă, vă rugăm să includeți o <span class="note">traducere legalizată</span> în limba română

</div>

<span class="note-important">Important:</span> Vă rugăm să consultați [Lista de cerințe de legalizare](https://www.upit.ro/_document/28154/lista_statelor_pentru_care_se_solicita_apostilarea_sau_supralegalizarea.pdf) pentru a verifica dacă actul de studii trebuie să aibă **Apostila de la Haga** sau dacă trebuie să fie **supralegalizat**.

1. [Cerere de recunoaștere a studiilor](https://www.upit.ro/_document/113090/11a_cerere-recunoastere-studii-cetateni-europeni-admitere-licenta_2021_en.doc)
2. Diploma de studiu
3. Supliment la diplomă (**doar** dacă specializarea nu este menționată pe documentul de studii)
4. Foile matricole
5. Alte documente relevante (dacă este cazul).

<span class="note">Notă:</span> Pentru diplomele eliberate în Republica Moldova înainte de 2008, este necesară și o **Adeverință de autenticitate** eliberată de către Ministerul Educației din Moldova.

Dacă actul de studii vizează o profesie reglementată în România **și** a fost obținut în afara UE, vă rugăm să includeți și:

1. Descrierea completă a tuturor cursurilor specializării
2. Programa analitică/Planul de învățământ al tuturor cursurilor
3. Orice document de studii relevant

# Taxe de recunoaștere CNRED

Taxa de procesare a dosarului este: **100 RON**.

Aceasta poate fi achitată:

* personal, la casieria CNRED
* personal, prin ordin de plată bancar sau prin mandat poștal
* online, prin transfer bancar

## Coordonate bancare

Dovada de plată trebuie să includă următoarele informații:

1. IBAN-ul Ministerului Educației
2. Numele candidatului
3. Numele angajatorului care depune cererea de recunoaștere (dacă este cazul)

### Cont în RON

| Categorie             | Coordonate bancare                                                                   |
|-----------------------|--------------------------------------------------------------------------------------|
| Numele beneficiarului | Ministerul Educației                                                                 |
| IBAN                  | RO86 TREZ 7002 0E33 0500 XXXX                                                        |
| SWIFT                 | TREZROBU                                                                             |
| Numele băncii         | Activitatea de Trezorerie şi Contabilitate Publică a Municipiului Bucureşti - ATCPMB |
| CUI                   | 13729380                                                                             |

### Cont în EUR

<span class="note">Notă:</span> Plățile în EUR sunt recalculate în RON la cursul de schimb al <abbr title="Băncii Naționale a României">BNR</abbr> de la data respectivă.

| Categorie             | Coordonate bancare            |
|-----------------------|-------------------------------|
| Numele beneficiarului | Ministerul Educației          |
| IBAN                  | RO35 RNCB 0080 0056 3030 0077 |
| SWIFT                 | RNCBROBU                      |
| Numele băncii         | Banca Comercială Română       |
| CUI                   | 13729380                      |
