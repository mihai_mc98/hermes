Cetățenii din <abbr title="Uniunea Europeană">UE</abbr>/<abbr title="Spațiul Economic European">SEE</abbr>/<abbr title="Confederația Elvețiană">CH</abbr> se bucură de aceleași avantaje ca şi cetățenii români. Acestea includ:

1. Plata acelorași taxe de școlarizare ca un cetățean român
2. Recunoașterea diplomelor străine

Indiferent de specializare, studenții trebuie să își echivaleze documentele de studii la <abbr title="Centrul Național pentru Recunoașterea și Echivalarea Diplomelor">CNRED</abbr>. Acest lucru se poate face:

* independent, prin depunerea unei cereri pe [site-ul CNRED](https://cnred.edu.ro/)
* prin intermediul centrului nostru universitar, ca parte a procesului de admitere

<span class="note">Notă:</span> Deși ambele modalități presupun costuri **identice**, nu putem sprijini decât candidații care au solicitat recunoașterea diplomelor prin intermediul centrului nostru universitar.

<div class="boxes">
    <a href="/admissions/EU-students/recognition-dossier">Dosar de recunoaștere</a>
    <a href="/admissions/EU-students/how-to-apply">Cum aplic ?</a>
    <a href="/admissions/preparatory-year">Anul pregătitor de limba română</a>
    <a href="/admissions/tuition-fees#Învățământ-superior">Taxe de școlarizare</a>
    <a href="/admissions/visa-and-residence-permit#Cetățeni-UE">Înregistrare rezidență</a>
    <a href="/admissions/EU-students/regulations">Reglementări</a>
</div>

# Contact

<div class="paragraph">
Dna. LEMNARU Ana Cristina, Referent al Biroului studenților internaționali

Nr. Tel.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
