If you want to study at The University Centre of Pitești and have a degree fully acknowledged all over the EU, you need to get informed. The University Centre of Pitești has a 50-year tradition of welcoming international students. Depending on where you are from, there are different paths to becoming a student.

<div class="cards">
    <div class="card">
        <img src="/images/Cards/Notebook.jpg" alt="Someone writing in a Notebook">
        <div class="card-content">
            <a href="/admissions/preparatory-year">Romanian Language Preparatory Year</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Friends in Tulip Field.jpg" alt="Friends in a Tulip Field">
        <div class="card-content">
            <a href="/admissions/romanians-everywhere">Romanians Everywhere</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Library.jpg" alt="Students in a Library">
        <div class="card-content">
            <a href="/admissions/non-EU-students">Non-EU Students</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Look at what I made.jpg" alt="People around a Computer">
        <div class="card-content">
            <a href="/admissions/EU-students">EU Students</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Bills and Fees.jpg" alt="Bills and Fees">
        <div class="card-content">
            <a href="/admissions/tuition-fees">Tuition Fees</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Passport and Visa.jpg" alt="Passport and Visa">
        <div class="card-content">
            <a href="/admissions/visa-and-residence-permit">Visa and Residence Permit</a>
        </div>
    </div>
</div>

# Discover our University Centre

* [International Student's Guide (EN)](https://www.upit.ro/_document/175051/international_students_guide.pdf)
* [University Centre Description Brochure (AR)](https://www.upit.ro/_document/193777/2022_brosura_de_prezentare_ar.pdf)

# Useful Documents

* [International Student's Regulations](https://www.upit.ro/_document/112151/regulament_studenti_internationali_senat_27.07.2020.pdf)
* [Procedures and Regulations](https://www.upit.ro/en/international/cpri/regulamente-si-proceduri)

# Contact

<div class="paragraph">
Ms. LEMNARU Ana Cristina, International Students' Office Clerk

Phone: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
