Si vous voulez étudier au Centre Universitaire de Pitești et que vous possédiez un diplôme reconnu sur tout le territoire de l’UE, vous devez vous en renseigner auprès. Le Centre Universitaire de Pitești a une tradition de presque 50 ans dans la scolarisation des étudiants internationaux. En fonction de votre pays d’origine, nous vous offrons des modalités adaptées pour devenir étudiant. 

<div class="cards">
    <div class="card">
        <img src="/images/Cards/Notebook.jpg" alt="Someone writing in a Notebook">
        <div class="card-content">
            <a href="/admissions/preparatory-year">Année préparatoire de langue roumaine</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Friends in Tulip Field.jpg" alt="Friends in a Tulip Field">
        <div class="card-content">
            <a href="/admissions/romanians-everywhere">Roumains de partout</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Library.jpg" alt="Students in a Library">
        <div class="card-content">
            <a href="/admissions/non-EU-students">Étudiants non-UE</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Look at what I made.jpg" alt="People around a Computer">
        <div class="card-content">
            <a href="/admissions/EU-students">Étudiants UE</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Bills and Fees.jpg" alt="Taxe">
        <div class="card-content">
            <a href="/admissions/tuition-fees">Frais de scolarité</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Passport and Visa.jpg" alt="Passeport et visa">
        <div class="card-content">
            <a href="/admissions/visa-and-residence-permit"> Visa et permis de séjour </a>
        </div>
    </div>
</div>

# Découvrez notre Centre Universitaire

* [Guide étudiant international (EN)](https://www.upit.ro/_document/175051/international_students_guide.pdf)
* [Plaquette de présentation (AR)](https://www.upit.ro/_document/193777/2022_brosura_de_prezentare_ar.pdf)

# Documents utiles

* [Réglementations des étudiants internationaux](https://www.upit.ro/_document/112151/regulament_studenti_internationali_senat_27.07.2020.pdf)
* [Procédures and Réglementations](https://www.upit.ro/en/international/cpri/regulamente-si-proceduri)

# Contact

<div class="paragraph">
Mme. LEMNARU Ana Cristina, Référent du Bureau des étudiants internationaux 

Tél.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
