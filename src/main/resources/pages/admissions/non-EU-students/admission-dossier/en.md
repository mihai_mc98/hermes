The Admission Dossier consists of a number of documents, grouped by type:

<table-of-contents depth="1" lang="en"></table-of-contents>

In the interest of avoiding delays, please double-check that all your documents adhere to the following guidelines **before** submitting your dossier.

If unsure of the requirements, please contact the [International Students' Office](/admissions#Contact).

<div class="info-box">

## What you need to do depends on a document's issuing language

* For documents issued in Romanian, French or English, please include a <span class="note">certified copy</span> in the admission dossier
* For documents issued in any other languages, please translate them into Romanian, French or English and include the <span class="note">certified translation</span> in the admission dossier

</div>

# Identity Documents

1. Birth Certificate
2. Proof of Permanent Residence Abroad
3. Proof of Name Change (if necessary)

## Passport

Please observe the specific requirements related to passports:

* Only a <span class="note">scanned copy</span> of pages 1, 2, 3 and 4 need be submitted
* The passport must be valid for at least another **6 months** starting from <span class="note">October 1st</span>

# Medical Certificate

1. It must be **issued** in Romanian, French or English
2. It must mention that the applicant does not suffer from:
    * Any contagious diseases
    * Other afflictions deemed incompatible with the future profession

# Study Documents

Please check the [List of Recognised Degrees and Diplomas](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) to ensure your documents will be recognised by the Ministry of Education.

1. [Study Request Form](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Degrees and Diplomas (or [Provisional Certificates](#Provisional-Certificates)):
   * Baccalaureate Diploma
   * Bachelor's Degree (if appropriate)
   * Master's Degree (if appropriate)
3. Academic Records for all submitted documents showing the GPAs for the years of study
4. Relevant Curriculum for all submitted documents (**only** if requesting the recognition of previous studies)

## Provisional Certificates

Strictly whenever candidates cannot provide the relevant study document due to delays in issuing by the respective institution, our University Centre will accept a Provisional Certificate. Such certificates must:
* be signed and/or stamped
* be issued by the Institution where the Examination/Degree was held
* certify that the Candidate has passed the relevant Exams
* mention the grades that would be written on the Diploma

Regardless of the reason, Provisional Certificates are only valid for **up to 1 year** after the successful completion of the relevant studies. Whenever the study document is issued, the Provisional Certificate automatically becomes **null and void**.

<span class="note">NOTE:</span> All Provisional Certificates must be replaced by the original underlying study documents.

<div class="info-box">

## What you need to do depends on the language in which the document was issued

* For documents issued in Romanian, French or English, please include a <span class="note">certified copy</span> in the admission dossier
* For documents issued in any other languages, please translate them into Romanian, French or English and include the <span class="note">certified translation</span> in the admission dossier

</div>

## Hague Apostille

* Was the study document issued by a member-country of the Convention regarding the Hague Apostille ?
   1. Then, the study document must be <span class="note">certified with the Hague Apostille</span> by the issuing competent authorities
* Otherwise, the study document must be <span class="note">legalised</span> by both:
   1. The issuing country's Ministry of Foreign Affairs
   2. The Romanian Embassy or Consulate of the issuing country


# Language Requirements

Please familiarise yourself with the [Language Requirements](/admissions/non-EU-students/language-requirements) needed to apply to Higher Education Degrees.