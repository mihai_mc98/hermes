Le dossier d’admission comprend plusieurs catégories de documents :

<table-of-contents depth="1" lang="fr"></table-of-contents>

Pour éviter les retards, veuillez vérifier si tous les documents respectent les conditions suivantes **avant** d’envoyer le dossier.

Si vous avez des questions, veuillez contacter le [Bureau des étudiants internationaux](/admissions#Contact).

<div class="info-box">

## Attention ! Les conditions varient en fonction de la langue dans laquelle le document a été émis !

* Pour les documents émis en roumain, français ou anglais, veuillez inclure une <span class="note">copie légalisée</span> dans le dossier d’admission. 
* Pour les documents émis dans toute autre langue, veuillez les faire traduire en roumain, français ou anglais et inclure une <span class="note">traduction légalisée </span> dans le dossier d’admission.

</div>

# Pièces d’identité

1. Acte de naissance
2. Preuve de la résidence permanente à l’étranger
3. Preuve du changement du nom (si c’est le cas).

## Passeport

Veuillez prêter une attention particulière aux conditions spécifiques concernant le passeport : 

* Vous devez joindre seulement une <span class="note"> copie scannée </span> des pages no. 1, 2, 3 et 4
* Le passeport doit avoir une durée de validité d’au moins encore **6 mois** à partir du<span class="note">1 octobre</span>

# Attestation médicale

1. Elle doit être **délivrée** en roumain, français ou anglais  
2. Elle doit mentionner que l’étudiant ne souffre :
     * d’aucune maladie contagieuse 
     * d’aucune autre affection estimée incompatible avec la future profession 

# Documents d’études 

Veuillez consulter la [Liste des spécialisations et des diplômes reconnus](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pour avoir la certitude que vos documents sont reconnus par le Ministère de l’Education. 

1. [Demande d’inscription aux études](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Diplômes (ou [Attestations d’études](#Attestations-d’études)) :
   *  Diplôme de baccalauréat
    * Diplôme de licence (si c’est le cas)
    * Diplôme de master (si c’est le cas)
3. Relevés de notes de tous les diplômes qui permettent de calculer la moyenne des années d’études. 
4. Programme analytique/Plan d’enseignement pour tous les diplômes déposés (**seulement** si le candidat sollicite la reconnaissance des études antérieures)

## Attestations d’études 

Le centre universitaire peut accepter des attestations d’études, seulement s’il y a des retards dans la délivrance des documents d’études originaux. De telles attestations doivent :
* être signées et/ou avoir le tampon de l’établissement
* être délivrées par l’établissement dans lequel le candidat a passé l’examen
* confirmer le fait que le candidat a réussi aux examens pris en considération
* mentionner les notes qui seront inscrites sur le diplôme  

Quelle que soit la raison, les attestations ne sont valables qu’**un an** à compter du moment de la fin des études. Une fois le diplôme délivré, l’Attestation devient automatiquement **nulle et non avenue**.

<span class="note">Note :</span> Les attestations d’études ne sont qu’une solution temporaire ! Ce sont les diplômes en original que les candidats doivent présenter pour la poursuite de leurs études.

<div class="info-box">

## Attention ! Les conditions diffèrent en fonction de la langue dans laquelle le document a été émis !

* Pour les documents émis en roumain, français ou anglais, veuillez inclure <span class="note">copie légalisée</span> dans le dossier d’admission.
* Pour les documents émis dans n’importe quelle autre langue, veuillez les faire traduire en roumain, français ou anglais et inclure une <span class="note">traduction légalisée</span> dans le dossier d’admission.

</div>

# Apostille de la Haya

* Le document d’études a été émis par un pays membre de la Convention d’Apostille de la Haye ?
   1. Dans ce cas, le document d’études doit être <span class="note">légalisé avec l’Apostille de la Haye</span> par les autorités compétentes. 
* Dans le cas contraire, le document d’études doit être <span class="note">surlégalisé</span> par :
   1. Le Ministère des Affaires Extérieures du pays émetteur 
   2. L’Ambassade ou le Consulat de Roumanie dans le pays émetteur

# Conditions linguistiques

Veuillez vous renseigner sur les [Conditions linguistiques](/admissions/non-EU-students/language-requirements) nécessaires pour l’inscription aux programmes d’études de l’enseignement supérieur.
