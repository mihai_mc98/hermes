Dosarul de admitere cuprinde un număr de documente, grupate în categorii:

<table-of-contents depth="1" lang="ro"></table-of-contents>

Pentru a evita întârzierile, vă rugăm să verificați că toate documentele respectă următoarele cerințe **înainte** de a trimite dosarul.

Dacă nu sunteți sigur de cerințe, vă rugăm să contactați [Biroul studenților internaționali](/admissions#Contact).

<div class="info-box">

## Atenție ! Cerințe diferă în funcție de limba în care a fost emis documentul !

* Pentru documentele emise în limba română, franceză sau engleză, vă rugăm să includeți o <span class="note">copie legalizată</span> în dosarul de admitere
* Pentru documentele emise în orice altă limbă, vă rugăm să le traduceți în limba română, franceză sau engleză și să includeți o <span class="note">traducere legalizată</span> în dosarul de admitere.

</div>

# Acte de identitate

1. Certificat de naștere
2. Dovada reședinței permanente în străinătate
3. Dovada schimbării numelui (dacă este cazul).

## Pașaport

Vă rugăm să acordați o atenție sporită cerințelor specifice legate de pașapoarte:

* Trebuie depusă o <span class="note"> copie scanată </span> a paginilor 1, 2, 3 și 4
* Pașaportul trebuie să fie valabil cel puțin încă **6 luni** începând de la data de <span class="note">1 octombrie</span>

# Adeverință medicală

1. Trebuie să fie **eliberată** în română, franceză sau engleză
2. Trebuie să menționeze că studentul nu suferă de:
     * Nicio boală contagioasă
     * Nicio altă afecțiune considerată incompatibilă cu viitoarea profesie

# Documente de studiu

Vă rugăm să consultați [Lista specializărilor și a diplomelor recunoscute](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pentru a vă asigura că actele dumneavoastră sunt recunoscute de Ministerul Educației.

1. [Cerere de înscriere la studii](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Diplome (sau [Adeverințe de studiu](#Adeverințe-de-studiu)):
   *  Diploma de bacalaureat
    * Diploma de licență (dacă este cazul)
    * Diploma de masterat (dacă este cazul)
3. Foile matricole ale tuturor diplomelor din care să reiasă media anilor de studiu
4. Programa analitică/Planul de învățământ pentru toate diplomele depuse (**doar** dacă se solicită recunoașterea studiilor anterioare)

## Adeverințe de studiu

Centrul universitar poate accepta adeverințe de studiu, numai când există întârzieri în eliberarea documentelor de studiu originale. Astfel de adeverințe trebuie să:
* fie semnate și/sau ștampilate
* fie eliberate de instituția unde s-a susținut examenul
* ateste faptul că studentul a promovat examenele relevante
* menționeze notele care vor fi înscrise pe diplomă

Indiferent de motiv, adeverințele sunt valabile doar **1 an** de la finalizarea studiilor. Odată ce diploma de studii este eliberată, Adeverința devine automat **nulă de drept**.

<span class="note">Notă:</span> Adeverințele de studiu nu sunt decât o soluție temporară ! Diplomele originale trebuie prezentate pentru continuarea studiilor.

<div class="info-box">

## Atenție ! Cerințe diferă în funcție de limba în care a fost emis documentul !

* Pentru documentele emise în română, franceză sau engleză, vă rugăm să includeți o <span class="note">copie legalizată</span> în dosarul de admitere.
* Pentru documentele emise în orice altă limbă, vă rugăm să le traduceți în română, franceză sau engleză și să includeți <span class="note">traducerea legalizată </span> în dosarul de admitere.

</div>

# Apostila de la Haga

* A fost actul de studii emis de către o țară-membră a Convenției privind Apostila de la Haga?
   1. Atunci, actul de studii trebuie să fie <span class="note">legalizat cu apostila de la Haga</span> de către autoritățile competente
* În caz contrar, actul de studii trebuie să fie <span class="note">supralegalizată</span> de către:
   1. Ministerul Afacerilor Externe al țării emitente
   2. Ambasada sau Consulatul României din țara emitentă

# Cerințe lingvistice

Vă rugăm să vă familiarizați cu privire la [Cerințele lingvistice](/admissions/non-EU-students/language-requirements) necesare pentru înscrierea la programele de studiu din învățământul superior.
