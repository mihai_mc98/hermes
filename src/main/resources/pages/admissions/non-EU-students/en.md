
# Entry Requirements

Admission of non-EU students at The University Centre of Pitești is based on the:

* [Admission Dossier](/admissions/non-EU-students/admission-dossier)
* [Language Requirements](/admissions/non-EU-students/language-requirements) 
* Faculty-specific criteria
* Places availability

Unless otherwise specified, **no** admission exams are required.

<div class="boxes">
    <a href="/admissions/non-EU-students/language-requirements">Language Requirements</a>
    <a href="/admissions/non-EU-students/admission-dossier">Admission Dossier</a>
    <a href="/admissions/non-EU-students/how-to-apply">How to Apply</a>
    <a href="/admissions/tuition-fees">Tuition Fees</a>
    <a href="/admissions/visa-and-residence-permit">Study Visa and Residence Permit</a>
    <a href="/admissions/non-EU-students/regulations">Regulations</a>
</div>

# Admission Calendar

- Registration opens: <span class="note">1st of February <span class="this-year"></span></span>
- Registration deadline: <span class="note">31st of July <span class="this-year"></span></span>

<span class="note-important">Important:</span> In line with the Study Enrolment Contract, whereby students enrol after the <span class="note">15th of October <span class="this-year"></span></span>, the Entry Point is delayed to the next Academic Year.

# Contact

<div class="paragraph">
Ms. LEMNARU Ana Cristina, International Students' Office Clerk

Phone: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>