# Conditions d’admission 

L’admission des étudiants hors UE au Centre Universitaire de Pitești repose sur :

* [Dossier d’admission](/admissions/non-EU-students/admission-dossier)
* Satisfaction des [Conditions linguistiques](/admissions/non-EU-students/language-requirements) 
* Critères spécifiques à chaque faculté 
* L’existence de places disponibles 

En général, **il n’y a pas** d’examen d’admission, sauf si le programme d’études en nécessite un.

<div class="boxes">
    <a href="/admissions/non-EU-students/language-requirements">Conditions linguistiques</a>
    <a href="/admissions/non-EU-students/admission-dossier">Dossier d’admission</a>
    <a href="/admissions/non-EU-students/how-to-apply">Comment postuler?</a>
    <a href="/admissions/tuition-fees">Frais de scolarité</a>
    <a href="/admissions/visa-and-residence-permit">Visa d’études et permis de séjour</a>
    <a href="/admissions/non-EU-students/regulations">Réglementations</a>
</div>

# Calendrier des inscriptions

- Date de commencement: <span class="note">1 février <span class="this-year"></span></span>
- Date limite: <span class="note">31 juillet <span class="this-year"></span></span>

<span class="note-important">Important :</span> Conformément au contrat de scolarité, dans le cas où l’immatriculation des étudiants serait faite après le <span class="note">15 octobre <span class="this-year"></span></span>, les études seront reportées pour l’année universitaire suivante.

# Contact

<div class="paragraph">
Mme. LEMNARU Ana Cristina, Référent du Bureau des étudiants internationaux 

Tél.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
