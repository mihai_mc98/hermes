Nombre de places disponibles: <span class="note">150 places/an</span>.

# Comment postuler ?

Les candidats doivent déposer le [dossier d’admission](/admissions/non-EU-students/admission-dossier) soit :
- par courriel, à l’adresse : [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
- personnellement ou bien par la poste à l’adresse: **No 1, Rue Târgu din Vale, Pitești, Argeș, 110040, România**

# Processus d’admission

1. Notre centre universitaire analyse le dossier d’admission 
2. Après l’analyse initiale, nous envoyons le dossier d’admission au Ministère de l’Education en vue de sa validation
3. Le Ministère de l’Education émet une **Lettre d’acceptation aux études** pour les candidats
4. Une fois reçue la **Lettre d’acceptation**, nous vous informons sur le résultat concernant votre dossier 

# Pas à suivre

## Visa d’études

<span class="note-important">Important :</span> Il est possible que vous ayez besoin d’un visa d’études pour pouvoir faire des études en Roumanie !

Pour solliciter un visa d’études, vous aurez besoin des documents suivants :
1. Lettre d’acceptation aux études
2. Preuve du paiement [frais de scolarité](/admissions/tuition-fees#Enseignement-supérieur)

Veuillez visiter la section pour les [Visas et permis de séjour](/admissions/visa-and-residence-permit#Citoyens-hors-de-l’UE) pour des renseignements, encadrement et appui supplémentaire.

## Immatriculation

<span class="note-important">Important :</span> Pour être immatriculés, les candidats doivent apporter **personnellement** tous les documents du Dossier d’admission en original au Bureau des étudiants internationaux.

Parmi ces documents, veuillez retenir :
1. Copies légalisées et/ou traductions légalisées 
2. Documents d’études 
3. Lettre d’acceptation délivrée par le Ministère de l’Education
4. Passeport valable avec le visa d’études 

### Bureau

Le bureau des étudiants internationaux se trouve à la [Chambre I.33, Réctorat, Bâtiment B, No 1, Rue Târgu din Vale, Pitești, Argeș, 110440, România](https://g.page/official_upit).

