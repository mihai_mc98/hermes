Număr de locuri disponibile: <span class="note">150 de locuri/an</span>.

# Cum aplic ?

Candidații trebuie să depună [dosarul de admitere](/admissions/non-EU-students/admission-dossier) fie:
- prin e-mail, la adresa: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
- personal sau prin poștă, la adresa: **Str. Târgu din Vale, Nr. 1, Pitești, Argeș, 110040, România**

# Procesul de admitere

1. Centrul nostru universitar analizează dosarul de admitere
2. După analiza inițială, trimitem dosarul de admitere către Ministerul Educației pentru validare
3. Ministerul Educației emite o **Scrisoare de acceptare la studii** candidaților
4. Odată ce primim **Scrisorea de acceptare**, vă informăm cu privire la rezultatul dosarului dumneavoastră

# Pașii următori

## Viza de studiu

<span class="note-important">Important:</span> Este posibil să aveți nevoie de o viză de studiu pentru a studia în România !

Pentru a solicita o viză de studii, veți avea nevoie de următoarele documente:
1. Scrisoarea de acceptare la studii
2. Dovada plății [taxelor de școlarizare](/admissions/tuition-fees#Învățământ-superior)

Vă rugăm să vizitați secțiunea pentru [Vize și permis de ședere](/admissions/visa-and-residence-permit#Cetățeni-din-afara-UE) pentru informații, îndrumare și sprijin suplimentar.

## Înmatriculare

<span class="note-important">Important:</span> Pentru a se înmatricula, candidații trebuie să aducă **personal** toate documentele din Dosarul de admitere în original la Biroul studenților internaționali.

Printre acestea se numără:
1. Copiile legalizate și/sau traducerile legalizate
2. Documente de studiu
3. Scrisoarea de acceptare eliberată de Ministerul Educației
4. Un pașaport în valabilitate cu viză de studiu

### Locație

Biroul studenților internaționali se află la [Camera I.33, Rectorat, Corp B, Str. Târgu din Vale nr. 1, Pitești, Argeș, 110440, România](https://g.page/official_upit).

