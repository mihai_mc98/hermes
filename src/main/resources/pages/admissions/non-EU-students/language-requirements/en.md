All candidates must submit proof of good command of the Romanian language: at least **level B2** (Common European Framework of Reference for Languages) for the following components:
* Reading
* Writing
* Listening
* Speaking

Unless otherwise exempt, all students must enrol in and graduate the [Romanian Language Preparatory Year](/admissions/preparatory-year) before progressing to Higher Education Degrees.

# Exemptions and Exclusions

The following categories are exempt from enrolling in the Preparatory Year:

- Candidates enrolling into Higher Education Degrees that are taught in a foreign language
- Candidates who have a Romanian Language Proficiency Certificate – at least level B2 in all components
- Candidates who have graduated the Preparatory Year in the past