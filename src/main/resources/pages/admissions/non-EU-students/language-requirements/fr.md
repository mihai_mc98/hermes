Les candidats doivent faire preuve d’une bonne maîtrise de la langue roumaine : au moins le **niveau B2** (Cadre Européen Commun de Référence) pour les compétences suivantes :
* Lire
* Écrire
* Écouter
* Parler

S’ils n’en sont pas déjà dispensés, tous les étudiants doivent s’inscrire à l’[Année préparatoire de langue roumaine](/admissions/preparatory-year) et finaliser cette année avant de continuer dans le cadre des programmes d’études de l’enseignement supérieur.

# Exceptions

Sont dispensés de l’inscription à l’Année préparatoire de langue roumaine et de la finalisation de cette année : 

- Les candidats qui s’inscrivent aux programmes d’enseignements supérieurs dispensés dans une langue étrangère 
- Les candidats qui possèdent un certificat de compétence linguistique pour la langue roumaine – au moins le niveau B2 pour toutes les compétences
- Les candidats qui ont déjà finalisé l’année préparatoire 

