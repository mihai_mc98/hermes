Candidații trebuie dovedească o bună stăpânire a limbii române: cel puțin **nivelul B2** (Cadrul European Comun de Referință) pentru următoarele componente:
* Citire
* Scriere
* Ascultare
* Vorbire

Dacă nu sunt deja scutiți, toți studenții trebuie să se înscrie la și să finalizeze [Anul pregătitor de limba română](/admissions/preparatory-year) înainte de a progresa către programele de studii din învățământul superior.

# Excepții

Sunt scutiți de la înscrierea și absolvirea Anului pregătitor următoarele categorii:

- Candidații care se înscriu la programme de învățământ superior care sunt predate într-o limbă străină
- Candidați care au un certificat de competență lingvistică pentru limba română – cel puțin nivelul B2 la toate componentele
- Candidații care au absolvit deja anul pregătitor
