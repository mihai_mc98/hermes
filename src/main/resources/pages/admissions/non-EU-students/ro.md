# Cerințe de admitere

Admiterea studenților din afara UE la Centrul Universitar Pitești se face pe bază de:

* [Dosar de admitere](/admissions/non-EU-students/admission-dossier)
* Satisfacerea [Cerințelor lingvistice](/admissions/non-EU-students/language-requirements) 
* Criterii specifice ale fiecărei facultăți
* Disponibilitatea locurilor

De regulă, **nu** se organizează examen de admitere decât dacă programul de studiu necesită acest lucru.

<div class="boxes">
    <a href="/admissions/non-EU-students/language-requirements">Cerințe lingvistice</a>
    <a href="/admissions/non-EU-students/admission-dossier">Dosar de admitere</a>
    <a href="/admissions/non-EU-students/how-to-apply">Cum aplic ?</a>
    <a href="/admissions/tuition-fees">Taxe de școlarizare</a>
    <a href="/admissions/visa-and-residence-permit">Viză de studii și permis de ședere</a>
    <a href="/admissions/non-EU-students/regulations">Reglementări</a>
</div>

# Calendar de înscriere

- Data de începere: <span class="note">1 februarie <span class="this-year"></span></span>
- Data limită: <span class="note">31 iulie <span class="this-year"></span></span>

<span class="note-important">Important:</span> În conformitate cu contractul de școlarizare, în cazul în care studenții se înmatriculează după <span class="note">15 octombrie <span class="this-year"></span></span>, studiile se vor raporta pentru următorul anul universitar.

# Contact

<div class="paragraph">
Dna. LEMNARU Ana Cristina, Referent al Biroului studenților internaționali

Nr. Tel.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
