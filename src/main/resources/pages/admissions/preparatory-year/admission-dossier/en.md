The Admission Dossier consists of a number of documents, grouped by type:

<table-of-contents depth="1" lang="en"></table-of-contents>

<span class="note-important">Important:</span> In the interest of avoiding delays, please double-check that all your documents adhere to the following guidelines **before** submitting your dossier.

If unsure of the requirements, please contact our [International Students' Office](/admissions#Contact)

# Identity Documents

1. Birth Certificate
2. Proof of Permanent Residence Abroad
3. Proof of Name Change (if necessary)

<div class="info-box">

## What you need to do depends on the language in which the document was issued

* For documents issued in Romanian, French or English, please include a <span class="note">certified copy</span> in the admission dossier
* For documents issued in any other languages, please translate them into Romanian, French or English and include the <span class="note">certified translation</span> in the admission dossier

</div>

## Passport

Please observe the specific requirements related to passports:

* Only a <span class="note">scanned copy</span> of pages 1, 2, 3 and 4 need be submitted
* The passport must be valid for at least another **6 months** starting from <span class="note">October 1st</span>

# Medical Certificate

1. It must be **issued** in Romanian, French or English
2. It must mention that the applicant does not suffer from:
   * Any contagious diseases
   * Other diseases deemed incompatible with the future profession

# Study Documents

Please consult the [List of Recognised Degrees and Diplomas](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) to ensure your documents are recognised by the Ministry of Education.

1. [Study Request Application Form](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Degrees and Diplomas:
   * Baccalaureate Diploma
   * Bachelor's Degree (if appropriate)
   * Master's Degree (if appropriate)
3. Academic Records for all submitted documents
4. Relevant Curriculum for all submitted documents (**only** if requesting the recognition of previous studies)

<div class="info-box">

## What you need to do depends on the language in which the document was issued

* For documents issued in Romanian, French or English, please include a <span class="note">certified copy</span> in the admission dossier
* For documents issued in any other languages, please translate them into Romanian, French or English and include the <span class="note">certified translation</span> in the admission dossier

</div>

## Hague Apostille

Please observe the <span class="note">different</span> requirements for each Study Document, depending on whether it was issued in a [Member-Country](https://www.hcch.net/en/instruments/conventions/status-table) of the Hague Apostille Convention or not.

### Member of the Hague Apostille Convention

The Study Document must be <span class="note">certified with the Hague Apostille</span> by the Competent Authorities.

### Non-Members of the Hague Apostille Convention

The Study Document must be <span class="note">legalised</span> by both:
1. The Ministry of Foreign Affairs of the Country of Issue
2. The Romanian Embassy/Consulate in the Country of Issue

<span class="note-important">IMPORTANT NOTE</span>

Please bring your original documents as well as the original certified copies/translations with you at enrolment !
