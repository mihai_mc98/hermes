Le dossier d’admission comprend plusieurs catégories de documents : 

<table-of-contents depth="1" lang="fr"></table-of-contents>

<span class="note-important">Important :</span> Pour éviter les retards, veuillez vérifier si tous les documents respectent les conditions suivantes **avant** d’envoyer le dossier.

Si vous avez des questions, veuillez contacter le [Bureau des étudiants internationaux](/admissions#Contact)

# Pièces d’identité

1. Acte de naissance
2. Preuve de la résidence permanente à l’étranger
3. Preuve du changement du nom (si c’est le cas).

<div class="info-box">

## Attention ! Les conditions diffèrent en fonction de la langue dans laquelle le document a été émis !

* Pour les documents émis en roumain, français ou anglais, veuillez inclure <span class="note">copie légalisée</span> dans le dossier d’admission.

* Pour les documents émis dans n’importe quelle autre langue, veuillez les faire traduire en roumain, français ou anglais et inclure une <span class="note">traduction légalisée</span> dans le dossier d’admission.
</div>

## Passeport 

Veuillez prêter une attention particulière aux conditions spécifiques concernant le passeport : 

* Vous devez joindre seulement une <span class="note"> copie scannée </span> des pages no. 1, 2, 3 et 4
* Le passeport doit avoir une durée de validité d’au moins encore **6 mois** à partir du<span class="note">1 octobre</span>

# Attestation médicale

1. Elle doit être **émise** en roumain, français ou anglais  
2. Elle doit mentionner que l’étudiant ne souffre :
     * d’aucune maladie contagieuse 
     * d’aucune autre affection estimée incompatible avec la future profession 

# Documents d’études

Veuillez consulter la [Liste des diplômes reconnus](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pour avoir la certitude que vos documents sont reconnus par le Ministère de l’Education.

1. [Demande d’inscription aux études](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Diplômes :
   * Diplôme de baccalauréat
    * Diplôme de licence (si c’est le cas)
    * Diplôme de master (si c’est le cas)
3. Relevés de notes associés à tous les diplômes
4. Programme analytique/Plan d’enseignement pour tous les documents déposés (**seulement** si le candidat sollicite la reconnaissance des études antérieures)

<div class="info-box">

## Attention ! Les conditions diffèrent en fonction de la langue dans laquelle le document a été émis !

* Pour les documents émis en roumain, français ou anglais, veuillez inclure la <span class="note">copie légalisée</span> dans le dossier d’admission 
* Pour les documents émis dans n’importe quelle autre langue, veuillez les faire traduire en roumain, français ou anglais et inclure une <span class="note">traduction légalisée</span> dans le dossier d’admission.

</div>

## Apostille de la Haye

Veuillez prêter une attention particulière aux conditions <span class="note">différentes</span> pour chaque document d’études, en fonction de [l’appartenance du pays émetteur](https://www.hcch.net/en/instruments/conventions/status-table) à la Convention d’Apostille de la Haye.

### Etats membres de la Convention

Le document d’études doit être <span class="note">légalisé avec l’Apostille de la Haye</span> par les autorités compétentes.

### Etats non-membres de la Convention 

Le document d’études doit être <span class="note">surlégalisé</span> par:
1. Le Ministère des Affaires Extérieures du pays émetteur 
  2. L’Ambassade ou le Consulat de Roumanie dans le pays émetteur

<span class="note-important">NOTE IMPORTANTE</span>.

Veuillez apporter à l’immatriculation les documents en original, ainsi que les copies/ traductions légalisées en original !
