Dosarul de admitere cuprinde o serie de documente, grupate în categorii:

<table-of-contents depth="1" lang="ro"></table-of-contents>

<span class="note-important">Important:</span> Pentru a evita întârzierile, vă rugăm să verificați că toate documentele respectă următoarele cerințe **înainte** de a depune dosarul.

Dacă nu sunteți sigur de cerințe, vă rugăm să contactați [Biroul studenților internaționali](/admissions#Contact).

# Documente de identitate

1. Certificatul de naștere
2. Dovada reședinței permanente în străinătate
3. Dovada schimbării numelui (dacă este necesar)

<div class="info-box">

## Cerințe diferite în funcție de limba în care a fost emis documentul !

* Pentru documentele eliberate în limba română, franceză sau engleză, vă rugăm să includeți o <span class="note">copie legalizată</span> în dosarul de admitere
* Pentru documentele eliberate în orice altă limbă, vă rugăm să le traduceți în limba română, franceză sau engleză și să includeți <span class="note">traducerea legalizată</span> în dosarul de admitere

</div>

## Pașaport

Vă rugăm să acordați o atenție sporită cerințelor specifice referitoare la pașapoarte:

* Doar o <span class="note">copie scanată</span> a paginilor 1, 2, 3 și 4 este necesară
* Pașaportul trebuie să fie valabil încă cel puțin **6 luni** de la data de <span class="note">1 octombrie</span>.

# Adeverință medicală

1. Aceasta trebuie să fie **emisă** în limba română, franceză sau engleză
2. Acesta trebuie să menționeze că studentul nu suferă de:
   * Orice boală contagioasă
   * Alte boli considerate incompatibile cu viitoarea profesie

# Documente de studii

Vă rugăm să consultați [Lista diplomelor recunoscute](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pentru a vă asigura că documentele dumneavoastră sunt recunoscute de Ministerul Educației.

1. [Cerere de înscriere la studii](https://www.upit.ro/_document/111547/anexa_nr._2-2021-2022.pdf)
2. Diplome:
   * Diploma de bacalaureat
   * Diplomă de licență (dacă este cazul)
   * Diplomă de master (dacă este cazul)
3. Foile matricole asociate tuturor diplomelor
4. Programa analitică/Planul de învățământ pentru toate documentele depuse (**numai** dacă se solicită recunoașterea studiilor anterioare)


<div class="info-box">

## Atenție ! Cerințe diferă în funcție de limba în care a fost emis documentul !

* Pentru documentele eliberate în limba română, franceză sau engleză, vă rugăm să includeți o <span class="note">copie legalizată</span> în dosarul de admitere
* Pentru documentele eliberate în orice altă limbă, vă rugăm să le traduceți în limba română, franceză sau engleză și să includeți <span class="note">traducerea legalizată</span> în dosarul de admitere

</div>

## Apostila de la Haga

Vă rugăm să acordați o atenție sporită cerințelor <span class="note">diferite</span> pentru fiecare document de studiu, în funcție de [apartenența țării emitente](https://www.hcch.net/en/instruments/conventions/status-table) la Convenția privind Apostila de la Haga.

### Stat membru al Convenției

Documentul de studiu trebuie să fie <span class="note">legalizat cu Apostila de la Haga</span> de către autoritățile competente.

### Statele care nu sunt membre ale Convenției

Documentul de studiu trebuie să fie <span class="note">supralegalizat</span> de ambele către:
1. Ministerul Afacerilor Externe al țării de emitere
2. Ambasada/Consulatul României din țara de emitere

<span class="note-important">NOTĂ IMPORTANTĂ</span>.

Vă rugăm să aduceți la înmatriculare documentele originale, precum și copiile/traducerile legalizate în original !
