# Advantages

* Studies conducted in an intercultural academic environment, which allows graduates an excellent integration into the labor market, anywhere in the world
* The opportunity to discover Romania, a country with wonderful landscapes and powerful traditions where events for young people are always held
* The language skills acquired through this programme may come as a considerable advantage when being hired by companies in Romania

# Course Description

This programme is addressed to candidates who wish to study in Romania and who do not have a Certificate of Language Proficiency.
* Duration: 1 year (2 semesters)
* Acquired Skills:
  * Reading, listening, speaking and writing – at least **level B1** according to the Common European Framework of Reference (CEFR)
  * Language skills specific to the future study field

# Course Units

<table>
    <thead>
        <tr>
            <th>Course Title</th>
            <th>Language Skills</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Romanian Language</td>
<td>

- Understanding written texts and speeches
- Grammatical structures and vocabulary
- Oral and written communication

</td>
        </tr>
        <tr>
            <td>Romanian Culture and Civilisation</td>
<td>

* Understanding written texts and speeches
* Gramatical structures and vocabulary

</td>
        </tr>
        <tr>
            <td>Romanian Language Practical Course</td>
<td>

* Drafting written texts and speaking
* Oral and written communication
* Specific vocabulary in various fields (e.g.: Engineering, Biology, Medicine, Social Sciences and Humanities, Law, Economy and Arts)

</td>
        </tr>
    </tbody>
</table>

# Course-Unit Exams

1. Each subject in the Curriculum has a **final exam**
2. Awarded grades range from **1** to **10**
3. The minimum passing grade is **5**

<span class="note-important">Important:</span> Students need a minimum Mark Overall Average of **7** to progress to Higher Education Degrees

# Completion of Studies (Graduation Exam)

<div class="info-box">

## Your Graduation Exam is different if you are pursuing in Romania:
* Higher Education Degrees
* High-School Education

</div>

## Students pursuing Higher Education Degrees

The Graduation Exams will consist of **3** parts:
1. Written exam for assessing the fundamental and specialised knowledge of the Romanian language and of Romanian for Special Purposes
2. Europass-style Curriculum Vitæ written in Romanian
3. Essay on Culture and Civilisation comparing and contrasting the Romanian Culture and Civilisation with those of the students'

## Students pursuing High-School Education

The Graduation Exam will consist of **2** parts:
1. Written exam for assessing the fundamental and specialised knowledge of the Romanian language
2. Europass-style Curriculum Vitæ written in Romanian