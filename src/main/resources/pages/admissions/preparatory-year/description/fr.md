# Avantages

* Études suivies dans un milieu académique interculturel qui permet aux diplômés une excellente intégration sur le marché du travail, partout dans le monde ;
* La possibilité de découvrir la Roumanie, un pays avec des paysages magnifiques et des traditions enracinées, qui organise régulièrement de nombreux évènements destinés aux jeunes
* Les compétences linguistiques acquises au cours de ce programme constituent un avantage considérable pour trouver un emploi au sein des sociétés de Roumanie.

# Description du programme

Ce programme s’adresse aux candidats qui souhaitent suivre leurs études en Roumanie et ne possèdent pas un certificat de compétence linguistique pour cette langue.
* Durée : 1 an (2 semestres)
* Compétences acquises :
  * Compréhension écrite, compréhension orale, expression orale et expression écrite - minimum **niveau B1** conformément au Cadre Européen Commun de Référence (CECR) 
  * Compétences linguistiques spécifiques au domaine dans lequel le candidat poursuivra ses études ;


# Cours du programme 

<table>
    <thead>
        <tr>
            <th>Nom du cours</th>
            <th>Connaissances linguistiques acquises</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Langue roumaine</td>
<td>

- Réception du texte écrit et oral
- Structures grammaticales et vocabulaire
- Communication orale et écrite


</td>
        </tr>
        <tr>
            <td>Culture et civilisation roumaines</td>
<td>

* Réception du texte écrit et oral
* Structures grammaticales et vocabulaire

</td>
        </tr>
        <tr>
            <td>Travaux dirigés de langue roumaine</td>
<td>

* Rédaction du texte écrit et expression orale
* Communication orale et écrite
* Langage spécialisé dans différents domaines (Ingénierie, Biologie, Médecine, Sciences sociales et humaines, Droit, Économie et Arts)
</td>
        </tr>
    </tbody>
</table>

# Évaluation

1. Chaque discipline du plan d’enseignement prévoit une **évaluation finale**.
2. Les résultats sont exprimés en notes de **1** à **10** 
3. La note minimale de promotion est **5**

<span class="note-important">Important :</span> Les élèves ont besoin d’une moyenne générale minimale de **7** pour pouvoir s’inscrire aux programmes d’études de l’enseignement supérieur

# Fin des études (examen de fin d’études)

<div class="info-box">

## L’examen de fin d’études diffère selon ce que vous allez étudier en Roumanie :
* Études universitaires
* Études de lycée 

</div>

## Les étudiants qui suivront des études universitaires

L’examen de fin d’études se compose de **3** épreuves : 
1. Épreuve écrite pour l’évaluation des connaissances fondamentales et de spécialité de langue roumaine et de langages de spécialité
2. Curriculum Vitae, format Europass, en roumain
3. Essai sur un sujet de culture et civilisation qui compare la culture et civilisation roumaines avec celles de l’étudiant

## Les étudiants qui suivront des études de lycée

L’examen de fin d’études se compose de **2** épreuves : 
1. Épreuve écrite pour l’évaluation des connaissances fondamentales et de spécialité de langue roumaine
2. Curriculum Vitae, format Europass, en roumain

