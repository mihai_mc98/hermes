# Avantaje

* Studii efectuate într-un mediu academic intercultural, ceea ce permite absolvenților o integrare excelentă pe piața muncii, oriunde în lume
* Posibilitatea de a descoperi România, o țară cu peisaje minunate și tradiții puternice, unde se organizează mereu evenimente pentru tineri
* Competențele lingvistice dobândite prin acest program pot constitui un avantaj considerabil la angajarea în cadrul companiilor din România

# Descrierea cursului

Acest program se adresează candidaților care doresc să studieze în România și care nu dețin un certificat de competență lingvistică.
* Durata: 1 an (2 semestre)
* Competențe dobândite:
  * Citire, ascultare, vorbire și scriere - cel puțin **nivelul B1** conform Cadrului European Comun de Referință (CEFR)
  * Competențe lingvistice specifice viitorului domeniu de studiu


# Cursuri din cadrul programului 

<table>
    <thead>
        <tr>
            <th>Numele cursului</th>
            <th>Cunoștințe lingvistice dobândite</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Limba română</td>
<td>

- Înțelegerea textelor scrise și a limbajului vorbit
- Structuri gramaticale și vocabular
- Comunicarea orală și scrisă


</td>
        </tr>
        <tr>
            <td>Cultura și civilizația românească</td>
<td>

* Înțelegerea textelor scrise și a limbajului vorbit
* Structuri gramaticale și vocabular

</td>
        </tr>
        <tr>
            <td>Curs practic de limba română</td>
<td>

* Redactarea de texte scrise și exprimarea orală
* Comunicare orală și scrisă
* Vocabular specific în diverse domenii (de ex: Inginerie, Biologie, Medicină, Științe sociale și umaniste, Drept, Economie și Arte)
</td>
        </tr>
    </tbody>
</table>

# Examene

1. Fiecare disciplină din planul de învățământ are un **examen final**.
2. Notele acordate variază de la **1** la **10**
3. Nota minimă de trecere este **5**

<span class="note-important">Important:</span> Elevii au nevoie de o medie generală minimă de **7** pentru a se putea înscrie la programele de studiu din învățământul superior

# Finalizarea studiilor (examen de absolvire)

<div class="info-box">

## Examenul de absolvire diferă în funcție de ceea ce va urma să studiați în România:
* Programe de studii din învățământul superior
* Studii de liceu

</div>

## Studenții care vor urma programe de studiu din învățământul superior

Examenele de absolvire vor consta în **3** probe:
1. Proba scrisă de evaluare a cunoștințelor fundamentale și de specialitate de limba română și de limba română specializată
2. Curriculum Vitæ în format tip Europass redactat în limba română
3. Eseu de cultură și civilizație în care se vor compara cultura și civilizația românească cu cele ale studentului

## Studenții care vor urma studii liceale

Examenul de absolvire va consta în **2** probe:
1. Proba scrisă de evaluare a cunoștințelor fundamentale și de specialitate de limba română
2. Curriculum Vitæ în format Europass redactat în limba română

