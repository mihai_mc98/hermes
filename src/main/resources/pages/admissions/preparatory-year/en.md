This programme is aimed at candidates who do not have the necessary Romanian language skills and who wish to study in Romania one of the following:

- pre-university courses
- university degrees (Bachelor's Degree/Master's Degree/Ph.D.)
- courses that allow for teaching careers
- postgraduate degrees
- specialisation courses

Candidates with the necessary Romanian language skills who do not have a Language Proficiency Certificate may request to sit such an exam at The University Centre of Pitești.

<div class="boxes">
    <a href="/admissions/preparatory-year/description">What is the Romanian Language Preparatory Year ?</a>
    <a href="/admissions/preparatory-year/admission-dossier">Admission Dossier</a>
    <a href="/admissions/preparatory-year/how-to-apply">How to Apply</a>
    <a href="/admissions/tuition-fees">Tuition Fees</a>
    <a href="/admissions/visa-and-residence-permit">Visa and Residence Permit</a>
    <a href="/admissions/preparatory-year/regulations">Regulations</a>
</div>

# Admission Calendar

- Registration deadline: <span class="note">31st of July <span class="this-year"></span></span>
- Enrolment deadline: <span class="note">20th of December <span class="this-year"></span></span>

# Contact

<div class="paragraph">
Ms. LEMNARU Ana Cristina, International Students' Office Clerk

Phone: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>