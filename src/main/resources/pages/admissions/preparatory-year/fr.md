Ce programme s’adresse aux candidats qui n’ont pas les connaissances nécessaires en roumain et qui souhaitent étudier en Roumanie :
- des cours préuniversitaires
- des études universitaires (licence/master/doctorat)
- des cours permettant l’accès à une carrière d’enseignant
- des études postuniversitaires 
- études de spécialisation

Les candidats ayant les connaissances requises en roumain et ne détenant pas de certificat de compétence linguistique peuvent demander de soutenir un examen au Centre Universitaire de Pitești.

<div class="boxes">
    <a href="/admissions/preparatory-year/description">Qu’est-ce que l’année préparatoire de langue roumaine ?</a> 
    <a href="/admissions/preparatory-year/admission-dossier">Dossier d’admission</a>
    <a href="/admissions/preparatory-year/how-to-apply">Comment puis-je m’inscrire ?</a> 
    <a href="/admissions/tuition-fees#Année-préparatoire-de-langue-roumaine">Frais de scolarité</a> 
    <a href="/admissions/visa-and-residence-permit">Visa et permis de séjour</a> 
    <a href="/admissions/preparatory-year/regulations">Réglementations</a> 
</div>

# Calendrier d’admission

- Date limite d’inscription: <span class="note">31 juillet <span class="this-year"></span></span> 
- Date limite pour l’immatriculation: <span class="note">20 décembre <span class="this-year"></span></span>  

# Contact

<div class="paragraph">
Mme. LEMNARU Ana Cristina, Référent du Bureau des étudiants internationaux 

Tél.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
