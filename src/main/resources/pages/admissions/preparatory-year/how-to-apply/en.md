Number of available places: <span class="note">150 places/year</span>.

# Apply

Please send all documents in the [Admission Dossier](/admissions/preparatory-year/admission-dossier) to [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro).


# Admission Process

1. Our University Centre analyses your Admission Dossier
2. After the initial screening, we forward your Admission Dossier to The Ministry of Education for validation
3. The Ministry of Education issues a **Letter of Acceptance to Studies** to successful candidates
4. Upon receiving the **Letter of Acceptance**, we update you on the outcome of the application

# Next Steps

## Study Visa

<span class="note-important">Important:</span> Successful candidates may need a **Study Visa** to study in Romania !

To apply for a Study Visa, the Romanian Diplomatic Mission will require the following documents:
1. Letter of Acceptance to Studies
2. Proof of payment for the relevant [Tuition Fees](/admissions/tuition-fees#Romanian-Language-Preparatory-Year)

Please visit [Visa and Residence Permit](/admissions/visa-and-residence-permit) for further information, guidance and support.

## Enrolment

<span class="note-important">Important:</span> To enrol, candidates must bring **in person** all original documents in the Admission Dossier to our International Students' Office.

These may include:
1. Certified Copies or Translations
2. Original Study Documents
3. The Letter of Acceptance from the Ministry of Education
4. A valid passport with a Study Visa

### Location

The [International Student's Office](https://g.page/official_upit) is located at [Room I.33, Rectorate, Building B, 1 Târgu din Vale Street, Pitești, Argeș, 110440, România](https://g.page/official_upit).
