Nombre de places disponibles: <span class="note">150 places/an</span>.

# Inscrivez-vous

Veuillez envoyer tous les documents du [Dossier d’admission](/admissions/preparatory-year/admission-dossier) à [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro).


# Processus d’admission 

1. Notre centre universitaire examine le Dossier d’admission
2. Après l’analyse initiale, nous transmettons le Dossier d’admission au Ministère de l’Éducation afin d’être validé 
3. Le Ministère de l’Éducation délivre une **Lettre d’acceptation aux études** pour les candidats admis 
4. Une fois que nous aurons reçu la **Lettre d’acceptation**, nous vous informerons du résultat du dossier d’admission

# Les pas suivants 

## Visa d’études 

<span class="note-important">Important :</span> Pour étudier en Roumanie, un **Visa d’études** pourrait être nécessaire !

Pour demander un Visa d’études, la Mission Diplomatique Roumaine vous demandera les documents suivants :
1. Lettre d’acceptation aux études
2. Justificatif de paiement [des Frais de scolarité](/admissions/tuition-fees#Année-préparatoire-de-langue-roumaine) afférents

Veuillez visiter la section [Visa et permis de séjour](/admissions/visa-and-residence-permit) pour plus d’informations, d’orientation et de soutien.


## Immatriculation

<span class="note-important">Important :</span> Pour faire leur immatriculation, les candidats doivent présenter **personnellement** tous les documents du dossier d’admission en original au Bureau des Étudiants Internationaux.

Parmi ceux-ci, on énumère :
1. Copies légalisées et/ou copies et traductions assermentées
2. Documents d’études 
3. Lettre d’acceptation délivrée par le Ministère de l’Éducation
4. Un passeport valide avec un visa d’études

### Adresse

[Bureau des Étudiants Internationaux](https://g.page/official_upit) est [Salle I.33, Rectorat, Bât B, Rue Târgu din Vale, no. 1, Pitești, Argeș, 110440, Roumanie](https://g.page/official_upit).



