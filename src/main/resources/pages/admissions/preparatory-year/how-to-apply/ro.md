Numărul de locuri disponibile: <span class="note">150 de locuri/an</span>.

# Aplicați

Vă rugăm să trimiteți toate documentele din [Dosarul de admitere](/admissions/preparatory-year/admission-dossier) la [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro).


# Procesul de admitere

1. Centrul nostru universitar analizează Dosarul de admitere
2. După analiza inițială, transmitem Dosarul de Admitere către Ministerul Educației pentru validare
3. Ministerul Educației eliberează o **Scrisoare de acceptare la studii** pentru candidații admiși
4. Odată ce primim **Scrisoarea de acceptare**, vă informăm cu privire la rezultatul dosarului de admitere

# Următorii pași

## Viza de studiu

<span class="note-important">Important:</span> Pentru a studiu în România, o **Viză de Studii** ar putea fi necesară !

Pentru a aplica pentru o Viză de Studii, Misiunea diplomatică Română vă va solicita următoarele documente:
1. Scrisoarea de acceptare la studii
2. Dovada plății [Taxelor de școlarizare](/admissions/tuition-fees#Anul-pregătitor-de-limba-română) aferente

Vă rugăm să vizitați secțiunea de [Viză și permis de ședere](/admissions/visa-and-residence-permit) pentru informații suplimentare, îndrumare și sprijin.


## Înmatriculare

<span class="note-important">Important:</span> Pentru a se înmatricula, candidații trebuie să prezinte **personal** toate documentele din dosarul de admitere în original la Biroul studenților internaționali.

Printre acestea se numără:
1. Copiile legalizate și/sau traducerile legalizate
2. Documente de studiu
3. Scrisoarea de acceptare eliberată de Ministerul Educației
4. Un pașaport în valabilitate cu viză de studiu

### Adresă

[Biroul studenților internaționali](https://g.page/official_upit) se află în [Sala I.33, Rectorat, Corp B, Strada Târgu din Vale, nr. 1, Pitești, Argeș, 110440, România](https://g.page/official_upit).



