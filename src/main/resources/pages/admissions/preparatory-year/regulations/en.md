# Current Regulations

- [Ordre no. 3873 of 05.05.2017](https://www.upit.ro/_document/28603/ordin_3873_modif_desfasurare_an_pregatitor_2017.pdf) on amending Ordre no. 6156/2016 of 22.12.2016 on the organisation and conduct of the Romanian Language Preparatory Year for foreign citizens
- Methodology for receiving and the schooling foreign citizens starting with the 2017/2018 school/academic year – [Annex to OMNE No. 3473 of 17.03.2017](https://www.upit.ro/_document/28604/ordin_3473-2017_metodologie_studenti_straini.pdf)
- [Ordnance no. 22 of 29.08.2009](https://www.upit.ro/_document/28605/ordonanta_22_cuantum_minim_taxe.pdf) on establishing the minimum tuition fees, in foreign currencies, of self-sustaining citizens studying in România

# Previous Regulations

- [OM no. 6000 of 15.10.2012](https://www.upit.ro/_document/10954/om_6000_15.10.2012_admirere_studii_cetateni_non_ue.pdf) on the admission of non-EU students
- [Methodology of March 2013](https://www.upit.ro/_document/10955/metodologie_admirere_studii_cetateni_non_ue_martie_2013.pdf) on the admission of non-EU students
- [OMECS no. 3782 of 2015](https://www.upit.ro/_document/10956/omecs_3782_metodologie_admirere_studii_cetateni_non_ue_2015.pdf) on the methodology for admission of non-EU students
- [The 2015 Methodology](https://www.upit.ro/_document/10957/metodologie_admirere_studii_cetateni_non_ue_2015.pdf) on the admission of non-EU students