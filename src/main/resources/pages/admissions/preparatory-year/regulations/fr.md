# Réglementations actuelles 

- [Arrêté nº 3873 du 05.05.2017](https://www.upit.ro/_document/28603/ordin_3873_modif_desfasurare_an_pregatitor_2017.pdf) pour la modification de l’Arrêté nº 6156/2016 du 22.12.2016 concernant l’organisation et le déroulement de l’Année préparatoire de langue roumaine pour les citoyens étrangers
- Méthodologie d’accueil et de scolarité des citoyens étrangers à partir de l’année scolaire/universitaire 2017/2018 - [Annexe à l’OMEN nº 3473 du 17.03.2017](https://www.upit.ro/_document/28604/ordin_3473-2017_metodologie_studenti_straini.pdf)
- [Arrêté nº 22 du 29.08.2009](https://www.upit.ro/_document/28605/ordonanta_22_cuantum_minim_taxe.pdf) fixant le montant minimum des frais de scolarité, en devises, pour les citoyens qui étudient sur leur propre compte en Roumanie

# Réglementations antérieures

- [OM nr. 6000 du 15.10.2012](https://www.upit.ro/_document/10954/om_6000_15.10.2012_admirere_studii_cetateni_non_ue.pdf) concernant l’admission des étudiants ressortissants des pays tiers à l’UE
- [Méthodologie de mars 2013](https://www.upit.ro/_document/10955/metodologie_admirere_studii_cetateni_non_ue_martie_2013.pdf) concernant l’admission des étudiants ressortissants des pays tiers à l’UE
- [OMECS nº 3782 du 2015](https://www.upit.ro/_document/10956/omecs_3782_metodologie_admirere_studii_cetateni_non_ue_2015.pdf) concernant la méthodologie d’admission des étudiants ressortissants des pays tiers à l’UE
- [Méthodologie de 2015](https://www.upit.ro/_document/10957/metodologie_admirere_studii_cetateni_non_ue_2015.pdf) concernant l’admission des étudiants ressortissants des pays tiers à l’UE
