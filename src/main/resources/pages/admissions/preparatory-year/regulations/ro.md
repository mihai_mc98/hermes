# Reglementări actuale

- [Ordinul nr. 3873 din 05.05.2017](https://www.upit.ro/_document/28603/ordin_3873_modif_desfasurare_an_pregatitor_2017.pdf) privind modificarea Ordinului nr. 6156/2016 din 22.12.2016 privind organizarea și desfășurarea Anului pregătitor de limba română pentru cetățenii străini
- Metodologia de primire și de școlarizare a cetățenilor străini începând cu anul școlar/academic 2017/2018 - [Anexă la OMEN nr. 3473 din 17.03.2017](https://www.upit.ro/_document/28604/ordin_3473-2017_metodologie_studenti_straini.pdf)
- [Ordinul nr. 22 din 29.08.2009](https://www.upit.ro/_document/28605/ordonanta_22_cuantum_minim_taxe.pdf) privind stabilirea taxelor minime de școlarizare, în valută, a cetățenilor care studiază în România pe cont propriu


# Reglementări anterioare

- [OM nr. 6000 din 15.10.2012](https://www.upit.ro/_document/10954/om_6000_15.10.2012_admirere_studii_cetateni_non_ue.pdf) privind admiterea studenților din afara UE
- [Metodologia din martie 2013](https://www.upit.ro/_document/10955/metodologie_admirere_studii_cetateni_non_ue_martie_2013.pdf) privind admiterea studenților din afara UE
- [OMECS nr. 3782 din 2015](https://www.upit.ro/_document/10956/omecs_3782_metodologie_admirere_studii_cetateni_non_ue_2015.pdf) privind metodologia de admitere a studenților non-UE
- [Metodologia din 2015](https://www.upit.ro/_document/10957/metodologie_admirere_studii_cetateni_non_ue_2015.pdf) privind admiterea studenților non-UE
