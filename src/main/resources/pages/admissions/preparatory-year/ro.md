Acest program se adresează candidaților care nu au cunoștințele necesare de limba română și care doresc să studieze în România:

- cursuri preuniversitare
- studii universitare (licență/masterat/doctorat)
- cursuri care permit accesul la o carieră didactică
- studii postuniversitare
- studii de specializare

Candidații care au cunoștințele necesare de limba română și care nu dețin un certificat de competență lingvistică pot solicita susținerea unui de examen la Centrul Universitar Pitești.

<div class="boxes">
    <a href="/admissions/preparatory-year/description">Ce este anul pregătitor de limba română ?</a>
    <a href="/admissions/preparatory-year/admission-dossier">Dosar de admitere</a>
    <a href="/admissions/preparatory-year/how-to-apply">Cum aplic ?</a>
    <a href="/admissions/tuition-fees#Anul-pregătitor-de-limba-română">Taxe de școlarizare</a>
    <a href="/admissions/visa-and-residence-permit">Viza și permisul de ședere</a>
    <a href="/admissions/preparatory-year/regulations">Reglementări</a>
</div>

# Calendar admitere

- Data limită pentru înscriere: <span class="note">31 iulie <span class="this-year"></span></span>
- Data limită pentru înmatriculare: <span class="note">20 decembrie <span class="this-year"></span></span>

# Contact

<div class="paragraph">
Dna. LEMNARU Ana Cristina, Referent al Biroului studenților internaționali

Nr. Tel.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
