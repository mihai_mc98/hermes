Dacă vrei să studiezi la Centrul Universitar Pitești și să ai o diplomă recunoscută pe tot teritoriul UE, trebuie să te informezi. Centrul Universitar Pitești are o tradiție de 50 de ani de școlarizare a studenților internaționali. În funcție de țara de origine, îți oferim diferite modalități pentru a deveni student.

<div class="cards">
    <div class="card">
        <img src="/images/Cards/Notebook.jpg" alt="Someone writing in a Notebook">
        <div class="card-content">
            <a href="/admissions/preparatory-year">Anul pregătitor de limba română</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Friends in Tulip Field.jpg" alt="Friends in a Tulip Field">
        <div class="card-content">
            <a href="/admissions/romanians-everywhere">Români de pretutindeni</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Library.jpg" alt="Students in a Library">
        <div class="card-content">
            <a href="/admissions/non-EU-students">Studenți non-UE</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Look at what I made.jpg" alt="People around a Computer">
        <div class="card-content">
            <a href="/admissions/EU-students">Studenți UE</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Bills and Fees.jpg" alt="Taxe">
        <div class="card-content">
            <a href="/admissions/tuition-fees">Taxe de școlarizare</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Passport and Visa.jpg" alt="Pașaport și viză">
        <div class="card-content">
            <a href="/admissions/visa-and-residence-permit"> Viză și permis de ședere </a>
        </div>
    </div>
</div>

# Descoperă Centrul Universitar Pitești

* [Ghidul studentului internațional (EN)](https://www.upit.ro/_document/175051/international_students_guide.pdf)
* [Broșură de prezentare a centrului universitar (AR)](https://www.upit.ro/_document/193777/2022_brosura_de_prezentare_ar.pdf)

# Documente utile

* [Regulamentul studenților internaționali](https://www.upit.ro/_document/112151/regulament_studenti_internationali_senat_27.07.2020.pdf)
* [Regulamente și proceduri](https://www.upit.ro/ro/international/cpri/regulamente-si-proceduri)

# Contact

<div class="paragraph">
Dna. LEMNARU Ana Cristina, Referent al Biroului studenților internaționali

Nr. Tel.: [+40 348 453 334](tel:+40348453334)

E-Mail: [internationalstudents@upit.ro](mailto:internationalstudents@upit.ro)
</div>
