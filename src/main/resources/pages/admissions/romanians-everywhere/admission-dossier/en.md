<div class="info-box-ukraine">
   <div>
      <h2>Admission Changes for Ukrainian Citizens</h2>
   </div>
    <div>
      Please read the <a href="https://www.upit.ro/_document/178691/instructiuni_dgriae_08.06.2022_admitere_romanii_de_pretutindeni_2022-2023.pdf">new instructions</a> for admission of Ukrainian citizens via the Romanians Everywhere programme. For further information, please do not hesitate to <a href="/admissions/romanians-everywhere#Contact">contact us</a> !
    </div>
</div>

The Admission Dossier consists of a number of documents, grouped by type:

<table-of-contents depth="1" lang="en"></table-of-contents>

In the interest of avoiding delays, please double-check that all your documents adhere to the following guidelines **before** submitting your dossier.

If unsure of the requirements, please contact our [Romanians Everywhere Office](/admissions/romanians-everywhere#Contact)

<div class="info-box">

## What you need to do depends on the language in which the document was issued

* For documents issued in Romanian, French or English, please include a <span class="note">certified copy</span> in the admission dossier
* For documents issued in any other languages, please translate them into Romanian, French or English and include the <span class="note">certified translation</span> in the admission dossier

</div>

# Identity Documents

1. Birth Certificate
2. National ID Card (if applicable)
3. Proof of Name Change (if necessary)

## Passport

<div class="info-box">

## Update on Passport Validity

* 🇲🇩 Republic of Moldova: Passports from the Republic of Moldova that expire between **01.01.2020** – **31.12.2022** are deemed to be valid until **30.06.2023**
* 🇺🇦 Ukraine: While the war in Ukraine continues, Ukrainian citizens can extend their passport validity at the Ukrainian Diplomatic Missions

</div>

Please observe the specific requirements related to passports:

* Only a <span class="note">scanned copy</span> of pages 1, 2 and 3 need be submitted
* The passport must be valid for at least another **6 months** starting from <span class="note">October 1st</span>

# Medical Certificate

1. It must be **issued** in Romanian, French or English
2. It must mention that the applicant does not suffer from:
    * Any contagious diseases
    * Other afflictions deemed incompatible with the future profession

<span class="note">Note: </span> This document is not required for candidates:
* Applying for Ph.D. programmes
* From the Transnistria Region

# Study Documents

Please consult the [List of Recognised Degrees and Diplomas](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) to ensure your documents are recognised by the Ministry of Education.

1. [Study Request Application Form](https://www.upit.ro/_document/113165/anexa2_alb_mac_serb_ukr.pdf) – **only** for citizens of Albania, Macedonia, Ukraine and Serbia
2. Degrees and Diplomas (or [Provisional Certificates](#Provisional-Certificates))
    * Baccalaureate Diploma and associated grades
    * Bachelor's Degree (if appropriate)
    * Master's Degree (if appropriate)
3. Academic Records for all submitted documents showing the GPAs for the years of study

<span class="note">Note:</span> The Baccalaureate Diploma must certify the successful completion of the relevant [National School Leaving Examinations](https://www.upit.ro/_document/27076/anexa_5_lista_diplome.pdf) 

## Provisional Certificates

Strictly whenever candidates cannot provide the relevant study document due to delays in issuing by the respective institution, our University Centre will accept a Provisional Certificate. Such certificates must:
* be signed and/or stamped
* be issued by the Institution where the Examination/Degree was held
* certify that the Candidate has passed the relevant Exams
* mention the grades that would be written on the Diploma

Regardless of the reason, Provisional Certificates are only valid for **up to 1 year** after the successful completion of the relevant studies. Whenever the study document is issued, the Provisional Certificate automatically becomes **null and void**.

<span class="note">Note:</span> All Provisional Certificates must be replaced by the original underlying study documents.

# Language Requirements

All candidates must submit proof of good command of the Romanian language: at least **level B1** (Common European Framework of Reference for Languages) for the following components:
   * Reading
   * Writing
   * Listening
   * Speaking

<span class="note-important">Important Note:</span> Candidates from Bulgaria, Hungary and Serbia who did not attend High-School in Romania, must provide language certificates issued by one of the following:
   * The Romanian Diplomatic Missions in the country of residence
   * an Accredited Educational Institution in the country of residence

Please familiarise yourself with the [Language Requirements](/admissions/romanians-everywhere/language-requirements) needed to apply via the Romanians Everywhere Programme.

# Programme‑Specific Requirements

Depending on the study programme, there may be specific requirements that need to be observed. Please consult the following list before submitting your admission dossier:

1. [Bachelor's and Master's Degrees](#Bachelor's-and-Master's-Degrees)
2. [Ph.D. Degrees](#Ph.D.-Degrees)
3. [Theology Degrees](#Theology-Degrees)
4. [Faculty‑Specific Requirements](#Faculty‑Specific-Requirements)

## Bachelor's and Master's Degrees

All candidates to Bachelor's and Master's Degrees must submit a Personal Statement Essay that tests your knowledge and cognitive skills.

This Essay must be written in accordance with the faculty-specific requirements.

## Ph.D. Degrees

Applicants to Ph.D. Degrees must submit copies after their:
1. Curriculum Vitæ
2. List of Published Papers

<span class="note-important">Important note:</span> Both documents must bear the applicant's handwritten signature

## Theology Degrees

Candidates applying for Theology Degrees must either have:
* the Recommendation of the Religious Hierarchies
* the Consent of Local Religious Leaders

## Faculty‑Specific Requirements

Please consult the website of the Faculty where you are applying. Alternatively, you may also contact our [Romanians Everywhere Office](/admissions/romanians-everywhere#Contact).

# Affidavits and Forms

Under the Romanians Everywhere programme, candidates are required to submit the following documents :
1. [Romanian Identity Affidavit](#Romanian-Identity-Affidavit)
2. [GDPR Form](#GDPR-Form)
3. [State‑Funded Subsidies Affidavit](#State‑Funded-Subsidies-Affidavit)

## Romanian Identity Affidavit

All candidates applying via the Romanians Everywhere programme must submit an [Affidavit undertaking the Romanian Cultural Identity](https://www.upit.ro/_document/27074/anexa_3_declaratie_apartenenta.pdf).

This declaration must be certified by one of the following institutions:
* The diplomatic mission of Romania in the country of origin
* The Department for Romanians Everywhere

<span class="note">Note: </span> This document is not required for candidates:
* Citizens of the Republic of Moldova
* Citizens of Romania residing abroad

## GDPR Form

Candidates must sign and submit [Consent Declaration Form and the Information Notice](https://www.upit.ro/_document/114399/anexa_5_declaratie_consimtamant_si_nota_de_informare.pdf) regarding the processing of personal data in accordance with the General Data Protection Regulations (GDPR) in the EU.

## State‑Funded Subsidies Affidavit

The candidate must submit an affidavit certifying whether they had ever benefited **in Romania** from a State-Funded Subsidy for: 
* Bachelor's Degrees
* Master's Degrees
* Ph.D. Degrees

This affidavit must be accompanied by a certificate issued by the respective Institution.