<div class="info-box-ukraine">
    <div>
      <h2>Changements de procedure d’admission pour les citoyens ukrainiens</h2>
   </div>
    <div>
      Veuillez lire les <a href="https://www.upit.ro/_document/178691/instructiuni_dgriae_08.06.2022_admitere_romanii_de_pretutindeni_2022-2023.pdf">nouvelles instructions</a> pour l’admission des citoyens ukrainiens via le programme Roumains de partout. Pour plus d’informations, n’hésitez pas à <a href="/admissions/romanians-everywhere#Contact">nous contacter</a> !
   </div>
</div>

Le dossier d’admission est composé d’un certain nombre de documents, regroupés par catégories :

<table-of-contents depth="1" lang="fr"></table-of-contents>

Pour éviter les retards, veuillez vérifier que tous les documents sont conformes aux exigences suivantes **avant** d’envoyer votre dossier

Si vous n’êtes pas sûr des exigences, veuillez contacter le [Bureau des Roumains de partout](/admissions/romanians-everywhere#Contact)

<div class="info-box">

## Les exigences diffèrent selon la langue du document

* Pour les documents délivrés en roumain, en français ou en anglais, veuillez inclure une <span class="note">copie légalisée</span> dans le dossier d’admission
* Pour les documents délivrés dans toute autre langue, veuillez les traduire en roumain, en français ou en anglais et inclure la <span class="note">traduction assermentée</span> dans le dossier d’admission

</div>

# Documents d’identité

1. Acte de naissance
2. Carte d’identité (le cas échéant)
3. Le justificatif de changement du nom (le cas échéant)

## Passeport

<div class="info-box">

## Nouvelles dispositions sur la validité des passeports

* 🇲🇩 République de la Moldavie : Les passeports de la République de Moldavie qui expirent entre le **01.01.2020** et le **31.12.2022** se prolongent de plein droit jusqu’au **30.06.2023**
* 🇺🇦 Ukraine : Pendant l’état de guerre en Ukraine, les citoyens ukrainiens peuvent prolonger la validité de leur passeport au siège des missions diplomatiques ukrainiennes

</div>

Veuillez accorder une attention accrue aux exigences spécifiques liées aux passeports :

* Il est demandé une <span class="note">copie scannée</span> des pages 1, 2 et 3
* Le passeport doit avoir une validité d’au moins **6 mois** à compter du <span class="note">1er octobre</span>.

# Certificat médical

1. Doit être **délivré** en roumain, anglais, français
2. Doit confirmer que l’étudiant ne souffre pas :
    * De maladies contagieuses
    * D’autres affections incompatibles avec sa future profession

<span class="note">Note : </span> Ce document **n’** est pas nécessaire pour les candidats:
* aux programmes de doctorat
* de la région Transnistrie

# Documents d’études

Veuillez consulter la [Liste des études et diplômes reconnus](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pour s’assurer que vos documents sont reconnus par le Ministère de l’Éducation.

1. [Demande d’inscription aux études](https://www.upit.ro/_document/113165/anexa2_alb_mac_serb_ukr.pdf) - **seulement** pour les citoyens d’Albanie, de Macédoine, d’Ukraine et de Serbie
2. Diplôme (ou [Attestation d’études](#Attestation-d’études))
    * Diplôme de baccalauréat
    * Diplôme de licence (le cas échéant)
    * Diplôme de master (le cas échéant)
3. Relevés de notes pour tous les documents présentés présentant les moyennes générales pour les années d’études 

<span class="note">Note :</span> Le Diplôme de baccalauréat doit attester la réussite des [examens nationaux de fin d’études obligatoires](https://www.upit.ro/_document/27076/anexa_5_lista_diplome.pdf)

## Attestation d’études

Le centre universitaire ne peut accepter les attestations d’études que lorsqu’il y a des retards dans la délivrance des documents d’étude en original. Ces attestations doivent :
* être signées et/ou tamponnées 
* être délivrées par l’institution où l’examen a été passé
* attester que le candidat a réussi les examens pertinents
* mentionner les notes qui seront inscrites sur le diplôme

Quelle que soit la raison, les attestations ne sont valables qu’**un an** depuis la fin des études. Dès que le diplôme d’études est délivré, l’Attestation devient automatiquement **nulle et non avenue**.

<span class="note">Note :</span> Les attestations d’études ne sont qu’une solution temporaire ! Les diplômes originaux doivent être présentés pour la poursuite des études.

# Exigences linguistiques

Les candidats doivent prouver qu’ils maîtrisent le roumain : au moins au **niveau B1** (Cadre Européen Commun de Référence pour les Langues) pour les compétences : 
   * Compréhension des écrits
   * Expression écrite
   * Compréhension de l’oral
   * Expression orale

<span class="note-important">Note importante :</span> Les candidats de Bulgarie, de Hongrie et de Serbie, qui n’ont pas suivi les études secondaires en Roumanie, doivent présenter des certificats linguistiques délivrés par :
   * les missions diplomatiques de la Roumanie dans le pays de résidence
   * un établissement d’enseignement accrédité dans le pays de résidence

Veuillez vous familiariser avec les [Exigences linguistiques](/admissions/romanians-everywhere/language-requirements) requises pour postuler par le biais du programme les Roumains de partout.

# Exigences spécifiques au programme

Selon le programme d’études, il peut y avoir des exigences spécifiques pour l’admission. Veuillez consulter la liste suivante avant de soumettre votre dossier :

1. [Études de licence et de master](#Études-de-licence-et-de-master)
2. [Études de doctorat](#Études-de-doctorat)
3. [Programmes Théologie](#Programmes-Théologie)
4. [Exigences spécifiques des facultés](#Exigences-spécifiques-des-facultés)

## Études de licence et de master

Les candidats aux programmes de licence et de master doivent soumettre un Essai de Motivation qui teste vos connaissances et vos capacités cognitives.

Cet essai doit être rédigé conformément aux exigences spécifiques des facultés.

## Études de doctorat

Les candidats aux études universitaires de doctorat doivent présenter des copies de :
1. Curriculum Vitæ
2. Liste des articles publiés

<span class="note-important">Note importante :</span> Les deux documents doivent avoir la signature olographe du candidat

## Programmes Théologie

Les candidats qui postulent des programmes d’études en Théologie doivent avoir :
* la recommandation des hiérarques religieux
* le consentement des chefs religieux locaux

## Exigences spécifiques des facultés

Veuillez consulter le site Web de la Faculté où vous souhaitez postuler. Vous pouvez également contacter le [Bureau des Roumains de partout](/admissions/romanians-everywhere#Contact).

# Déclarations sur l’honneur et formulaires 

Dans le cadre du programme les Roumains de partout, les candidats doivent présenter les documents suivants :
1. [Déclaration sur l’honneur pour s’assumer l’identité roumaine](#S’assumer-l’identité-roumaine)
2. [Formulaire GDPR](#Formulaire-GDPR)
3. [Déclaration sur l’honneur relative aux subventions financées par l’État](#Subventions-d’État)

## S’assumer l’identité roumaine

Les candidats qui postulent par le biais du programme les Roumains de partout doivent soumettre une [Déclaration sur l’honneur pour l’identité culturelle roumaine](https://www.upit.ro/_document/27074/anexa_3_declaratie_apartenenta.pdf).

Cette déclaration doit être légalisée par l’un des établissements suivants :
* Mission diplomatique de la Roumanie dans son pays d’origine
* Département des Roumains de partout

<span class="note">Note : </span> Ce document n’est pas nécessaire pour les citoyens:
* de la République de Moldavie 
* Roumains domiciliés à l’étranger

## Formulaire GDPR

Les candidats doivent signer et envoyer le [Formulaire de déclaration de consentement et la Note d’information](https://www.upit.ro/_document/114399/anexa_5_declaratie_consimtamant_si_nota_de_informare.pdf) concernant le traitement des données à caractère personnel conformément au Règlement général sur la protection des données (RGPD) dans l’UE.

## Subventions d’État

Le candidat doit déposer une déclaration sur l’honneur indiquant s’il a déjà bénéficié **en Roumanie** d’une subvention financée par l’État pour les : 
* Études de licence
* Études de master
* Études de doctorat

Cette déclaration doit être accompagnée d’une attestation délivrée par les établissements concernés.	
