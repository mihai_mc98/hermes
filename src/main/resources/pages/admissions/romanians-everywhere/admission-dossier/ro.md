<div class="info-box-ukraine">
    <div>
      <h2>Procedură de admitere diferită pentru cetățenii ucrainieni</h2>
   </div>
    <div>
      Vă rugăm să consultați <a href="https://www.upit.ro/_document/178691/instructiuni_dgriae_08.06.2022_admitere_romanii_de_pretutindeni_2022-2023.pdf">noile instrucțiuni</a> cu privire la admiterea cetățenilor ucrainieni prin programul Românii de pretutindeni. Pentru mai multe informații, vă rugăm să <a href="/admissions/romanians-everywhere#Contact">ne contactați</a> !
   </div>
</div>

Dosarul de admitere este alcătuit dintr-un număr de documente, grupate pe categorii:

<table-of-contents depth="1" lang="ro"></table-of-contents>

Pentru a evita întârzierile, vă rugăm să verificați dacă toate documentele respectă următoarele cerințe **înainte** de a trimite dosarul.

Dacă nu sunteți sigur de cerințe, vă rugăm să contactați [Biroul Românilor de Pretutindeni](/admissions/romanians-everywhere#Contact)

<div class="info-box">

## Cerințele diferă în funcție de limba documentul a fost emis

* Pentru documentele eliberate în limba română, franceză sau engleză, vă rugăm să includeți o <span class="note">copie legalizată</span> în dosarul de admitere
* Pentru documentele eliberate în orice altă limbă, vă rugăm să le traduceți în română, franceză sau engleză și să includeți <span class="note">traducerea legalizată</span> în dosarul de admitere

</div>

# Documente de identitate

1. Certificatul de naștere
2. Carte de identitate (dacă este cazul)
3. Dovada schimbării numelui (dacă este necesar)

## Pașaport

<div class="info-box">

## Noi prevederi privind valabilitatea pașapoartelor

* 🇲🇩 Republica Moldova: Pașapoartele cetățenilor din Republica Moldova care expiră între **01.01.2020** și **31.12.2022** se prelungesc de drept până la data de **30.06.2023**
* 🇺🇦 Ucraina: Pe durata stării de război din Ucraina, cetățenii Ucraineni îsi pot extinde valabilitatea pașapoartelor la sediul misiunilor diplomatice ale Ucrainei

</div>

Vă rugăm să acordați o atenție sporită cerințelor specifice pentru pașapoarte:

* Este necesară numai o <span class="note">copie scanată</span> a paginilor 1, 2 și 3
* Pașaportul trebuie să fie valabil încă cel puțin **6 luni** de la data de <span class="note">1 octombrie</span>.

# Adeverință medicală

1. Trebuie să fie **emisă** în limba română, franceză sau engleză
2. Trebuie să se menționeze că studentul nu suferă de:
    * Orice boală contagioasă
    * Alte afecțiuni considerate incompatibile cu viitoarea profesie

<span class="note">Notă: </span> Acest document **nu** este necesar pentru candidații:
* Care candidează la programe de doctorat
* Din regiunea Transnistria

# Documente de studiu

Vă rugăm să consultați [Lista de studii și diplome recunoscute](https://www.upit.ro/_document/28343/lista_diplomelor_de_studii_liceale_recunoscute_de_m.e.n._1.doc) pentru a vă asigura că documentele dumneavoastră sunt recunoscute de către Ministerul Educației.

1. [Cerere de aplicare la studii](https://www.upit.ro/_document/113165/anexa2_alb_mac_serb_ukr.pdf) - **numai** pentru cetățenii din Albania, Macedonia, Ucraina și Serbia
2. Diplome (sau [Adeverință de studiu](#Adeverințe-de-studiu))
    * Diploma de bacalaureat
    * Diploma de licență (dacă este cazul)
    * Diplomă de master (dacă este cazul)
3. Foile matricole pentru toate documentele prezentate, din care să reiasă mediile generale pentru anii de studiu

<span class="note">Notă:</span> Diploma de bacalaureat trebuie să ateste absolvirea cu succes a [examenelor naționale de absolvire învățământului obligatoriu](https://www.upit.ro/_document/27076/anexa_5_lista_diplome.pdf)

## Adeverințe de studiu

Centrul universitar poate accepta adeverințe de studiu, numai când există întârzieri în eliberarea documentelor de studiu originale. Astfel de adeverințe trebuie să:
* fie semnate și/sau ștampilate 
* fie eliberate de instituția unde s-a susținut examenul
* ateste faptul că studentul a promovat examenele relevante
* menționeze notele care vor fi înscrise pe diplomă

Indiferent de motiv, adeverințele sunt valabile doar **1 an** de la finalizarea studiilor. Odată ce diploma de studii este eliberată, Adeverința devine automat **nulă de drept**.

<span class="note">Notă:</span> Adeverințele de studiu nu sunt decât o soluție temporară ! Diplomele originale trebuie prezentate pentru continuarea studiilor.

# Cerințe lingvistice

Candidații trebuie să dovedească bune cunoștințe de limbă române: cel puțin **nivelul B1** (Cadrul European Comun de Referință pentru Limbi) pentru următoarele componente:
   * Citire
   * Scriere
   * Ascultare
   * Exprimare orală

<span class="note-important">Nota importantă:</span> Candidații din Bulgaria, Ungaria și Serbia, care nu au urmat cursurile liceale în România, trebuie să prezinte certificate lingvistice eliberate de fie:
   * misiunile diplomatice ale României din țara de reședință
   * o instituție de învățământ acreditată din țara de reședință

Vă rugăm să vă familiarizați cu [Cerințele lingvistice](/admissions/romanians-everywhere/language-requirements) necesare pentru a aplica prin Programul Românii de Pretutindeni.

# Cerințe specifice programului

În funcție de programul de studii, pot exista cerințe specifice pentru admitere. Vă rugăm să consultați următoarea listă înainte de a vă depune dosarul:

1. [Studii de licență și de masterat](#Studii-de-licență-și-masterat)
2. [Studii de doctorat](#Studii-de-doctorat)
3. [Programe Teologie](#Programe-Teologie)
4. [Cerințe specifice ale facultăților](#Cerințe-specifice-ale-facultăților)

## Studii de licență și masterat

Candidații la programele de licență și masterat trebuie să trimită un Eseu Motivațional care vă testează cunoștințele și abilitățile cognitive.

Acest eseu trebuie să fie redactat în conformitate cu cerințele specifice ale facultăților.

## Studii de doctorat

Candidații la studiile universitare de doctorat trebuie să prezinte copii după:
1. Curriculum Vitæ
2. Lista lucrărilor publicate

<span class="note-important">Nota importantă:</span> Candidatul trebuie să semneze olograf ambele documente

## Programe Teologie

Candidații care aplică pentru programe de studiu de Teologie trebuie fie să aibă:
* recomandarea ierarhilor religioși
* consimțământul liderilor religioși locali

## Cerințe specifice ale facultăților

Vă rugăm să consultați site-ul web al Facultății unde doriți să aplicați. Puteți contacta, de asemenea, și [Biroul Românilor de Pretutindeni](/admissions/romanians-everywhere#Contact).

# Declarații pe proprie răspundere și formulare

În cadrul programului Românii de pretutindeni, candidații trebuie să prezinte următoarele documente:
1. [Declarație pe propria răspundere privind asumarea identității române](#Asumarea-identității-române)
2. [Formularul GDPR](#Formularul-GDPR)
3. [Declarație pe propria răspundere privind subvențiile finanțate de stat](#Subvențiile-de-stat)

## Asumarea identității române

Candidații care aplică prin programul Românii de Pretutindeni trebuie să depună o [Declarație pe propria răspundere privind identitatea culturală română](https://www.upit.ro/_document/27074/anexa_3_declaratie_apartenenta.pdf).

Această declarație trebuie să fie legalizată de către una dintre următoarele instituții:
* Misiunea diplomatică a României în țara de origine
* Departamentul pentru Românii de Pretutindeni

<span class="note">Notă: </span> Acest document nu este necesar pentru cetățenii:
* Republicii Moldova
* Români cu domiciliul în străinătate

## Formularul GDPR

Candidații trebuie să semneze și să trimită [Declarația de consimțământ și Nota de informare](https://www.upit.ro/_document/114399/anexa_5_declaratie_consimtamant_si_nota_de_informare.pdf) privind prelucrarea datelor cu caracter personal în conformitate cu Regulamentul general privind protecția datelor (GDPR) în UE.

## Subvențiile de stat

Candidatul trebuie să depună o declarație pe proprie răspundere prin care să ateste dacă a beneficiat vreodată **în România** de o subvenție finanțată de stat pentru: 
* Studii de licență
* Studii de masterat
* Studii de doctorat

Această declarație trebuie să fie însoțită de o adeverință eliberată de instituțiile respective.
