f# Eligibility Requirements

Under the Romanians Everywhere programme, you must freely assume the Romanian cultural identity and must either be:
* a Romanian citizens with a permanent address or residence abroad
* an emigrant abroad (or their descendant), regardless of retaining the Romanian citizenship
* of a Romanian origin or belonging to the Romanian linguistic and cultural background, outside the borders of Romania

Please [contact us](#Contact) if you are unsure whether your status qualifies under the Romanians Everywhere programme.

<div class="info-box">

## Who has a Romanian linguistic and cultural background ?

We provide a **non-comprehensive** list of frequently used ethnonymes: armâni, armânji, aromâni, basarabeni, bucovineni, cuțovlahi, daco-români, fărşeroţi, herţeni, istroromâni, latini dunăreni, macedoromâni, macedo-români, maramureşeni, megleniţi, megleno-români, moldoveni, moldovlahi, rrămâni, rumâni, valahi, vlahi, vlasi, volohi, macedo-armânji

</div>

# Apply via the Romanians Everywhere Programme

## How to Apply

Please familiarise yourself with the [Language Requirements](/admissions/romanians-everywhere/language-requirements) needed to apply via the Romanians Everywhere Programme.

Applicants via the Romanians Everywhere Programme must submit the [admission dossier](/admissions/romanians-everywhere/admission-dossier) to: [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

<div class="boxes">
    <a href="/admissions/romanians-everywhere/language-requirements">Language Requirements</a>
    <a href="/admissions/romanians-everywhere/admission-dossier">Application Dossier</a>
    <a href="/admissions/preparatory-year">Romanian Language Preparatory Year</a>
    <a href="/admissions/tuition-fees#Romanians-Everywhere-Programme">Tuition Fees</a>
    <a href="/admissions/visa-and-residence-permit#Romanians-Everywhere">Study Visa and Residence Permit</a>
    <a href="/admissions/romanians-everywhere/regulations">Regulations</a>
</div>

## Available Places

The following figures are valid for the 2023-2024 Academic Year.

<table>
    <thead>
        <tr>
            <th></th>
            <th>Bachelor's Degree</th>
            <th>Master's Degree</th>
            <th>Ph.D. Degree</th>
            <th>Romanian Language Preparatory Year</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>WITH Scholarship</td>
            <td>20</td>
            <td>7</td>
            <td>1</td>
            <td>-</td>
        </tr>
        <tr>
            <td>WITHOUT Scholarship</td>
            <td>55</td>
            <td>21</td>
            <td>-</td>
            <td>4</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>TOTAL</td>
            <td>75</td>
            <td>28</td>
            <td>1</td>
            <td>4</td>
        </tr>
    </tfoot>
</table>


## Application Decision

Please check the status of your application with the faculty you have chosen.

<span class="note-important">Important:</span> Successful applicants must confirm their place by submitting:
1. The [Choice Confirmation Form](https://www.upit.ro/_document/27075/anexa_4_confirmare_loc_studii.pdf)
2. The original study documents during Enrolment

## Useful Resources

* [Romanians Everywhere Admission Guide (EN)](https://www.upit.ro/_document/175736/2022_ghid_romanii_de_pretutindeni_en.pdf)
* [Video – Frequently Asked Questions about the Application Process](https://educatiacontinua.edu.ro/source/Intrebari%20raspunsuri%20universitar%202021.mp4)

# Extra-Curricular

Under the Romanians Everywhere Programme, you will also have the opportunity to:
* take part in a range of activities to discover Romania
* network with potential employers

# Contact

<div class="paragraph">

Ms. MIHAI Liliana, Secretary of International Relations

Phone: [+40 348 453 108](tel:+40348453108)

E-mail: [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

</div>

## Contact the Ministry of Education

Young people in the Republic of Moldova should contact the Ministry at [dosarerepmold@edu.gov.ro](mailto:dosarerepmold@edu.gov.ro).

Romanians from neighbouring countries and diaspora should contact the Ministry at [dosarerompret@edu.gov.ro](mailto:dosarerompret@edu.gov.ro).