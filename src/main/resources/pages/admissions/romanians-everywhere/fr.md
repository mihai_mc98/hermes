# Critères d’admissibilité

Dans le cadre du programme les Roumains de partout, vous devez vous assumer librement votre identité culturelle roumaine et vous devez être soit :
* citoyen roumain ayant son domicile ou sa résidence permanente à l’étranger
* émigrant à l’étranger (ou un de ses descendants), que la nationalité roumaine ait été conservée ou non
* d’origine roumaine ou appartenant à l’environnement linguistique et culturel roumain, en dehors des frontières de la Roumanie

Veuillez nous [contacter](#Contact) si vous n’êtes pas sûr que votre statut relève du programme les Roumains de partout.

<div class="info-box">

## Qu’est-ce que cela suppose d’être d’un contexte linguistique et culturel roumain ?

Nous mettons à votre disposition une liste **non exhaustive** d’ethnonymes couramment utilisés: Armâni, Armânji, Aroumains, Bessarabes, Bucoviniens, Cuatovlahi, Daco-Roumains, Fărşeroţi, Herţeni, Istro-Roumains, Latins Danubiens, Macédoniens, Maramureşeni, Megleniţi, Megleno-Roumains, Moldaves, Moldovlahi, Rrămâni, Rumâni, Wallachian, Valaques, Vlasi, Volohi, Macedo-Armânji
</div>

# Postuler par le biais du Programme les Roumains de partout

## Comment postuler

Veuillez vous familiariser avec les [Exigences linguistiques](/admissions/romanians-everywhere/language-requirements) nécessaires pour postuler par le biais du programme les Roumains partout
Les candidats doivent envoyer le [dossier d’admission](/admissions/romanians-everywhere/admission-dossier) à l’adresse : [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

<div class="boxes">
    <a href="/admissions/romanians-everywhere/language-requirements">Exigences linguistiques</a>
    <a href="/admissions/romanians-everywhere/admission-dossier">Dossier d’admission</a>
    <a href="/admissions/preparatory-year">Année préparatoire de langue roumaine</a>
    <a href="/admissions/tuition-fees#Les-Roumains-de-partout">Frais de scolarité</a>
    <a href="/admissions/visa-and-residence-permit#Les-Roumains-de-partout">Visa d’études et permis de séjour</a>
    <a href="/admissions/romanians-everywhere/regulations">Réglementations</a>
</div>

## Places disponibles

Les données suivantes sont valables pour l’année universitaire 2023-2024.

<table>
    <thead>
        <tr>
            <th></th>
            <th>Licence</th>
            <th>Master</th>
            <th>Doctorat</th>
            <th>Année préparatoire de langue roumaine</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>AVEC bourse</td>
            <td>20</td>
            <td>7</td>
            <td>1</td>
            <td>-</td>
        </tr>
        <tr>
            <td>SANS bourse</td>
            <td>55</td>
            <td>21</td>
            <td>-</td>
            <td>4</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>TOTAL</td>
            <td>75</td>
            <td>28</td>
            <td>1</td>
            <td>4</td>
        </tr>
    </tfoot>
</table>


## Décision du dossier d’admission

Veuillez contacter la faculté où vous avez postulé afin de connaître l’état de votre dossier.

<span class="note-important">Important:</span> Les candidats admis doivent confirmer leur place en envoyant:
1. [Formulaire de confirmation de la place d’études](https://www.upit.ro/_document/27075/anexa_4_confirmare_loc_studii.pdf)
2. Documents d’étude en original lors de l’immatriculation

## Ressources utiles

* [Guide d’admission pour les Roumains de partout (EN)](https://www.upit.ro/_document/175736/2022_ghid_romanii_de_pretutindeni_en.pdf)
* [Vidéo – Foire aux questions sur le processus d’inscription](https://educatiacontinua.edu.ro/source/Intrebari%20raspunsuri%20universitar%202021.mp4)

# Activités extra-curriculaires

Dans le cadre du programme les Roumains de partout, vous aurez l’occasion de :
* participer à des activités diverses pour découvrir la Roumanie
* prendre contact avec de potentiels employeurs 

# Contact

<div class="paragraph">

Mme. MIHAI Liliana, Secrétaire Relations Internationales

Tél: [+40 348 453 108](tel:+40348453108)

E-mail: [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

</div>

## Contact Ministère de l’Éducation

Les jeunes de la République de Moldavie doivent contacter le Ministère à l’adresse [dosarerepmold@edu.gov.ro](mailto:dosarerepmold@edu.gov.ro).

Les Roumains des pays voisins de la Roumanie et de la diaspora doivent contacter le Ministère à l’adresse [dosarerompret@edu.gov.ro](mailto:dosarerompret@edu.gov.ro).
