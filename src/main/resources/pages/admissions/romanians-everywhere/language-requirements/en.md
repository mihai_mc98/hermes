<span class="note">Note:</span> In order to study at our University Centre, students must prove a minimum of **level B1** according to the Common European Framework of Reference.

# Students with Romanian Language skills

If you have acquired Romanian language skills in non-formal or informal contexts, you can request to sit a Romanian Language Proficiency Exam organised by either:
* The University Centre of Pitești
* The Romanian Language Lectureships of the Romanian Language Institute
* The Romanian Cultural Institute

# Students without any Romanian language skills

Students with no Romanian Language skills must enrol in the [Romanian Language Preparatory Year](/admissions/preparatory-year) after receiving the Letter of Acceptance from the Ministry of Education.

Upon successful graduation of the Preparatory Year, students may continue their studies the next academic year at the chosen level (Bachelor's Degree/ Master's Degree/PhD).

# Exemptions

Students may be exempt from proving proficiency in the Romanian language if they either:
* have a Romanian Language Certificate/Diploma/Degree
* have studied in Romanian for at least four consecutive years at an educational establishment/institution within the Romanian National System of Education or abroad