<span class="note">Note :</span> Pour étudier à notre centre universitaire, les étudiants doivent maîtriser la langue roumaine au **niveau B1** minimum, conformément au Cadre Européen Commun de Référence.

# Étudiants ayant des connaissances de langue roumaine

Si vous avez acquis les connaissances de langue roumaine dans des contextes non formels ou informels, vous pouvez solliciter à passer un examen de compétences linguistiques en roumain, qui sera organisé soit par :
* le Centre Universitaire de Pitești
* les Lectorats de langue roumaine de l’Institut de la Langue Roumaine
* l’Institut Culturel Roumain

# Étudiants n’ayant pas de connaissances de langue roumaine

Les étudiants qui n’ont pas de connaissances de langue roumaine doivent s’inscrire dans [Année préparatoire de langue roumaine](/admissions/preparatory-year) après avoir reçu la Lettre d’acceptation du Ministère de l’Éducation.

Après avoir réussi l’Année préparatoire, les étudiants peuvent poursuivre leurs études dans l’année scolaire suivante au programme d’études choisi (licence/maîtrise/doctorat).

# Exceptions

Vous ne devez faire la preuve des connaissances de langue roumaine si vous avez :
* un certificat linguistique/diplôme/licence de langue roumaine
* étudié en roumain pendant au moins quatre années consécutives dans une unité / établissement d’enseignement du système éducatif national de Roumanie ou à l’étranger.
