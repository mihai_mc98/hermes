<span class="note">Notă:</span> Pentru a studia la centrul nostru universitar, studenții trebuie să stăpânească limba română la minim unui **nivel B1**, conform Cadrului European Comun de Referință.

# Studenții cu cunoștințe de limba română

Dacă ați dobândit cunoștințe de limba română în contexte non-formale sau informale, puteți solicita să susțineți un examen de competențe lingvistice de limba română, care să fie organizat fie de către:
* Centrul universitar Pitești
* Lectoratele de limba română ale Institutului Limbii Române
* Institutul Cultural Român

# Studenții fără cunoștințe de limba română

Studenții care nu au cunoștințe de limba română trebuie să se înscrie în [Anul pregătitor de limba română](/admissions/preparatory-year) după primirea Scrisorii de acceptare de la Ministerul Educației.

După absolvirea cu succes a Anului Pregătitor, studenții își pot continua studiile în anul universitar următor la programul de studiu ales (licență/masterat/doctorat).

# Excepții

Nu sunt obligați să facă dovada cunoștințelor de limba română studenții care fie:
* au un certificat lingvistic/diplomă/licență de limba română 
* au studiat în limba română timp de cel puțin patru ani consecutivi la o unitate/instituție de învățământ din cadrul sistemului național de învățământ din România sau din străinătate


