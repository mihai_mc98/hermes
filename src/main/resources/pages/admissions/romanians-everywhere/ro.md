# Criterii de eligibilitate

În cadrul programului Românii de Pretutindeni, trebuie să vă asumați liber identitatea culturală românească și trebuie să fiți fie:
* cetățean român cu domiciliul sau reședința permanentă în străinătate
* emigrant în străinătate (sau un descendent al acestuia), indiferent dacă s-a păstrat sau nu cetățenia română
* de origine română sau aparținând mediului lingvistic și cultural românesc, în afara granițelor României

Vă rugăm să ne [contactați](#Contact) dacă nu sunteți sigur că statutul dumneavoastră se încadrează în programul Românii de Pretutindeni.

<div class="info-box">

## Ce presupune a fi dintr-un context lingvistic și cultural românesc ?

Vă punem la dispoziție o listă **non-exhaustivă** de etnonime utilizate frecvent: armâni, armânji, aromâni, basarabeni, bucovineni, cuțovlahi, daco-români, fărşeroţi, herţeni, istroromâni, latini dunăreni, macedoromâni, macedo-români, maramureşeni, megleniţi, megleno-români, moldoveni, moldovlahi, rrămâni, rumâni, valahi, vlahi, vlasi, volohi, macedo-armânji

</div>

# Aplică prin Programul Românii de Pretutindeni

## Cum se aplică

Vă rugăm să vă familiarizați cu [Cerințe lingvistice](/admissions/romanians-everywhere/language-requirements) necesare pentru a aplica prin Programului Românii de Pretutindeni.

Candidații trebuie să trimită [dosarul de admitere](/admissions/romanians-everywhere/admission-dossier) la adresa: [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

<div class="boxes">
    <a href="/admissions/romanians-everywhere/language-requirements">Cerințe lingvistice</a>
    <a href="/admissions/romanians-everywhere/admission-dossier">Dosar de admitere</a>
    <a href="/admissions/preparatory-year">Anul pregătitor de limba română</a>
    <a href="/admissions/tuition-fees#Românii-de-pretutindeni">Taxe de școlarizare</a>
    <a href="/admissions/visa-and-residence-permit#Românii-de-pretutindeni">Viza de studii și permisul de ședere</a>
    <a href="/admissions/romanians-everywhere/regulations">Reglementări</a>
</div>

## Locuri disponibile

Următoarele date sunt valabile pentru anul universitar 2023-2024.

<table>
    <thead>
        <tr>
            <th></th>
            <th>Licență</th>
            <th>Master</th>
            <th>Doctorat</th>
            <th>Anul pregătitor de limba română</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>CU bursă</td>
            <td>20</td>
            <td>7</td>
            <td>1</td>
            <td>-</td>
        </tr>
        <tr>
            <td>FĂRĂ bursă</td>
            <td>55</td>
            <td>21</td>
            <td>-</td>
            <td>4</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>TOTAL</td>
            <td>75</td>
            <td>28</td>
            <td>1</td>
            <td>4</td>
        </tr>
    </tfoot>
</table>


## Decizia dosarului de admitere

Vă rugăm să contactați facultatea la care ați aplicat pentru a afla stadiul în care se regăsește dosarului dumneavoastră.

<span class="note-important">Important:</span> Candidații admiși trebuie să își confirme locul prin trimiterea:
1. [Formularului de confirmare a locului de studiu](https://www.upit.ro/_document/27075/anexa_4_confirmare_loc_studii.pdf)
2. Documentele de studiu originale la înmatriculare

## Resurse utile

* [Ghid de admitere pentru Românii de pretutindeni (RO)](https://www.upit.ro/_document/175737/2022_ghid_romanii_de_pretutindeni_ro.pdf)
* [Video - Întrebări frecvente despre procesul de înscriere](https://educatiacontinua.edu.ro/source/Intrebari%20raspunsuri%20universitar%202021.mp4)

# Activități extracurriculare

În cadrul programului Românii de Pretutindeni, veți avea ocazia să:
* participați la o serie de activități pentru a descoperi România
* relaționați cu potențiali angajatori

# Contact

<div class="paragraph">

Dna. MIHAI Liliana, Secretar Relațiile Internaționale

Tel: [+40 348 453 108](tel:+40348453108)

E-mail: [liliana.mihai@upit.ro](mailto:liliana.mihai@upit.ro)

</div>

## Contact Ministerul Educației

Tinerii din Republica Moldova trebuie să contacteze Ministerul la adresa [dosarerepmold@edu.gov.ro](mailto:dosarerepmold@edu.gov.ro).

Românii din țările învecinate României și din diaspora trebuie să contacteze Ministerul la [dosarerompret@edu.gov.ro](mailto:dosarerompret@edu.gov.ro).
