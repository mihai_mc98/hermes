<table-of-contents depth="1" lang="en"></table-of-contents>

Please note that Tuition Fees are usually set each **June** for the Academic Year starting the following **October**.

<span class="note-important">Important: </span> In the light of the current epidemiological context, our University Centre has stopped accepting cash payments. All tuition fees must be paid via Bank transfer instead.

<div class="info-box">

## Your tuition fees are tied to your citizenship and your relationship to Romania

* Citizens of EU third countries (also known as non-EU countries)
* Citizens of the European Union (EU), European Economic Area (EEA) or the Swiss Confederation (CH)
* Citizens with ties to Romania: the [Romanians Everywhere Programme](#Romanians-Everywhere-Programme)

</div>

# Romanian Language Preparatory Year

This is a preparatory course for learning the Romanian language, oftentimes required before enrolling in a degree.
* Duration: 1 year (2 semesters) full-time
* Frequency: 25 hours/week 

## Non-EU Citizens

This course will be required before enrolling in:
* Higher Education degrees (Bachelor's/Master's/Ph.D.)
* High-School
* Post-Secondary Education

### Tuition fee

For Higher Education courses, the tuition fee is <span class="note">€1980/Academic Year</span>.

For students pursuing pre-University courses is <span class="note">€1620/Academic Year</span>.

## EU Citizens

This course will be required before enrolling in:
* Higher Education degrees (Bachelor's/Master's/Ph.D.)
* Secondary Education

The tuition fee for EU citizens is <span class="note">3400 RON/Academic Year</span>.

# Higher Education Degrees

## Non-EU Citizens

<span class="note">Note:</span> The following figures are estimates for different groups of Courses for an entire Academic Year (9 months). Please check the [List of Tuition Fees](https://www.upit.ro/_document/28080/programe_de_studii_si_taxe-studenti_internationali_2018-2019.pdf) for exact information about a specific Course. 

| Course Group                                                                                    | Bachelor's/Master's/Medical Residency Degrees | Postgraduate and Ph.D. Degrees |
|-------------------------------------------------------------------------------------------------|-----------------------------------------------|--------------------------------|
| <abbr title="Science, Technology, Engineering and Mathematics">STEM</abbr>, Agronomy and Sports | €2430                                         | €2610                          |
| Architecture                                                                                    | €3150                                         | €3330                          |
| Social Studies, Psychology and Economy                                                          | €1980                                         | €2160                          |
| Medicine                                                                                        | €2880                                         | €3060                          |
| Music and Arts                                                                                  | €3780                                         | €3960                          |
| Musical Interpretation and Theatre                                                              | €6750                                         | €6930                          |
| Film Studies                                                                                    | €8550                                         | €8730                          |

### Payment Receipts

All payment receipts must display the following features:

1. The IBAN of The University Centre of Pitești
2. The Candidate's name
3. The name of the Bank Account Holder (**only** if different from the Candidate's name)

## EU Citizens

EU/EEA/CH students pay the same tuition fees as Romanian citizens. The following figures are for an entire Academic Year (9 months).

| Degree Type                      | Tuition Fees Range |
|----------------------------------|--------------------|
| Bachelor's Degrees               | 3400 – 3700 RON    |
| Master's Degrees                 | 3500 – 3700 RON    |
| Ph.D. Degrees                    | 5000 – 6000 RON    |
| Post-Secondary Education Degrees | 2200 – 2700 RON    |

# Romanians Everywhere Programme

The University Centre of Pitești offers applicants under the Romanians Everywhere programme **Fees Waivers** for:
* Tuition (**excluding** Degrees taught in a foreign language)
* Degree applications and dossiers' processing
* Degree Enrolment and Registration
* Language Proficiency Tests
* Applications to Ph.D. Degrees
* Examinations of specific skills needed for Higher Education Studies

<span class="note-important">Important: </span> Wherever required, students under the Romanians Everywhere programme pay the **same** Tuition Fees as Romanian citizens.

Subject to availability, students may also receive a **scholarship** funded by the Ministry of Education.

# Payment Details

## Non-EU Citizens

<span class="note-important">Important:</span> Tuition fees payment can only be made in **EUR**.

| Category       | Payment Details                                             |
|----------------|-------------------------------------------------------------|
| Payee Name     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Payee Address  | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN           | RO77 RNCB 0022 0136 1744 0040                               |
| SWIFT          | RNCB ROBU                                                   |
| Bank's Name    | Banca Comericală Română                                     |
| Bank's Address | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| TIN (CUI)      | RO 4122183                                                  |

## EU Citizens

<span class="note-important">Note:</span> Tuition fees payment can only be made in **RON**.
There are 2 available accounts for paying the tuition fees.

### BCR Account

| Category       | Payment Details                                             |
|----------------|-------------------------------------------------------------|
| Payee Name     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Payee Address  | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN           | RO63 RNCB 0022 0136 1744 0001                               |
| SWIFT          | RNCB ROBU                                                   |
| Bank's Name    | Banca Comericală Română                                     |
| Bank's Address | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| TIN (CUI)      | RO 4122183                                                  |

### Treasury Account

| Category       | Payment Details                                             |
|----------------|-------------------------------------------------------------|
| Payee Name     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Payee Address  | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN           | RO78 TREZ 0462 0F33 0500 XXXX                               |
| SWIFT          | TREZ ROBU XXX                                               |
| Bank's Name    | Trezoreria Pitești                                          |
| Bank's Address | Bd. Republicii, Nr. 118, 110050, Pitești, Argeș, Romania    |
| TIN (CUI)      | RO 4122183                                                  |

# Refunds

Please visit our [Tuition Fees Refunds Procedure](/admissions/tuition-fees/refunds) to find out more about our Policy for Refunding Tuition Fees.

## Postponing the Academic Year

<div class="info-box">

## University Policy

Please read the [Procedure for the Postponing of the Academic Year](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>

Please send all documents from the [Procedure for the Postponing of the Academic Year](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) to the [International Students' Office](/admissions#Contact).

### Deadline

The Postponing Request must be received by the **1st of May <span class="academic-year"></span>**.

### Useful Documents

* [Request Form for Postponing the Academic Year](https://www.upit.ro/_document/242044/cerere_reportare_taxe_2023.doc)