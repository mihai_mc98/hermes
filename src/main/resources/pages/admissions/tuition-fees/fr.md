<table-of-contents depth="1" lang="fr"></table-of-contents>

Veuillez noter que les frais de scolarité sont généralement fixés au mois de **juin** de l’année universitaire suivante qui commence en **octobre**.

<span class="note-important">Important : </span> Compte tenu du contexte épidémiologique actuel, notre centre universitaire a cessé d’accepter les paiements en espèces. Tous les frais de scolarité doivent être payés par virement bancaire.

<div class="info-box">

## Les frais de scolarité diffèrent selon votre nationalité et/ou vos liens avec la Roumanie

* Citoyens des pays tiers de l’UE (également connus sous le nom de pays non membres de l’UE)
* Citoyens de l’Union européenne (UE), de l’Espace économique européen (SEE) ou de la Confédération suisse (CH)
* Citoyens ayant des liens avec la Roumanie : programme [les Roumains de partout](#Les-Roumains-de-partout)

</div>

# Année préparatoire de langue roumaine

Il s’agit d’un cours préparatoire pour apprendre la langue roumaine, souvent nécessaire avant de s’inscrire à la faculté.
* Durée : 1 année (2 semestres) cours à plein temps
* Fréquence : 25 heures/semaine

## Citoyens des pays tiers de l’UE

Ce cours sera requis avant de s’inscrire à :
* Diplômes d’enseignement supérieur (licence/master/doctorat)
* Lycée
* Enseignement post-secondaire

### Frais de scolarité

Pour les étudiants qui suivront des programmes d’études de l’enseignement supérieur, les frais de scolarité sont <span class="note">€1980/année universitaire</span>.

Pour les étudiants qui suivront des cours préuniversitaires sont de <span class="note">€1620/année universitaire</span>.

## Citoyens de l’UE

Ce cours sera requis avant de s’inscrire dans :
* l’enseignement supérieur (licence/master/doctorat)
* l’enseignement préuniversitaire

Les frais de scolarité pour les citoyens de l’UE sont de <span class="note">3400 RON/année universitaire</span>.

# Enseignement supérieur

## Citoyens des pays tiers de l’UE

<span class="note">Note :</span> Les frais suivants sont estimés pour divers groupes de programmes d’études pour une année universitaire complète (neuf mois).
Veuillez consulter la [Liste des frais de scolarité](https://www.upit.ro/_document/28080/programe_de_studii_si_taxe-studenti_internationali_2018-2019.pdf) pour des informations précises sur un programme d’études particulier.

| Groupe de cours                                                                                 | Diplômes de licence/master/internat | Diplômes postuniversitaires et de doctorat |
|-------------------------------------------------------------------------------------------------|-------------------------------------|--------------------------------------------|
| <abbr title="Science, Technologie, Ingénierie et Mathématiques">STEM</abbr>, agronomie et sport | €2430                               | €2610                                      |
| Architecture                                                                                    | €3150                               | €3330                                      |
| Études sociales, psychologie et économie                                                        | €1980                               | €2160                                      |
| Médicine                                                                                        | €2880                               | €3060                                      |
| Musique et Arts                                                                                 | €3780                               | €3960                                      |
| Interprétation musicale et théâtre                                                              | €6750                               | €6930                                      |
| Film                                                                                            | €8550                               | €8730                                      |

### Justificatif de paiement

Tout justificatif de paiement doit présenter les caractéristiques suivantes : 

1. IBAN du Centre Universitaire de Pitești
2. Nom du candidat
3. Nom du titulaire du compte bancaire (**seulement** s’il diffère du nom du candidat)

## Citoyens de l’UE

Les étudiants de l’UE, l’EÉE ou la Suisse paient les mêmes frais de scolarité que les citoyens roumains. Les frais d’études suivants sont valables pour une année universitaire complète (9 mois).

| Type de diplôme         | Frais de scolarité |
|-------------------------|--------------------|
| Licence                 | 3400 – 3700 RON    |
| Master                  | 3500 – 3700 RON    |
| Doctorat                | 5000 – 6000 RON    |
| Études post-secondaires | 2200 – 2700 RON    |

# Les Roumains de partout

Le Centre Universitaire de Pitești offre aux candidats dans le cadre du programme les Roumains de partout des **exonérations de frais** pour :
* Frais de scolarité (**à l’exception** des diplômes avec enseignement dans une langue étrangère)
* Demandes d’inscription et traitement des dossiers
* Inscription et immatriculation
* Tests de compétence linguistique
* Examens des compétences spécifiques requises pour les études d’enseignement supérieur

<span class="note-important">Important :</span> En cas de besoin, les étudiants du programme les Roumains de partout paient les **mêmes** frais de scolarité que les citoyens roumains.

Sous réserve de disponibilité, les étudiants peuvent également recevoir une **bourse** du Ministère de l’Éducation.

# Coordonnées bancaires

## Citoyens des pays tiers de l’UE

<span class="note-important">Important :</span> Le paiement des frais de scolarité ne peut être effectué qu’en **EUR**.

| Catégorie               | Coordonnées bancaires                                       |
|-------------------------|-------------------------------------------------------------|
| Nom du bénéficiaire     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresse du bénéficiaire | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                    | RO77 RNCB 0022 0136 1744 0040                               |
| SWIFT                   | RNCB ROBU                                                   |
| Nom de la banque        | Banca Comericală Română                                     |
| Adresse de la banque    | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| NIF (CUI)               | RO 4122183                                                  |

## Citoyens de l’UE

<span class="note-important">Note :</span> Le paiement des frais de scolarité ne peut être effectué qu’en **RON**.
Il y a deux comptes disponibles pour payer les frais de scolarité.

### Compte BCR

| Catégorie               | Coordonnées bancaires                                       |
|-------------------------|-------------------------------------------------------------|
| Nom du bénéficiaire     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresse du bénéficiaire | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                    | RO63 RNCB 0022 0136 1744 0001                               |
| SWIFT                   | RNCB ROBU                                                   |
| Nom de la banque        | Banca Comericală Română                                     |
| Adresse de la banque    | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| NIF (CUI)               | RO 4122183                                                  |

### Compte de la Trésorerie

| Catégorie               | Coordonnées bancaires                                       |
|-------------------------|-------------------------------------------------------------|
| Nom du bénéficiaire     | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresse du bénéficiaire | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                    | RO78 TREZ 0462 0F33 0500 XXXX                               |
| SWIFT                   | TREZ ROBU XXX                                               |
| Nom de la banque        | Trezoreria Pitești                                          |
| Adresse de la banque    | Bd. Republicii, Nr. 118, 110050, Pitești, Argeș, Romania    |
| NIF (CUI)               | RO 4122183                                                  |

# Remboursements

Veuillez visiter la section de [Remboursements des frais d’études](/admissions/tuition-fees/refunds) pour connaître les conditions dans lesquelles les frais d’études peuvent être remboursés.

## Rapport de l’année universitaire

<div class="info-box">

## Procédure universitaire

Veuillez lire la [Procédure de rapport de l’année universitaire](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>

Veuillez envoyer tous les documents de la [Procédure de rapport de l’année universitaire](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) au [Bureau des Étudiants Internationaux](/admissions#Contact).

### Date limite

La demande de rapport doit être reçue par notre Bureau avant le **1 mai <span class="academic-year"></span>**.

### Documents utiles

* [Demande de rapport de l’année universitaire](https://www.upit.ro/_document/242044/cerere_reportare_taxe_2023.doc)
