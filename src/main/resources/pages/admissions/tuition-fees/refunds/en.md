<div class="info-box">

## University Policy

Please read the [Procedure for the Refunding of Tuition Fees](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>


# Eligibility

Depending on your circumstances, you may be eligible to have a **part** of your tuition fees refunded. The final amount is subject to:
1. A **5% deduction** withheld by the Ministry of Education for assessing your Admission Dossier
2. Any fees and/or foreign currency exchange commissions incurred to process the Bank Transfer

<span class="note">Note:</span> Please note that we can only issue refunds to the original Bank Account Holder.

# Withdrawing from the Degree

Please note that a maximum of **50%** of the paid Tuition Fees may be refunded to **enrolled** students withdrawing from their Degrees. 

Refund Request may **only** be submitted:
1. By the end of Semester I of the Degree
2. Within the relevant [Deadlines](#Deadlines)

# Deadlines

Students may **only** submit a Refund Request:
1. Within **6 months** of the Academic Year's start (in October)
2. By the **1st of May <span class="academic-year"></span>**

<span class="note-important">Important:</span> **NO** Tuition Fees will be refunded if the Refund Request is received after the relevant deadlines.

# Submit your Refund Request

Please send all documents requested in the [Procedure for Refunding of Tuition Fees](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) to the [International Students' Office](/admissions#Contact).

# Useful Documents

* [Request Form for Refunding Tuition Fees](https://www.upit.ro/_document/242045/cerere_restituire_taxe_2023.doc)