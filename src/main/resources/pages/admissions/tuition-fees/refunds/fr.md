<div class="info-box">

## Procédure universitaire

Veuillez lire la [Procédure de remboursement des frais d’études](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>


# Critères d’admissibilité

Les étudiants ont la possibilité d’enregistrer une demande de remboursement **partiel** des frais d’études. Sur le montant final, seront retenus :
1. Une **déduction de 5 %** du Ministère de l’Éducation pour l’évaluation de votre dossier d’admission
2. Tous frais et/ou frais de change que le centre universitaire encourra pour traiter le virement bancaire

<span class="note">Note :</span> Veuillez noter que les remboursements ne peuvent être effectués que sur le compte bancaire à partir duquel les frais d’études ont été payés.

# Retrait du programme d’études

Les étudiants **immatriculés** qui se retirent du programme d’études peuvent recevoir maximum **50 %** du montant des frais d’études payés.

Les demandes de remboursement ne peuvent être reçues **que** :
1. jusqu’à la fin du premier semestre du programme d’études
2. respectant les [Dates limites](#Date-limite) indiquées

# Date limite

Les demandes de remboursement ne seront acceptées **que** :
1. Lors des premiers **6 mois** après le début de l’année universitaire (en octobre)
2. Jusqu’au **1 mai <span class="academic-year"></span>**

<span class="note-important">Important :</span> Les frais d’études **NE** seront **PAS** remboursés si la demande de remboursement est reçue après les dates limites indiquées.

# Comment envoyer la demande ?

Veuillez envoyer tous les documents nécessaires de la [Procédure de remboursement des frais d’études](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) au [Bureau des Étudiants Internationaux](/admissions#Contact).

# Documents utiles

* [Demande de remboursement des frais d’études](https://www.upit.ro/_document/242045/cerere_restituire_taxe_2023.doc)
