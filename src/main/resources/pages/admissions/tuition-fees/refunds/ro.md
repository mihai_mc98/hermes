<div class="info-box">

## Procedură universitară

Vă rugăm să citiți [Procedura pentru rambursarea taxelor de învățământ](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>


# Criterii de eligibilitate

Studenții au posibilitatea de a înregistra o cerere pentru rambursarea **parțială** a taxelor de învățământ. Din suma finală se vor reține:
1. O **deducere de 5%** a Ministerului Educației pentru evaluarea dosarului dumneavoastră de admitere
2. Orice taxe și/sau comisioane de schimb valutar pe care centrul universitar le va suporta pentru a procesa transferul bancar

<span class="note">Notă:</span> Vă rugăm să luați la cunoștință că rambursările se pot efectua numai către contul bancar din care au fost plătite taxele de învățământ.

# Retragerea de la programul de studiu

Studenții **înmatriculați** care se retrag de la programul de studiu pot primi un maximum de **50%** din cuantumul taxelor de învățământ plătite.

Cererile de rambursare pot fi primite **numai**:
1. până la finalul semestrului I al programului de studiu
2. respectând [Datele limită](#Date-limită) relevante

# Date limită

Cererile de rambursare vor fi acceptate **numai**:
1. În primele **6 luni** de la începerea anului universitar (în octombrie)
2. Până la data de **1 mai <span class="academic-year"></span>**

<span class="note-important">Important:</span> **NU** se vor rambursa taxele de învățământ dacă cererea de rambursare este primită după datele limită relevante.

# Cum se trimite cererea ?

Vă rugăm să trimiteți toate documentele necesare din [Procedura pentru rambursarea taxelor de studiu](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) către [Biroul Studenților Internaționali](/admissions#Contact).

# Documente utile

* [Cerere de rambursare a taxelor de învățământ](https://www.upit.ro/_document/242045/cerere_restituire_taxe_2023.doc)