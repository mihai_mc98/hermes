<table-of-contents depth="1" lang="ro"></table-of-contents>

Vă rugăm să luați la cunoștință că taxele de școlarizare sunt stabilite de obicei în fiecare **iunie** al anului universitar următor începând în **octombrie**.

<span class="note-important">Important: </span> Având în vedere contextul epidemiologic actual, centrul nostru universitar a încetat să mai accepte plăți în numerar. Toate taxele de școlarizare trebuie plătite prin transfer bancar.

<div class="info-box">

## Taxele de școlarizare diferă în funcție de cetățenia dumneavoastră și/sau de legăturile dumneavoastră cu România

* Cetățeni ai țărilor terțe UE (cunoscute și ca țări non-UE)
* Cetățeni ai Uniunii Europene (UE), ai Spațiului Economic European (SEE) sau ai Confederației Elvețiene (CH)
* Cetățenii care au legături cu România: programul [Românii de pretutindeni](#Români-de-pretutindeni)

</div>

# Anul pregătitor de limba română

Acesta este un curs pregătitor pentru învățarea limbii române, adesea necesar înainte de înscrierea la o facultate.
* Durata: 1 an (2 semestre) cu frecvență
* Frecvență: 25 de ore/săptămână

## Cetățeni din afara UE

Acest curs va fi necesar înainte de înscrierea la:
* Diplome de învățământ superior (licență/masterat/doctorat)
* Liceu
* Învățământ post-liceal

### Taxa de școlarizare

Pentru studenții care vor urma programe de studiu din învățământul superior, taxa de școlarizare este de <span class="note">1980€/an universitar</span>.

Pentru studenții care vor urma cursuri preuniversitare este de <span class="note">1620€/an universitar</span>.

## Cetățeni UE

Acest curs va fi necesar înainte de a vă înscrie în:
* Învățământul universitar (licență/masterat/doctorat)
* Învățământul preuniversitar

Taxa de școlarizare pentru cetățenii UE este de <span class="note">3400 RON/an universitar</span>.

# Învățământ superior

## Cetățeni non-UE

<span class="note">Nota:</span> Următoarele taxe sunt estimative pentru diverse grupe de programe de studiu pentru un an universitar integral (9 luni). 
Vă rugăm să consultați [Lista taxelor de școlarizare](https://www.upit.ro/_document/28080/programe_de_studii_si_taxe-studenti_internationali_2018-2019.pdf) pentru informații exacte despre un anumit program de studii.

| Categorie programe de studiu                                                             | Diplome de licență/masterat/rezidențiat | Diplome postuniversitare și de doctorat |
|------------------------------------------------------------------------------------------|-----------------------------------------|-----------------------------------------|
| <abbr title="Științe, Tehnologie, Ingerie și Matematică">STEM</abbr>, agronomie și sport | €2430                                   | €2610                                   |
| Arhitectură                                                                              | €3150                                   | €3330                                   |
| Studii sociale, psihologie și economie                                                   | €1980                                   | €2160                                   |
| Medicină                                                                                 | €2880                                   | €3060                                   |
| Muzică și Arte                                                                           | €3780                                   | €3960                                   |
| Interpretare muzicală și teatru                                                          | €6750                                   | €6930                                   |
| Film                                                                                     | €8550                                   | €8730                                   |

### Dovadă de plată

Toate dovezile de plată trebuie să prezinte următoarele caracteristici:

1. IBAN-ul Centrului Universitar Pitești
2. Numele candidatului
3. Numele titularului contului bancar (**numai** dacă este diferit de numele candidatului)

## Cetățeni UE

Studenții din UE/SEE/CH plătesc aceleași taxe de școlarizare ca și cetățenii români. Următoarele taxe de învățământ sunt valabile pentru un an universitar integral (9 luni).

| Tipul de diplomă    | Taxe de școlarizare |
|---------------------|---------------------|
| Licență             | 3400 – 3700 RON     |
| Master              | 3500 – 3700 RON     |
| Doctorat            | 5000 – 6000 RON     |
| Studii post-liceale | 2200 – 2700 RON     |

# Români de pretutindeni

Centrul Universitar Pitești oferă candidaților în cadrul programului Românii de Pretutindeni **scutiri de taxe** pentru:
* Taxele de școlarizare (**cu excepția** diplomelor cu predare într-o limbă străină)
* Cererile de înscriere și procesare a dosarelor
* Înscrierea și înmatricularea
* Testele de competență lingvistică
* Examinări de competențe specifice necesare pentru studiile din învățământul superior

<span class="note-important">Important: </span> În caz de necesitate, studenții din cadrul programului Românii de pretutindeni plătesc **aceleași** taxe de școlarizare ca și cetățenii români.

În funcție de disponibilitate, studenții pot primi, de asemenea, și o **bursă** din partea Ministerului Educației.

# Coordonate bancare

## Cetățeni non-UE

<span class="note-important">Important:</span> Plata taxelor de școlarizare se poate face numai în **EUR**.

| Categorie             | Coordonate bancare                                          |
|-----------------------|-------------------------------------------------------------|
| Numele beneficiarului | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresa beneficiarului | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                  | RO77 RNCB 0022 0136 1744 0040                               |
| SWIFT                 | RNCB ROBU                                                   |
| Numele băncii         | Banca Comericală Română                                     |
| Adresa băncii         | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| CUI                   | RO 4122183                                                  |

## Cetățeni UE

<span class="note-important">Nota:</span> Plata taxelor de școlarizare se poate face numai în **RON**.
Există 2 conturi disponibile pentru plata taxelor de școlarizare.

### Cont BCR

| Categorie             | Coordonate bancare                                          |
|-----------------------|-------------------------------------------------------------|
| Numele beneficiarului | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresa beneficiarului | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                  | RO63 RNCB 0022 0136 1744 0001                               |
| SWIFT                 | RNCB ROBU                                                   |
| Numele băncii         | Banca Comericală Română                                     |
| Adresa băncii         | Bd. Republicii, Nr. 83, Pitești, Argeș, Romania             |
| CUI                   | RO 4122183                                                  |

### Contul de trezorerie

| Categorie             | Coordonate bancare                                          |
|-----------------------|-------------------------------------------------------------|
| Numele beneficiarului | CENTRUL UNIVERSITAR PITEȘTI                                 |
| Adresa beneficiarului | Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România |
| IBAN                  | RO78 TREZ 0462 0F33 0500 XXXX                               |
| SWIFT                 | TREZ ROBU XXX                                               |
| Numele băncii         | Trezoreria Pitești                                          |
| Adresa băncii         | Bd. Republicii, Nr. 118, 110050, Pitești, Argeș, Romania    |
| CUI                   | RO 4122183                                                  |

# Rambursări

Vă rugăm să vizitați secțiunea de [Rambursări ale taxelor de învățământ](/admissions/tuition-fees/refunds) pentru a afla condițiile în care taxele de învățământ pot fi restituite.

## Raportarea anului universitar

<div class="info-box">

## Procedură universitară

Vă rugăm să citiți [Procedura pentru raportarea anului universitar](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf).

</div>

Vă rugăm să trimiteți toate documentele din [Procedura pentru raportarea anului universitar](https://www.upit.ro/_document/242046/procedura_restituire_si_reportare_taxe_cpv.pdf) către [Biroul Studenților Internaționali](/admissions#Contact).

### Termen limită

Cererea de raportare trebuie să fie primită de către Biroul nostru până la data de **1 mai <span class="academic-year"></span>**.

### Documente utile

* [Cerere pentru raportarea anului universitar](https://www.upit.ro/_document/242044/cerere_reportare_taxe_2023.doc)
