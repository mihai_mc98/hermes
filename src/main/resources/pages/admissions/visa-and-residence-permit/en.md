# Romanian Citizens

Candidates holding Romanian citizenship need **not** apply for a Study Visa, nor a Residence Permit.

# Non-EU Citizens

Please note that The University Centre of Pitești is unable to intervene in the process of granting Study Visas.

Should the Embassy/Relevant Authorities require supporting documents issued by our University Centre, please [get in touch](/admissions#Contact) with us.

## Study Visa

Candidates must apply for a Study Visa at the Romanian Embassy in their country or at the closest Romanian Diplomatic Mission.
The following documents are required:

1. Letter of Acceptance to Studies
2. Proof of payment for the relevant [Tuition Fees](/admissions/tuition-fees)

## Residence Permit

To apply for a residence permit, you need:
1. [Certificate of Attendance](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) issued by the faculty where you are studying 
2. Bank Statement from any Romanian Bank proving the student has enough funds for covering the living costs – at least **€3000**

<span class="note-important">Important:</span> The Certificate of Attendance certificate must include your name and personal details (as in your passport), following the [Template for Faculties](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

### Fees

The following fees are estimates only. Please contact the relevant authorities for exact figures.

1. **Residence Permit fee**: 260 RON
2. **Consular fees**: €120 (paid in RON at the day's NBR exchange rate) – approx. 600 RON
3. **Medical Laboratory Tests**: 190 RON

Students aged 26 and older must also pay for:

1. **Compulsory Medical Insurance**: 208 RON/month
2. **Private Medical Insurance** in Romania: 90 RON/year

<span class="note-important">Important:</span> Students suffering from any contagious diseases (such as hepatitis or TB) may face deportation to their country of origin by the General Inspectorate for Immigration of the County of Argeș.

# EU Citizens

EU/EEA/CH citizens studying in Romania need not apply for a Study Visa. 

To exercise their EEA Treaty Rights, students need to lodge an application for an **EU Registration Certificate** with the General Inspectorate for Immigration of the County of Argeș.

## EU Registration Certificate

The following guidance is for an EU Registration Certificate for **studying** in Romania. Please visit the [General Inspectorate for Immigration website](http://igi.mai.gov.ro/en/content/citizens-eu-eea) for more information about different scenarios.

1. [Certificate of Attendance](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) issued by the faculty where you are studying.
2. [Registration Request Form](https://www.upit.ro/_document/35695/model_cerere_eliberarea_permis_de_sedere.pdf)
3. Bank Statement from any Romanian Bank proving the student has enough funds for covering the living costs – at least **1000 RON**

<span class="note-important">Important:</span> The Certificate of Attendance certificate must include your name and personal details (as in your passport), following the [Template for Faculties](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

Students aged 26 and older must also pay for:

1. **Compulsory Medical Insurance**: 208 RON/month
2. **Private Medical Insurance** in Romania: 90 RON/year

# Romanians Everywhere

## Study Visa

Romanian Everywhere candidates must:

* enter Romania with a valid passport holding a **D/SD STUDY VISA**
* submit an application to the Immigration Service to apply for a Residence Permit for studying in Romania

For further information, please see the guide on [How to Lodge an Application for a Residence Permit](https://www.upit.ro/_document/31314/brief_guide_study_visa_2019.pdf).

## Residence Permit

To apply for a residence permit, you need a [Certificate of Attendance](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) issued by the faculty where you are studying.

<span class="note-important">Important:</span> The Certificate of Attendance certificate must include your name and personal details (as in your passport), following the [Template for Faculties](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).