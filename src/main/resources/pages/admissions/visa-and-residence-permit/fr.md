# Citoyens roumains

Les candidats qui ont la nationalité roumaine n’ont besoin **ni** d’un visa d’études **ni** d’un permis de séjour.

# Citoyens hors de l’UE

Veuillez noter que le Centre Universitaire de Pitești ne peut pas intervenir lors du processus de délivrance des visas d’études.

Si l’Ambassade/les Autorités compétentes demandent des pièces justificatives délivrées par notre centre universitaire, veuillez nous [contacter](/admissions#Contact).


## Visa d’études

Les candidats doivent demander un visa d’études à l’Ambassade de Roumanie dans leur pays ou à la mission diplomatique roumaine la plus proche.
Les documents suivants sont requis :

1. Lettre d’acceptation aux études
2. Justificatif du paiement des [frais de scolarité](/admissions/tuition-fees) afférents

## Permis de séjour 

Pour obtenir un permis de séjour, vous avez besoin de :
1. [Attestation](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) délivrée par la faculté où vous étudiez
2. Relevé de compte bancaire de toute banque de Roumanie prouvant que l’étudiant dispose de fonds suffisants pour couvrir les frais d’entretien - au moins **€3000**.

<span class="note-important">Important :</span> L’attestation doit contenir le nom complet et les données personnelles (comme dans le passeport), selon le [Modèle pour les facultés](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

### Taxes

Les taxes suivantes ne sont que des estimations. Veuillez contacter les autorités compétentes pour obtenir des chiffres exacts.

1. **Taxe de permis de séjour** : 260 RON
2. **Taxes consulaires** : 120 € (payées en RON au taux de change de la BNR de ce jour-là) - environ 600 RON
3. **Analyses médicales** : 190 RON

Les étudiants âgés de 26 ans ou plus doivent aussi payer :

1. **Assurance médicale obligatoire** : 208 RON/mois
2. **Assurance médicale privée** en Roumanie : 90 RON/an

<span class="note-important">Important :</span> Les étudiants qui souffrent de toute maladie contagieuse (par exemple l’hépatite ou la tuberculose) peuvent être expulsés vers leur pays d’origine par l’Inspection générale de l’immigration du département d’Argeș.

# Citoyens de l’UE

Citoyens de l’UE, l’EÉE ou de la Suisse étudiant en Roumanie ne doivent pas demander un visa d’études. 

Afin d’exercer leurs droits prévus dans le traité EEE, les étudiants doivent présenter une demande de **Certificat d’enregistrement UE** auprès de l’Inspection générale de l’immigration du département d’Argeș.

## Certificat d’enregistrement UE

Les documents suivants sont strictement nécessaires pour obtenir un certificat d’enregistrement UE pour les **études** en Roumanie. Veuillez visiter le [site Web de l’Inspection générale de l’immigration](http://igi.mai.gov.ro/en/content/citizens-eu-eea) pour plus d’informations sur différents scénarios.

1. [Attestation](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) délivrée par la faculté où vous étudiez.
2. [Demande de délivrance du certificat d’enregistrement](https://www.upit.ro/_document/35695/model_cerere_eliberarea_permis_de_sedere.pdf)
3. Relevé de compte bancaire de toute banque de Roumanie prouvant que l’étudiant dispose de fonds suffisants pour couvrir les frais d’entretien - au moins **1000 RON**.

<span class="note-important">Important :</span> L’Attestation doit contenir le nom complet et les données personnelles (comme dans le passeport), selon le [Modèle pour les facultés](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

Les étudiants âgés de 26 ans ou plus doivent aussi payer :

1. **Assurance médicale obligatoire** : 208 RON/mois
2. **Assurance médicale privée** en Roumanie : 90 RON/an


# Les Roumains de partout

## Visa d’études

Les candidats Roumains de partout doivent :

* entrer en Roumanie avec un passeport valide possédant un **VISA D’ÉTUDES D/SD**.
* présenter une demande auprès du Service de l’immigration en vue d’obtenir un permis de séjour pour des études en Roumanie

Pour plus d’informations, veuillez consulter le guide [Comment présenter une demande de permis de séjour](https://www.upit.ro/_document/31314/brief_guide_study_visa_2019.pdf).

## Permis de séjour

Pour obtenir un permis de séjour, vous avez besoin d’une [Attestation](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) délivrée par la faculté où vous étudiez.

<span class="note-important">Important :</span> L’Attestation doit contenir le nom complet et les données personnelles (comme dans le passeport), selon le [Modèle pour les facultés](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).
