# Cetățeni români

Candidații care dețin cetățenie română **nu** au nevoie de o viză de studii, nici de un permis de ședere.

# Cetățeni din afara UE

Centrul Universitar Pitești nu poate interveni în procesul de acordare a vizelor de studiu.

În cazul în care Ambasada/Autoritățile relevante solicită documente justificative emise de centrul nostru universitar, vă rugăm să [luați legătura](/admissions#Contact) cu noi.


## Viza de studiu

Candidații trebuie să aplice pentru o viză de studiu la Ambasada României din țara lor sau la cea mai apropiată misiune diplomatică română.
Sunt necesare următoarele documente:

1. Scrisoarea de acceptare la studii
2. Dovada plății [taxelor de școlarizare](/admissions/tuition-fees) aferente

## Permis de ședere

Pentru a obține un permis de ședere, aveți nevoie de:
1. [Adeverință](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) emisă de facultatea la care studiați
2. Extras de cont bancar de la orice bancă din România prin care să se dovedească faptul că studentul dispune de fonduri suficiente pentru acoperirea cheltuielilor de întreținere - cel puțin **3000€**.

<span class="note-important">Important:</span> Adeverința trebuie să conțină numele complet și datele personale (exact ca în pașaport), urmând [Modelul pentru facultăți](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

### Taxe

Următoarele taxe sunt doar estimative. Vă rugăm să contactați autoritățile relevante pentru cifre exacte.

1. **Taxa pentru permisul de ședere**: 260 RON
2. **Taxe consulare**: 120 € (plătite în RON la cursul de schimb BNR din ziua respectivă) - aprox. 600 RON
3. **Analize medicale**: 190 RON

Studenții cu o vârsta de 26 de ani sau mai mare trebuie să mai achite și:

1. **Asigurarea medicală obligatorie**: 208 RON/lună
2. **Asigurarea medicală privată** în România: 90 RON/an

<span class="note-important">Important:</span> Studenții care suferă de orice boală contagioasă (ex.: hepatita sau TBC) pot fi expulzați în țara de origine de către Inspectoratul General pentru Imigrări al Județului Argeș.

# Cetățeni UE

Cetățenii UE/SEE/CH care studiază în România nu trebuie să aplice pentru o viză de studii. 

Pentru a-și exercita drepturile prevăzute în Tratatul SEE, studenții trebuie să depună o cerere pentru un **Certificat de înregistrare UE** la Inspectoratul General pentru Imigrări al Județului Argeș.

## Certificat de înregistrare UE

Documentele următoare sunt necesare strict pentru obținerea unui certificat de înregistrare UE pentru **studii** în România. Vă rugăm să vizitați [site-ul web al Inspectoratului General pentru Imigrări](http://igi.mai.gov.ro/en/content/citizens-eu-eea) pentru mai multe informații despre diferite scenarii.

1. [Adeverință](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) eliberată de facultatea unde studiați.
2. [Cerere pentru eliberarea certificatului de înregistrare](https://www.upit.ro/_document/35695/model_cerere_eliberarea_permis_de_sedere.pdf)
3. Extras de cont bancar de la orice bancă din România prin care să dovedească faptul că studentul dispune de fonduri suficiente pentru acoperirea cheltuielilor de întreținere - cel puțin **1000 RON**.

<span class="note-important">Important:</span> Adeverința trebuie să cuprindă numele complet și datele personale (exact ca în pașaport), urmând [Modelul pentru facultăți](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).

Studenții cu o vârsta de 26 de ani sau mai mare trebuie să mai achite și:

1. **Asigurarea medicală obligatorie**: 208 RON/lună
2. **Asigurarea medicală privată** în România: 90 RON/an

# Românii de pretutindeni

## Viză de studiu

Candidații români de pretutindeni trebuie:

* să intre în România cu un pașaport valabil care să dețină o **VISA DE STUDIU D/SD**.
* să depună o cerere la Serviciul pentru Imigrări în vederea obținerii permisului de ședere pentru studii în România

Pentru mai multe informații, vă rugăm să consultați ghidul [Cum se depune o cerere pentru obținerea permisului de ședere](https://www.upit.ro/_document/31314/brief_guide_study_visa_2019.pdf).

## Permis de ședere

Pentru a obține un permis de ședere, aveți nevoie de o [Adeverință](https://www.upit.ro/_document/35480/model_adeverinta_obtinere_permis_sedere.pdf) eliberată de facultatea la care studiați.

<span class="note-important">Important:</span> Adeverința trebuie să conțină numele complet și datele personale (exact ca în pașaport), urmând [Modelul pentru facultăți](https://www.upit.ro/_document/35482/model_completare_adeverinta_permis.pdf).
