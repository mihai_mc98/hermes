# What we offer

* **Career** Counselling and Guidance
* Psychological Counselling and Support
* Educational Counselling
* Support to develop skills for setting goals and objectives, time management and critical thinking 
* Counselling and **mentoring** for students with learning diﬃculties
* Support in identifying educational and workplace opportunities (internships, scholarships and experience exchanges, etc.) 
* **Free of charge** evaluations of professional interests and skills
* **Trainings** to develop the professional skills required in industry

# Who can access our services ?

Our services are opened to:
* Students and Alumni of The University Centre of Pitești
* Erasmus+ Students
* Final-Year High-School Students
* Students from other Universities
