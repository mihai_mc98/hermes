Founded in **2019** and financed by l'Agence Universitaire de la Francophonie and The University Centre of Pitești, the Centre is our go-to place to teach <abbr title="Français Langue Étrangère">FLE</abbr> through seminaries and language practice lessons. 

Our students benefit from the new IT equipment and a wide array of learning resources (audio CDs, French language methods, books and magazines). 

# Activities

- Autonomous studying for students to (further) develop their language skills in a modern environment
- Student events and exchanges
- Teacher exchanges and Seminars on the best practices for teaching <abbr title="Français Langue Étrangère">FLE</abbr>
- Language practice lessons
- Events focused on _La Francophonie_ (in partnership with [CRU](/campus/language-centres/cru)) for Students and Pupils

# Contact

<div class="paragraph">

Email: [upitcru@yahoo.com](mailto:upitcru@yahoo.com)

Address: [Room 110 – 111, First Floor, Faculty of Theology, Letters, History and Art, 7, Aleea Şcolii Normale, 110254, Pitești, Argeș, Romania](https://goo.gl/maps/V1sYMY8RYwmMfZtS9)

Facebook: <a class="inactive" href="#">@CRU Pitești</a>

</div>
