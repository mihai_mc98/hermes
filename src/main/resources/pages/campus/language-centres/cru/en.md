Founded in **2015** and receiving **yearly** co-financing from l'Agence Universitaire de la Francophonie, the Centre's mission is to promote the scientific and cultural _Francophonie_ at a local, national and international level.

# Activities and Events throughout the Year

Our activities are focused on 3 key **axes**: Students and Pupils, Teachers and _La Francophonie_.

## For Students and Pupils

Our events and activities are aimed at developing students' necessary transferable skills to facilitate their integration on the labour market. Our pool of services includes:

* **Career support**
* **Training courses** taught in French for students enrolled in our <a class="inactive" href="#">Francophone Degree Programmes</a>
* **Translation contests** 
* **Study and Scientific** events
* **Workshops**

# For Teachers and Academic Staff

We keep in contact and collaborate with the French teachers and academic staff in our region through:

* **International Conferences**
* **Round tables**
* _Jour du prof de français_

# Promoting _La Francophonie_

The best way to understand _La Francophonie_ is to experience it! We are looking forward to meeting **you** during one of our events: 

* _La Francophonie à l’UPIT_
* _Journée internationale de la Francophonie_
* Francophone Music and Cinema Nights and Christmas Carols

# Our Projects

| Project Name                                                                                                                                     | Duration    |
|--------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| Le Numérique à la portée de tous : métaplateforme d'intégration d'une façon innovatrice des ressources en ligne pour l'apprentissage du français | 2018 – 2020 |
| Apprendre en autonomie : une compétence clé pour la réussite professionnelle de nos étudiants                                                    | 2021        |


# Contact

<div class="paragraph">

Email: [upitcru@yahoo.com](mailto:upitcru@yahoo.com)

Address: [Room B003, Faculty of Theology, Letters, History and Art, 7, Aleea Şcolii Normale, 110254, Pitești, Argeș, Romania](https://goo.gl/maps/V1sYMY8RYwmMfZtS9)

Facebook: <a class="inactive" href="#">@CRU Pitești</a>

</div>
