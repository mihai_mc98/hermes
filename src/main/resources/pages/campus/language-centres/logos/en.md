The LOGOS Centre was founded in 2009 within the [Applied Foreign Languages Department](https://www.upit.ro/ro/faculties/facultatea-de-teologie-litere-istorie-si-arte/departamentul-limbi-straine-aplicate2) and focuses on the Research and Teaching of Foreign Languages as well as organising events for students, teachers and the wider community.

# What we do

## Foreign Language Courses

* [On-demand courses for learning a foreign language](#LOGOS-Language-Proficiency-Certification)
* Tailored/Specialised English, French and German courses for Business Professionals
* Romanian, English, French and German courses for the Entrance Exam into Higher Education Degrees
* Romanian as a foreign language to international students
* Mock Examinations for Cambridge ESOL, TOEFL, DELF/DALF and Sprachdiplom
* Organise the **LOGOS Language Proficiency Certification** Exam

## Translation

* General and specialised translations
* Certified and/or authorised translations
* Translations of documents from various fields (e.g. Medicine, STEM, Law)
* Interpretation Services
* Proofreading and linguistic review

## Research

Within the Applied Foreign Languages Department's Research Plan our Centre is active in the following fields:
* Theoretical and Applied Linguistics
* Specialised Language
* Traductology
* Anglophone and Francophone Culture and Civilization
* The Didactics of Foreign Languages

# LOGOS Language Proficiency Certification

This Exam certifies an applicants' foreign language skills for a certain <abbr title="Common European Framework of Reference for Languages">CEFR</abbr> level. The exam will assess the applicant's linguistic competences achieved in a formal/non-formal context in the 4 skills:

1. Listening
2. Speaking
3. Reading
4. Writing

## CEFR Levels

| CEFR Levels | Description  |
|-------------|--------------|
| A1          | Beginner     |
| A2          | Elementary   |
| B1          | Threshold    |
| B2          | Intermediary |
| C1          | Advanced     |
| C2          | Proficiency  |

## How to sit a LOGOS Exam

Sitting a LOGOS Foreign Language Exam is easy thanks to our simple 3-step approach:

1. [Apply](#Apply)
2. [Pay the Examination Fee](#Pay-the-Examination-Fee)
3. [Get Ready](#Get-Ready)

The language tests are organised **on-site** or **online**. Candidates will be able to express their option when submitting the Application Form. After registration and payment of the Examination Fee, candidates will receive via e-mail information regarding the examination date, time and place. For further queries, please [get in touch](#Contact).


## Apply

**Easy Apply**: Just fill out our [Online Application Form](https://forms.gle/MofnmBvTrPfbm3nL7) !

Alternatively, students may download and print the [Paper-based Application Form]( https://www.upit.ro/_document/174925/logos-formular_inscriere_testare.doc) and either submit it:
* **via e-mail**, to the Logos Centre at [logos.upit@yahoo.com](mailto:logos.upit@yahoo.com)
* **in person**, at the [Faculty's Secretariat Office](https://goo.gl/maps/RiZRq6JtJShQ5o7KA)

<span class="note-important">Important:</span> Please submit your application at least 24 hours prior to examination, or your place might not be confirmed !

## Pay the Examination Fee

Please note that your fee amount will differ, depending on your status within The University Centre of Pitești.

| Applicant Category               | Examination Fee |
|----------------------------------|-----------------|
| Enrolled Students                | 80 RON          |
| Students from other Universities | 110 RON         |
| University Staff                 | 130 RON         |
| General Candidates               | 220 RON         |

Candidates may pay the examination fee via Bank Transfer, at the [Faculty Cashier's Office](https://goo.gl/maps/RiZRq6JtJShQ5o7KA) or at the [University Centre Cashier's Office](https://g.page/official_upit).

### Bank Transfer Details

| Category          | Payment Details               |
|-------------------|-------------------------------|
| Payee Name        | CENTRUL UNIVERSITAR PITEȘTI   |
| IBAN              | RO78 TREZ 0462 0F33 0500 XXXX |
| Bank's Name       | TREZORERIA PITEŞTI            |
| Payment Reference | "Taxa atestat LOGOS"          |
| TIN (CUI)         | RO 4122183                    |

## Get Ready

Before sitting the Exam, candidates must bring a couple of documents with them for verification purposes:

1. EU National ID/Passport, **and** a photocopy
2. Valid student ID (if required)
3. Printed Receipt for the Examination Fee (or Bank Transfer)

## Exam Results and Certificates

Results are generally communicated within the **24 hours** after sitting the examination.

The Language Certificates are usually issued within 4 working days. Successful candidates may request a Provisional Certificate confirming their language proficiency level.

<span class="note">Note:</span> Any and all Provisional Certificates become **null and void** upon the issuing and receipt of the LOGOS Language Certificate.

## Replacements

In case of loss, damage or theft, applicants may be issued a replacement for their LOGOS Language Certificate.

<span class="note-important">Important: </span> Replacement certificates may only be issued within a maximum of **2 years** starting from the date of the examination.

# Contact

<div class="paragraph">

Phone: [+40 724 007 884](tel:+40724007884)

E-Mail: [logos.upit@yahoo.com](mailto:logos.upit@yahoo.com)

Address: [Room 105, Faculty of Theology, Letters, History and Art, 7, Aleea Şcolii Normale, 110254, Pitești, Argeș, Romania](https://goo.gl/maps/V1sYMY8RYwmMfZtS9)

</div>