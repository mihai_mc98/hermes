The University Centre of Pitești has two libraries where students are welcomed to study and browse our collections.

<table-of-contents depth="1" lang="en"></table-of-contents>

# Our Locations

## Main Library

The [Main Library](https://g.page/official_upit) is located in the Central Building (The Rectorate) where you will find a variety of books and academic journals in Romanian, English, French and Spanish. It holds the most important Collections for Research and Teaching in Humanities, STEM, Law, Medicine and Medicine.

## Library Hall

The [Library Hall](https://goo.gl/maps/CFLBDj1yJumY2Gs97) is in Building B, near the Student Halls of Residence.

# Opening Hours

| Week Days | Opening Hours |
|-----------|---------------|
| Monday    | 08:00 – 20:00 |
| Tuesday   | 08:00 – 18:00 |
| Wednesday | 08:00 – 20:00 |
| Thursday  | 08:00 – 20:00 |
| Friday    | 08:00 – 20:00 |
| Saturday  | Closed        |
| Sunday    | Closed        |


# Library Facilities

Students may use the Library Computers and Printers during working hours. Access is limited to for research and homework **only**. For all other activities, please consult a member of the Library Staff. 

We offer **free Wi-Fi** and electric sockets in each Library Hall, so students may also bring their own laptops and enjoy the same levels comfort.

<div class="info-box">

## Digitalisation

As part of our digitalisation endeavours, we are now offering **on-demand** scanned pages via [e-mail](#Contact) !

</div>

We offer:
* more than **300 000** titles
* A [Digital Library](http://cat-biblioteca.upit.ro/bibl/Pagina%20WEB/Site_nou/DigLib.htm), [Bibliographies](http://tinread.upit.ro/opac/public_library_lists) and an [Online Catalogue](http://biblioteca.upit.ro)
* An **Access Point to Information** (PAI) – implemented by [L'Agence Universitaire de la Francophonie](https://www.auf.org/)
* Access to the **European Documentation Centre** (EDC-UPIT)
* Mobile access to Scientific Databases via [E-nformation](https://www.e-nformation.ro/profil-acces) – please check out our [Set-Up Guide](http://cat-biblioteca.upit.ro/bibl/Pagina%20WEB/Site_nou/BazeDate.htm)

# Membership

To borrow books or consult our [Online Catalogue](http://biblioteca.upit.ro), students must be registered with the Library and hold a **valid** Library Card.

To apply or renew your Library Card, you will need:
* a valid Student ID
* your EU National ID/Passport

# Contact

## General Enquiries

<div class="paragraph">

Phone/Fax: [+40 348 453 445](tel:+40348453445)

E-Mail: [biblioteca@upit.ro](mailto:biblioteca@upit.ro)

Address: [1 Târgu din Vale Street, Pitești, Argeș, 110440, România](https://g.page/official_upit)

Website: [http://biblioteca.upit.ro](http://biblioteca.upit.ro)

</div>

## Apply or Renew your Library Card

Phone: [+40 348 453 447](tel:+40348453447)

## Borrow or Return Books and Publications

Phone: [+40 348 453 448](tel:+40348453448)

## On-Demand Scanning

<div class="paragraph">

E-Mail: <a class="inactive" href="#">biblioteca@upit.ro</a>

Phone: [+40 348 453 451](tel:+40348453451)

</div>
