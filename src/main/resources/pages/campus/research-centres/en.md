# Centres per Faculty

<table>
    <thead>
        <th>Faculty</th>
        <th>Research Centre</th>
    </thead>
    <tbody>
        <tr>
            <td>Faculty of Sciences, Physical Education and Computer Science</td>
            <td>
                <ul>
                    <li>Environmental Protection Research Centre</li>
                    <li>Biotechnological Research in Horticulture and Environmental Protection Centre</li>
                    <li>Human Performance Research Centre</li>
                    <li>Calculation Methods and Programming Research Centre</li>
                    <li>"Optomat" Research Laboratory</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Faculty of Mechanics and Technology</td>
            <td>
                <ul>
                    <li>Automotive Engineering Research Centre</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Faculty of Electronics, Computers and Communication</td>
            <td>
                <ul>
                    <li>Modelling and Simulation of Processes and Systems Research Centre</li>
                    <li>"ELECTROMET" Research Centre</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Faculty of Economic Sciences and Law</td>
            <td>
                <ul>
                    <li>Economic Analysis and Modelling Research Centre</li> 
                    <li>Law and Administration Research Centre</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Faculty of Education Sciences, Social Sciences and Psychology</td>
            <td>
                <ul>
                    <li>Research Centre for Promoting Excellence in Professional Training "PRO ED EXPERT"</li>
                    <li>Applied Psycho-pedagogy Research Centre</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>Faculty of Theology, Letters, History and Arts</td>
            <td>
                <ul>
                    <li>IMAGINES Research Centre for Imaginary, Text, Speech and Communication</li> 
                    <li>"Gh. I. Brătianu" Research Centre for History, Archaeology and Heritage</li> 
                    <li><a href="/campus/language-centres/logos">LOGOS Foreign Languages Research Centre</a></li> 
                    <li>Applied Theology Research Centre</li>
                    <li>Intangible Cultural Heritage Research Centre</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

# <abbr title="The Regional Centre for Research & Development of Innovative Materials, Processes and Products in the Automotive Industry">CRC&D-Auto</abbr>

This Centre was founded in 2015 with a €7.7M investment from the <abbr title="Sectorial Operational Program for the Increase in Economic Competitiveness">POS CCE</abbr> 221.
The Centre's **6** Laboratories come equipped with state-of-the-art R&D facilities where many Ph.D. and Masters students carry out research internships.

## Facilities

* Advanced Materials Lab
* Fuels and Lubricants Lab
* Manufacturing and Prototyping Lab
* Thermal engines, Testing and Advanced Simulation Lab
* Battery Testing Lab
* Smart Vehicles Lab
