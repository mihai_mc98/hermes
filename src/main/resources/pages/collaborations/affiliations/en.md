# International Associations and Networks

In addition to our international partners, The University Centre of Pitești is a member of various networks and associations. These international networks provide the University Centre the opportunity to extend our partnerships and to develop new projects.

<div class="cards">
    <div class="card">
        <img src="/images/Partners/AUF.jpg" alt="L'Agence Universitaire de la Francophonie (AUF)">
        <div class="card-content">
            <a href="https://www.auf.org">L'Agence Universitaire de la Francophonie (AUF)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/EUA.jpg" alt="European University Association (EUA)">
        <div class="card-content">
            <a href="https://eua.eu">European University Association (EUA)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/DRC.jpg" alt="The Danube Rectors' Conference (DRC)">
        <div class="card-content">
            <a href="https://www.drc-danube.org">The Danube Rectors' Conference (DRC)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/EfVET.jpg" alt="European Forum of Technical and Vocational Education and Training (EfVET)">
        <div class="card-content">
            <a href="https://www.efvet.org">European Forum of Technical and Vocational Education and Training (EfVET)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/EDEN.jpg" alt="European Distance Learning and E-Learning Network (EDEN)">
        <div class="card-content">
            <a href="https://www.eden-online.org">European Distance Learning and E-Learning Network (EDEN)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/IAU.jpg" alt="International Association of Universities (IAU)">
        <div class="card-content">
            <a href="https://www.iau-aiu.net">International Association of Universities (IAU)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/ATEE.jpg" alt="Association for Teacher Education in Europe (ATEE)">
        <div class="card-content">
            <a href="https://atee.education">Association for Teacher Education in Europe (ATEE)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/CELELC.jpg" alt="Conseil Européen pour les Langues/European Language Council">
        <div class="card-content">
            <a href="https://atee.education">Conseil Européen pour les Langues/European Language Council</a>
        </div>
    </div>
</div>