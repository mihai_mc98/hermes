L’[Agence Universitaire de la Francophonie](https://www.auf.org) (AUF) regroupe 1007 universités, grandes écoles, réseaux universitaires et centres de recherche scientifique utilisant la langue française dans 119 pays. Elle est l’une des plus importantes associations d’établissements d’enseignement supérieur et de recherche au monde, mais aussi l’opérateur pour l’enseignement supérieur et la recherche du Sommet de la Francophonie.

# Contact

* Vice-Rectrice des Relations Internationales, Co-Responsable CRU et CFDLA – Mme [Corina-Amelia GEORGESCU](mailto:corina.georgescu@upit.ro)
* Directrice du Centre des Relations Internationales du Centre Universitaire de Pitești – Mme [Loredana Bloju](mailto:loredana.bloju@upit.ro)
* Responsable du Bureau de la Francophonie au Centre Universitaire de Pitești – [Mme Laura Cîțu](mailto:laura.citu@upit.ro)
* Co-Responsable CRU et CFDLA, Responsable des Bourses Eugen Ionescu au Centre Universitaire de Pitești – Mme [Liliana Voiculescu](mailto:liliana.voiculescu@upit.ro)
