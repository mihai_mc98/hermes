Le Centre Universitaire de Pitești participe au [Programme « Eugen Ionescu »](https://www.auf.org/nouvelles/appels-a-candidatures/bourses-de-recherche-doctorale-et-postdoctorale-eugen-ionescu-appel-candidatures-2022/) qui permet aux chercheurs des pays membres et observateurs de l’OIF et de l’Algérie, issus des établissements d’enseignement supérieurs membres de l’AUF, de bénéficier d’une mobilité de recherche de trois mois.

<div class="info-box">

## Délai : L’appel international à candidatures est ouvert jusqu’à 11 mars 2022 (23:00, GMT + 2)
</div>

# Niveau

Ce programme s’adresse aux doctorants et aux enseignants-chercheurs étrangers qui veulent faire un stage de recherche dans les institutions d’enseignement supérieur de Roumanie, membres de l’Agence Universitaire de la Francophonie.

# Financement

Le Programme « Eugen Ionescu » est financé par le Gouvernement de la Roumanie, à travers le Ministère roumain des Affaires Étrangères. La gestion effective de ces bourses est coordonnée par l’Agence Universitaire de la Francophonie

# Modalités de candidature

La candidature est un processus qui comporte 2 étapes :
1. **La préinscription** au programme suppose l’obtention d’une attestation d’accueil de la part de l’université roumaine que le candidat a choisie et où il doit envoyer un dossier de préinscription (voir les règlements ci-dessous). La préinscription doit se faire donc suffisamment à l’avance pour permettre à l’université d’accueil de traiter le dossier
2. Le dépôt de la candidature auprès de l’AUF : le candidat doit remplir un dossier de candidature, **impérativement et exclusivement en ligne **:
    * [Demande de bourse doctorale Eugen Ionescu 2021-2022](https://formulaires.auf.org/login/)
    * [Demande de bourse postdoctorale Eugen Ionescu 2021-2022](https://formulaires.auf.org/login/)

Le formulaire représente le dossier de candidature. Aucun autre dossier ne doit pas être envoyé à l’AUF !

# Documents de référence pour l’appel à candidatures

* [Présentation du Programme des Bourses Eugen Ionescu 2022](https://www.auf.org/wp-content/uploads/2022/01/Pr%C3%A9sentation-du-Programme-des-Bourses-Eugen-Ionescu-2022.pdf)
* [Liste des universités roumaines d’accueil et leurs thématiques de recherche](https://www.auf.org/wp-content/uploads/2022/01/BOURSES-EUGEN-IONESCU-THEMATIQUES-DE-RECHERCHE-2021-2022.docx)
* [Règlement pour les bourses de doctorat](https://www.auf.org/wp-content/uploads/2022/01/R%C3%A8glement-pour-les-bourses-de-doctorat.pdf)
* [Règlement pour les bourses de postdoctorat](https://www.auf.org/wp-content/uploads/2022/01/R%C3%A8glement-pour-les-bourses-de-postdoctorat-1.pdf)

# Modèles de documents fournis par l’AUF

* [Modèle de fiche signalétique](https://www.auf.org/wp-content/uploads/2022/01/Mod%C3%A8le-de-fiche-signal%C3%A9tique-2022.docx)
* [Modèle de projet de recherche](https://www.auf.org/wp-content/uploads/2022/01/Mod%C3%A8le-de-projet-de-recherche-2022.docx)
* [Modèle attestation d’accord de l’établissement d’accueil](https://www.auf.org/wp-content/uploads/2022/01/Mode%CC%80le-attestation-daccord-de-le%CC%81tablissement-daccueil-2022.docx)

# Liens utiles

* [La liste des 88 États et gouvernements membres et observateurs de l’OIF](https://www.francophonie.org/88-etats-et-gouvernements-125)
* [La liste des 1007 établissements membres de l’AUF](https://www.auf.org/les_membres/nos-membres/page/3/)

# Contact

Responsable des Bourses « Eugen Ionescu » – [Mme Liliana Voiculescu](mailto:liliana.voiculescu@upit.ro)
