Le rayonnement de la francophonie au Centre Universitaire de Pitești est assuré par les deux centres francophones placés sous le haut patronage de l’Agence Universitaire de la Francophonie :

<table-of-contents depth="1" lang="fr"></table-of-contents>

Les deux organisent systématiquement des événements ayant pour buts la promotion des valeurs de la francophonie, ainsi que de la coopération entre les centres universitaires francophones ou entre le Centre Universitaire de Pitești et ses partenaires

# Centre de Réussite Universitaire – CRU

Créé en 2015 et cofinancé annuellement par l’Agence Universitaire de la Francophonie, a comme mission principale la promotion de la francophonie scientifique et culturelle au niveau local, régional, national et international. Les événements déroulés au sein du CRU ont comme objectif la formation des compétences transversales des étudiants pour une meilleure intégration sur le marché du travail. Les principaux axes visés par les activités du CRU sont :
* **la recherche** – sessions scientifiques des étudiants et des doctorants, conférence internationale annuelle de la faculté, concours de traductions pour les étudiants et les élèves, tables rondes avec les professeurs de français de l’enseignement supérieur et préuniversitaire
* **l’orientation professionnelle** pour les étudiants de l’UPIT et les élèves de la région
* **la culture francophone** – Journée internationale de la Francophonie, soirées musique et cinéma francophones, chansons de Noël, etc.
* **les formations** en français pour les étudiants des programmes francophones de l’UPIT.

# Centre Francophone de Développement Linguistique Autonome – CFDLA

Créé en 2019 et financé par l’Agence Universitaire de la Francophonie, est un espace qui sert de salle de séminaire et travaux dirigés, laboratoire et de centre de documentation mettant à la disposition des étudiants des ressources numériques (ordinateurs, CD, DVD, méthodes de langue française) ou matérielles (livres, magazines, fiches-support).

La mission principale du CFDLA est de développer les compétences nécessaires pour l’autonomisation de l’étudiant dans le processus d’apprentissage en l’apprenant à apprendre au cours de ses études universitaires.