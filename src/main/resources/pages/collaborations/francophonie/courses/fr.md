Le Centre Universitaire de Pitești déroule, à présent, des programmes de licence et de master qui visent l’étude de la langue et de la culture française, tout comme des programmes qui impliquent des collaborations de longue durée avec les universités françaises. Celles-ci ont également permis la soutenance des thèses en cotutelle ou des thèses soutenues en France.

# Études de Licence

## La Faculté de Théologie, Lettres, Histoire et Arts

| Programme d’études                                                 | Durée    | Crédits ECTS |
|--------------------------------------------------------------------|----------|--------------|
| Langue et littérature anglaises – Langue et littérature françaises | 3 années | 180 ECTS     |
| Langue et littérature roumaines – Langue et littérature françaises | 3 années | 180 ECTS     |
| Langues modernes appliquées (anglais – français)                   | 3 années | 180 ECTS     |

# Études de Master

## La Faculté de Théologie, Lettres, Histoire et Art

| Programme d’études                                                                 | Durée    | Crédits ECTS |
|------------------------------------------------------------------------------------|----------|--------------|
| Traductologie – Langue Anglaise/Langue Française. Traductions en contexte européen | 2 années | 120 ECTS     |
| Langages spécialisés et Traduction assistée par ordinateur                         | 2 années | 120 ECTS     |

## La Faculté de Mécanique et Technologie

| Programme d’études                                        | Mentions                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|-----------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| L’Ingénierie et la Gestion de la Fabrication des Produits | Un master en collaboration avec l’Université de Lorraine - site Metz, depuis 2012 (Les Professeurs Sebastien Mercier et Marion Martiny) et, depuis 2015, le programme se déroule également en coopération avec  l’ENSTA Bretagne (les Professeurs Claudiu Bădulescu et Younes Demmouche)                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Gestion de la logistique                                  | Le master se déroule en collaboration avec l’Université de Lorraine - site Metz (Pr. Sauer Nathalie, Pr. Schutz Jeremie et Pr. Dellagie Sofiene)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| Science et Technologie des matériaux (STM)                | Le master est initié en 1995 par Madame le Professeur des Universités Mărioara Abrudeanu (Centre Universitaire de Pitești) et par M. le Professeur des Universités Jean Philibert (Institut de Matériaux-Université Paris Sud Orsay), avec le soutien de l’Ambassade de France en Roumanie. Il réunit plusieurs universités et Grandes Écoles françaises : Université Paris Sud (Pr. Jean Philibert et Pr. Georges Cizeron), Université Paris Nord-École Centrale Paris (Pr. Claude Petot et Dr.  Georgette Petot-Ervas, directeur de recherches CNRS), INP Toulouse (ENSIACET - Pr. Alain Gleizes et ENI Trabes - Pr. Jacques-Alain Petit), INSA Lyon (Pr. Henri Mazille et Pr. Jean Pierre Millet), Université de Metz (Pr. Marcel Berveiller) |

# Programme de doctorat

Une partie des anciens doctorants ayant soutenu des thèses en cotutelle enseignent à présent à l’UPIT :
* Prof. Eduard Laurenṭiu Niṭu
* Prof. Adrian Clenci
* Conf. Dan Constantin Anghel
* Conf. Monica Iordache
* Conf. Victor Iorga
* Conf. Alin Rizea
* Conf. Liliana Voiculescu
* Conf. Cătălin Zaharia
* Conf. Adriana Apostol (le chargé de cours)

# Quelques thèses soutenues en Cotutelle

| Nom                   | Titre du thèse                                                                                                                  | Universités                                                                                               | Année de soutenance |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|---------------------|
| Conf. Cătălin Zaharia | Études théoriques et expérimentales concernant l’évaluation des performances d’une automobile soumise aux accélérations         | Centre Universitaire de Pitești, Roumanie et Conservatoire National des Arts et Métiers, Paris, France    | 2006                |
| Conf. Adriana Apostol | Le Fantastique littéraire en France et en Roumanie. Quelques aspects au XIXe siècle : Une rhétorique de la (dé)construction     | Centre Universitaire de Pitești, Roumanie et Université Paris XII Val-de-Marne (Paris Est)                | 2011                |
| Conf. Victor Iorga    | Études par simulation numérique des écoulements dans le conduit d’admission d’un moteur à levée de soupape d’admission variable | Centre Universitaire de Pitești, Roumanie et le Conservatoire National des Arts et Métiers, Paris, France | 2012                |

# Quelques thèses soutenues à des Universités françaises

| Nom                           | Titre du thèse                                                               | Université                    | Année de soutenance |
|-------------------------------|------------------------------------------------------------------------------|-------------------------------|---------------------|
| Conf. Corina Amelia Georgescu | Le regard comme signe de la mentalité dans le roman du XIX siècle            | Université Paris IV Sorbonne  | 2004                |
| Conf. Liliana Voiculescu      | La Representation des identités sociales dans le roman canadien contemporain | Université Jean Moulin Lyon 3 | 2009                |
