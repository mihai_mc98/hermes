<div class="cards">
    <div class="card">
        <img src="/images/Cards/Woman leading a meeting.jpg" alt="Woman leading a meeting">
        <div class="card-content">
            <a href="/collaborations/francophonie/partners">Coopération avec les partenaires francophones</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/People standing behind an AUF Banner.jpg" alt="People standing behind an AUF Banner">
        <div class="card-content">
            <a href="/collaborations/francophonie/events">Promotion de la francophonie – Événements</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Logos/CRU.jpg" alt="Logo of Centre de Réussite Universitaire">
        <div class="card-content">
            <a href="/collaborations/francophonie/centres">Centres francophones</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Woman standing next to a whiteboard.jpg" alt="People standing behind an AUF Banner">
        <div class="card-content">
            <a href="/collaborations/francophonie/courses">Programmes d’études</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Bills and Fees 2.jpg" alt="Bills and Fees">
        <div class="card-content">
            <a href="/collaborations/francophonie/bourses">Le Programme de bourses « Eugen Ionescu »</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Partners/AUF.jpg" alt="L'Agence Universitaire de la Francophonie (AUF)">
        <div class="card-content">
            <a href="/collaborations/francophonie/auf">L'Agence Universitaire de la Francophonie (AUF)</a>
        </div>
    </div>
</div>