Le Centre Universitaire de Pitești continue ses traditions francophones par la collaboration constante qu’elle a avec des établissements nationaux et internationaux ou par les projets déroulés avec des partenaires francophones.

# Accords avec des universités des pays francophones

Le Centre Universitaire de Pitești a **11** accords bilatéraux avec des établissements de plusieurs pays francophones (la République Française, le Maroc, l’Algérie, le Canada) et **35** accords Erasmus (la République Française, le Maroc, l’Algérie, le Canada, la Tunisie) et a comme objectif de conclure de nouveaux accords.

# Partenariat traditionnel avec l’<abbr title="Agence Universitaire de la Francophonie">AUF</abbr>

Par notre partenariat avec l’Agence Universitaire de la Francophonie, notre Université peut accueillir des boursiers du programme « Eugen Ionescu ».

# Des partenariats avec des associations nationales

Ces partenariats promeuvent les valeurs de la francophonie comme l’<abbr title="Association Roumaine des Professeurs Francophones">ARPR</abbr>

# Des mobilités d’enseignement et de formation Erasmus+ dans des pays francophones

- La Bourse du Gouvernement Français
- La Bourse du Gouvernement Roumain
- La Bourse de complément de spécialisation du Gouvernement du Canada,
- La Bourse de formation à la recherche de l’<abbr title="Agence Universitaire de la Francophonie">AUF</abbr>
- La Bourse de l’<abbr title="Agence Universitaire de la Francophonie">AUF</abbr> pour la participation à des conférences scientifiques
- Bourses Erasmus Université Paris XII Val-de-Marne (Paris Est)
- Stages de recherche doctorale dans le cadre des universités ou des institutions françaises et francophones (l’École doctorale LIS - Lettres Idées Savoirs, Université Paris Est, l’Université de Bourgogne, Centre Interlangues – Texte, Image, Langage, l’Institut National pour Langues et Cultures Orientales (INALCO), Paris, le Parlement européen, Luxembourg) 

# Des projets financés par l’<abbr title="Agence Universitaire de la Francophonie">AUF</abbr>

Ces projets ont impliqué la promotion de la francophonie ou de la langue française et auxquels les enseignants du Centre Universitaire de Pitești ont pris part ou des projets avec des partenaires francophones

<table>
    <thead>
        <th>Projet</th>
        <th>Description</th>
    </thead>
    <tbody>
        <tr>
            <td>TradSpe (La traduction spécialisée : domaine de recherche pour la construction d’un modèle didactique opératoire en contexte pluriculturel)</td>
            <td>Projet financé par l’AUF (BECO/P1/2011/46115FT103), 2012-2013 (Mme Cristina Ilinca – membre dans l’équipe de recherche)</td>
        </tr>
        <tr>
            <td>Ressources de Recherche et de Didactique Universitaire (ResReDU)</td>
            <td>Projet financé par l’AUF (BECO/P1/2011/46115FT105), 2012-2013 (Mme Cristina Ilinca – membre dans l’équipe de recherche)</td>
        </tr>
        <tr>
            <td>Soutien aux formations francophones ou partiellement francophones de niveau master, Master Traductologie. Domaine franco-roumain</td>
            <td>Projet financé par l’AUF BECO, €10.000 no.15.067/18.12.2014., 2014- 2016. (Mme Adriana Apostol, Mme Diana Lefter)</td>
        </tr>
        <tr>
            <td>Le Numérique à la portée de tous - métaplateforme d’intégration d’une façon innovatrice des ressources en ligne pour l’apprentissage du français - Centre de Réussite Universitaire</td>
            <td>Projet financé par l’AUF BECO, €20.000 2018-2020 (Mme Corina Georgescu, Mme Liliana Voiculescu)</td>
        </tr>
        <tr>
            <td>Apprendre en autonomie : une compétence-clé pour la réussite professionnelle de nos étudiants</td>
            <td>Projet financé par l’AUF BECO, €3.000 2021-2022 (Mme Corina Georgescu, Mme Liliana Voiculescu)</td>
        </tr>
        <tr>
            <td>FLASHMIND (plateforme gratuite d’enseignement-apprentissage pour les élèves de collège)</td>
            <td>
                Projet financé par le Programme Erasmus+ (2019) - KA201, avec des partenaires francophones :

* EDULOG (France, coordonnateur)
* LogoPsyCom (Belgique)
* European Education and Learning Institute (Grèce)
* Euphoria (Italie)
* Les Apprimeurs (France)
* Le Centre Universitaire de Pitești
            </td>
        </tr>
    </tbody>
</table>

# Des colloques scientifiques

| Événement                                                                               | Description                                                                                                                                                                                                                                                                                                                                                                                                                 |
|-----------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Langue et Littérature. Repères Identitaires en Contexte Européen                        | La conférence annuelle de la Faculté de Théologie, Lettres, Histoire et Arts, accueille chaque année des enseignants-chercheurs de tout le monde au sein des sections dédiées à la langue et littérature françaises, aux littératures francophones et à la didactique du FLE. La conférence est organisée sous le haut patronage de l’Agence Universitaire de la Francophonie qui offre des financements pour cet événement |
| COFRET : Le Colloque Francophone en Energie, Environnement, Économie et Thermodynamique | Le colloque est organisé par le Centre Universitaire de Pitești en collaboration avec l’Université Polytechnique de Bucarest                                                                                                                                                                                                                                                                                                |
| Les Ateliers de Didactique du FLE                                                       | C’est un événement annuel, déroulé depuis 2016, qui réunit des spécialistes roumains et étrangers du milieu universitaire et préuniversitaire, organisé par la Faculté de Théologie, Lettres, Histoire et Arts                                                                                                                                                                                                              |
| La Didactique à l’ère du numérique                                                      | Journée d’études, organisée par notre Centre de Réussite Universitaire (CRU) de l’UPIT, notre Centre Francophone de Développement Linguistique Autonome (CFDLA) et l’ARPR. Cet événement a réuni des spécialistes roumains et étrangers du milieu universitaire et préuniversitaire                                                                                                                                         |
