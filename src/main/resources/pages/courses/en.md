<div class="info-box">

## Page under Construction

To find out more about our Study Programmes, please consult each Faculty's website.

</div>

* [Faculty of Sciences, Physical Education and Informatics](https://www.upit.ro/faculties/facultatea-de-stiinte-educatie-fizica-si-informatica)
* [Faculty of Mechanics and Technology](https://www.upit.ro/academia-reorganizata/facultatea-de-mecanica-si-tehnologie-2)
* [Faculty of Electronics, Communications and Computers](https://www.upit.ro/academia-reorganizata/facultatea-de-electronica-comunicatii-si-calculatoare-2)
* [Faculty of Education Sciences, Social Sciences and Psychology](https://www.upit.ro/academia-reorganizata/facultatea-de-stiintele-educatiei-stiinte-sociale-si-psihologie-2)
* [Faculty of Economy and Law](https://www.upit.ro/academia-reorganizata/facultatea-de-stiinte-economice-si-drept-1)
* [Faculty of Theology, Lettres, History and Arts](https://www.upit.ro/academia-reorganizata/facultatea-de-teologie-litere-istorie-si-arte)

# Study Programmes

## Most Popular Degrees

* [Biology](https://www.upit.ro/_document/111112/biology.pdf)
* [Business Administration](https://www.upit.ro/_document/111114/business_administration.pdf)
* [Computer Science](https://www.upit.ro/_document/111115/computer_science.pdf)
* Double Major in [Language and Literature: English with French/German/Spanish](https://www.upit.ro/_document/111116/english-fr.pdf)
* [Materials' Science and Technology](https://www.upit.ro/_document/111117/materials_science_and_technology.pdf)
* [Medical Biology](https://www.upit.ro/_document/111118/medical_biology.pdf)
* [Physical Therapy and Special Motriciy](https://www.upit.ro/_document/111119/physical_therapy_and_special_motricity.pdf)
* [The Romanian Language Preparatory Year](https://www.upit.ro/_document/111120/preparatory_year_for_learning_romanian.pdf)
* [Psychology](https://www.upit.ro/_document/111121/psihology.pdf)
* [Strategic Management](https://www.upit.ro/_document/111122/strategic_management.pdf)

## Master's Degrees taught in a Foreign Language

Students undertaking any of the following degrees will be expected to meet the Foreign Language requirements before being accepted to the Degree.

| Master's Degree                                                                                                                                                                                                                                                                                                                                                      | Teaching Language |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|
| [Advanced Techniques for Information Processing](https://www.upit.ro/ro/academia-reorganizata/facultatea-de-stiinte-educatie-fizica-si-informatica/departamente-la-nivelul-facultatii-de-stiinte-educatie-fizica-si-informatica/departamentul-matematica-informatica2/mastere-departamentul-matematica-informatica2/tehnici-avansate-pentru-prelucrarea-informatiei) | English           |
| [Materials' Science and Technology](https://www.upit.ro/ro/faculties/facultatea-de-mecanica-si-tehnologie-2/departamentul-fabricatie-si-management-industrial2/programe-de-master-dfmi/stiinta-si-tehnologia-materialelor)                                                                                                                                           | French            |
| [Automotive Engineering for Sustainable Mobility](https://www.upit.ro/ro/academia-reorganizata/facultatea-de-mecanica-si-tehnologie-2)                                                                                                                                                                                                                               | French            |
| [Strategic Management and Business Development](https://www.upit.ro/ro/academia-reorganizata/facultatea-de-stiinte-economice-si-drept-1/departamentul-management-i-administrarea-afacerilor2/programe-de-master-dmaa/management-strategic-si-dezvoltarea-afacerilorengleza)                                                                                          | English           |