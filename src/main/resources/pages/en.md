Welcome to the International Relations Centre of The University Centre of Pitești!

<div class="cards">
    <div class="card">
        <img src="/images/Cards/EU Flag.jpg" alt="The flag of the European Union">
        <div class="card-content">
            <a href="/erasmus">Erasmus+</a>
            <p>Discover our Erasmus+ programmes and what to do: from the application to arrival</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Students Playing Instruments.jpg" alt="Students Playing Instruments">
        <div class="card-content">
            <a href="/admissions">Admissions</a>
            <p>Find out more about how to apply as an international student at The University Centre of Piteşti</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Women in the Traditional Romanian Blouse.jpg" alt="Women in the Traditional Romanian Blouse">
        <div class="card-content">
            <a href="/about-us">Why UPIT ?</a>
            <p>Find out what the advantages of becoming a student at The University Centre of Pitești are</p>
        </div>
    </div>
</div>