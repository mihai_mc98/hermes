Pentru date actualizate, vă rugăm să consultați [Lista Oficială a Țărilor Participante](https://erasmus-plus.ec.europa.eu/ro/programme-guide/part-a/eligible-countries).

<table-of-contents depth="2" lang="ro"></table-of-contents>

# KA103/KA131 – Țări participante

## State-membre ale Uniunii Europene

| Countries (A – F) | Countries (F – M) | Countries (N – Z)      |
|-------------------|-------------------|------------------------|
| Austria           | Franța            | Polonia                |
| Belgia            | Germania          | Portugalia             |
| Bulgaria          | Grecia            | România                |
| Republica Cehă    | Irlanda           | Slovacia               |
| Cipru             | Italia            | Slovenia               |
| Croația           | Letonia           | Spania                 |
| Danemarca         | Lituania          | Suedia                 |
| Estonia           | Luxemburg         | Regatul Țărilor de Jos |
| Finlanda          | Malta             | Ungaria                |

## Țări non-UE

| Țări din <abbr title="Spațiul Economic European">SEE</abbr> | Țări candidate UE           |
|-------------------------------------------------------------|-----------------------------|
| Islanda                                                     | Republica Macedonia de Nord |
| Liechtenstein                                               | Serbia                      |
| Norvegia                                                    | Turcia                      |

# KA107/KA171 – Țări partenere

## Regiunea 1: Balcanii de Vest

* Albania
* Bosnia și Herțegovina
* Kosovo
* Muntenegru

## Regiunea 2: Țările Parteneriatului Estic

* Armenia
* Azerbaidjan
* Belarus
* Georgia
* Moldova
* Teritoriul Ucrainei

## Regiunea 3: Țările sudmediteraneene

| Countries (A – L) | Countries (L – Z) |
|-------------------|-------------------|
| Algeria           | Libia             |
| Egipt             | Maroc             |
| Israel            | Palestina         |
| Iordania          | Siria             |
| Liban             | Tunisia           |

## Regiunea 4: Federația Rusă

* Teritoriul Rusiei, astfel cum este recunoscut de dreptul internațional

## Regiunea 5

* Andorra
* Monaco
* San Marino
* Statul Cetății Vaticanului

## Regiunea 6: Asia

| Countries (A – C) | Countries (C – L)                   | Countries (M – N) | Countries (O – Z) |
|-------------------|-------------------------------------|-------------------|-------------------|
| Afganistan        | Republica Populară Democrată Coreea | Malaezia          | Pakistan          |
| Bangladesh        | Filipine                            | Maldive           | Sri Lanka         |
| Bhutan            | India                               | Mongolia          | Thailanda         |
| Cambodgia         | Indonezia                           | Myanmar           | Vietnam           |
| China             | Laos                                | Nepal             |                   |

## Regiunea 7: Asia Centrală

* Kazahstan
* Kârgâzstan
* Tadjikistan
* Turkmenistan
* Uzbekistan

## Regiunea 8: America Latină

| Countries (A – C) | Countries (C – E) | Countries (F – N) | Countries (O – Z) |
|-------------------|-------------------|-------------------|-------------------|
| Argentina         | Costa Rica        | Guatemala         | Panama            |
| Bolivia           | Cuba              | Honduras          | Paraguay          |
| Brazilia          | Ecuador           | Mexic             | Peru              |
| Columbia          | El Salvador       | Nicaragua         | Venezuela         |

## Regiunea 9

* Iran
* Irak
* Yemen

## Regiunea 10

* Africa de Sud

## Regiunea 11: ACP

| Countries (A – C)       | Countries (C – F)           | Countries (G – L)   | Countries (M – N)                | Countries (O – S)            | Countries (S – Z)                        |
|-------------------------|-----------------------------|---------------------|----------------------------------|------------------------------|------------------------------------------|
| Angola                  | Republica Coasta de Fildeș  | Gambia              | Madagascar                       | Palau                        | Sudan                                    |
| Antigua și Barbuda      | Comore                      | Ghana               | Malawi                           | Papua Noua Guinee            | Sudanul de Sud                           |
| Bahamas                 | Congo                       | Grenada             | Mali                             | Rwanda                       | Suriname                                 |
| Barbados                | Republica Democratică Congo | Guineea             | Insulele Marshall                | Saint Kitts și Nevis         | Tanzania                                 |
| Belize                  | Insulele Cook               | Guineea-Bissau      | Mauritania                       | Saint Lucia                  | Republica Democratică a Timorului de Est |
| Benin                   | Djibouti                    | Guineea Ecuatorială | Mauritius                        | Saint Vincent și Grenadinele | Togo                                     |
| Botswana                | Dominica                    | Guyana              | Statele Federate ale Microneziei | Samoa                        | Tonga                                    |
| Burkina Faso            | Republica Dominicană        | Haiti               | Mozambic                         | Sao Tome și Principe         | Trinidad și Tobago                       |
| Burundi                 | Eritreea                    | Jamaica             | Namibia                          | Senegal                      | Tuvalu                                   |
| Cabo Verde              | Eswatini                    | Kenya               | Nauru                            | Seychelles                   | Uganda                                   |
| Camerun                 | Etiopia                     | Kiribati            | Niger                            | Sierra Leone                 | Vanuatu                                  |
| Republica Centrafricană | Fiji                        | Lesotho             | Nigeria                          | Insulele Solomon             | Zambia                                   |

## Regiunea 12: Consiliul de Cooperare al Golfului

* Emiratele Arabe Unite
* Arabia Saudită
* Bahrain
* Kuweit
* Oman
* Qatar

## Regiunea 13: Alte țări industrializate

| Countries (A – J) | Countries (K – Z)          |
|-------------------|----------------------------|
| Australia         | Macao                      |
| Brunei            | Noua Zeelandă              |
| Canada            | Singapore                  |
| Republica Coreea  | Statele Unite ale Americii |
| Chile             | Taiwan                     |
| Hong Kong         | Uruguay                    |
| Japonia           |                            |

## Regiunea 14

* Insulele Feroe
* Elveția
* Regatul Unit