<div class="info-box">

## Atenție ! Țările programului Erasmus+ 2021 – 2027 s-au schimbat !

Pentru programul Erasmus pre-2021, vă rugăm să consultați [Lista țărilor participante pre-2021](/erasmus/about/countries/pre-2021) !

</div>

Pentru date actualizate, vă rugăm să consultați [Lista Oficială a Țărilor Participante](https://erasmus-plus.ec.europa.eu/ro/programme-guide/part-a/eligible-countries).

<table-of-contents depth="2" lang="ro"></table-of-contents>

# KA103/KA131 – Țări participante

## State-membre ale Uniunii Europene

| Countries (A – F) | Countries (F – M) | Countries (N – Z)      |
|-------------------|-------------------|------------------------|
| Austria           | Franța            | Polonia                |
| Belgia            | Germania          | Portugalia             |
| Bulgaria          | Grecia            | România                |
| Republica Cehă    | Irlanda           | Slovacia               |
| Cipru             | Italia            | Slovenia               |
| Croația           | Letonia           | Spania                 |
| Danemarca         | Lituania          | Suedia                 |
| Estonia           | Luxemburg         | Regatul Țărilor de Jos |
| Finlanda          | Malta             | Ungaria                |

## Țări non-UE

| Țări din <abbr title="Spațiul Economic European">SEE</abbr> | Țări candidate UE           |
|-------------------------------------------------------------|-----------------------------|
| Islanda                                                     | Republica Macedonia de Nord |
| Liechtenstein                                               | Serbia                      |
| Norvegia                                                    | Turcia                      |

# KA107/KA171 – Țări partenere

## Regiunea 1: Balcanii de Vest

* Albania
* Bosnia și Herțegovina
* Kosovo
* Muntenegru

## Regiunea 2: Vecinătatea estică

* Armenia
* Azerbaidjan
* Belarus
* Georgia
* Moldova
* Teritoriul Ucrainei

## Regiunea 3: Țările sud mediteraneene

| Countries (A – L) | Countries (L – Z) |
|-------------------|-------------------|
| Algeria           | Libia             |
| Egipt             | Maroc             |
| Israel            | Palestina         |
| Iordania          | Siria             |
| Liban             | Tunisia           |


## Regiunea 4: Federația Rusă

* Teritoriul Rusiei, astfel cum este recunoscut de dreptul internațional

## Regiunea 5: Asia

| Countries (A – F)                   | Countries (G – M) | Countries (M – Z) |
|-------------------------------------|-------------------|-------------------|
| Bangladesh                          | India             | Myanmar           |
| Bhutan                              | Indonezia         | Nepal             |
| Cambodgia                           | Laos              | Pakistan          |
| China                               | Malaezia          | Sri Lanka         |
| Republica Populară Democrată Coreea | Maldive           | Thailanda         |
| Filipine                            | Mongolia          | Vietnam           |

### Țări cu venituri ridicate

* Brunei
* Coreea
* Hong Kong
* Japonia
* Macao
* Singapore
* Taiwan

## Regiunea 6: Asia Centrală

* Afganistan
* Kazahstan
* Kârgâzstan
* Tadjikistan
* Turkmenistan
* Uzbekistan

## Regiunea 7: Orientul Mijlociu

* Iran
* Irak
* Yemen

### Țări cu venituri ridicate

* Emiratele Arabe Unite
* Arabia Saudită
* Bahrain
* Kuweit
* Oman
* Qatar

## Regiunea 8: Zona Pacificului

| Countries (A – M)                | Countries (N – S) | Countries (S – Z)                        |
|----------------------------------|-------------------|------------------------------------------|
| Insulele Cook                    | Nauru             | Insulele Solomon                         |
| Fiji                             | Niue              | Republica Democratică a Timorului de Est |
| Kiribati                         | Palau             | Tonga                                    |
| Statele Federate ale Microneziei | Samoa             | Vanuatu                                  |

### Țări cu venituri ridicate

* Australia
* Noua Zeelandă

## Regiunea 9: Africa subsahariană

| Countries (A – C)       | Countries (C – G)           | Countries (G – M)   | Countries (M – S)    | Countries (S – Z) |
|-------------------------|-----------------------------|---------------------|----------------------|-------------------|
| Africa de Sud           | Republica Coasta de Fildeș  | Ghana               | Mauritania           | Sierra Leone      |
| Angola                  | Comore                      | Guineea             | Mauritius            | Somalia           |
| Benin                   | Congo                       | Guineea-Bissau      | Mozambic             | Sudan             |
| Botswana                | Republica Democratică Congo | Guineea Ecuatorială | Namibia              | Sudanul de Sud    |
| Burkina Faso            | Djibouti                    | Kenya               | Niger                | Tanzania          |
| Burundi                 | Eritreea                    | Lesotho             | Nigeria              | Togo              |
| Cabo Verde              | Eswatini                    | Liberia             | Rwanda               | Uganda            |
| Camerun                 | Etiopia                     | Madagascar          | Sao Tome și Principe | Zambia            |
| Republica Centrafricană | Gabon                       | Malawi              | Senegal              | Zimbabwe          |
| Ciad                    | Gambia                      | Mali                | Seychelles           |

## Regiunea 10: America Latină

| Countries (A – C) | Countries (D – N) | Countries (O – Z) |
|-------------------|-------------------|-------------------|
| Argentina         | Ecuador           | Panama            |
| Bolivia           | El Salvador       | Paraguay          |
| Brazilia          | Guatemala         | Peru              |
| Chile             | Honduras          | Uruguay           |
| Columbia          | Mexic             | Venezuela         |
| Costa Rica        | Nicaragua         |                   |

## Regiunea 11: Caraibe

| Countries (A – B)  | Countries (C – G)    | Countries (G – S)    | Countries (S – Z)            |
|--------------------|----------------------|----------------------|------------------------------|
| Antigua și Barbuda | Cuba                 | Guyana               | Saint Lucia                  |
| Bahamas            | Dominica             | Haiti                | Saint Vincent și Grenadinele |
| Barbados           | Republica Dominicană | Jamaica              | Suriname                     |
| Belize             | Grenada              | Saint Kitts și Nevis | Trinidad și Tobago           |

## Regiunea 12: SUA și Canada

* Canada
* Statele Unite ale Americii

## Regiunea 13

* Andorra
* Monaco
* San Marino
* Statul Cetății Vaticanului

## Regiunea 14

* Insulele Feroe
* Elveția
* Regatul Unit