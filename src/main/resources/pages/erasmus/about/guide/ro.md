Este un instrument destinat:
- oricui dorește să cunoască oportunitățile acestui program
- celor care doresc să obțină o **finanțare** depunând proiecte specifice diferitelor componente ale programului
- organizațiilor participante
- persoanelor (elevi, stagiari, ucenici, studenți, cursanți adulți, tineri, voluntari sau cadre didactice universitare, profesori, formatori, lucrători de tineret, profesioniști în domeniile educației, formării, tineretului și sportului etc.)

În fiecare an, mii de proiecte sunt prezentate de organizații din întreaga Europă pentru a primi sprijin financiar prin programul Erasmus+. Acestea sunt supuse unui proces de evaluare transparent care are ca scop acordarea de granturi pentru cele mai bune proiecte.

În vederea depunerii unui astfel de proiect, vă rugăm să consultați:
1. [Ghidul Programului Erasmus+ 2021 – 2027](https://erasmus-plus.ec.europa.eu/sites/default/files/2021-11/2022-erasmusplus-programme-guide_ro.pdf)
2. Website-ul [Agenției Naționale pentru Programe Comunitare în Domeniul Educației și Formării Profesionale](https://www.anpcdefp.ro/)