# Ce este programul Erasmus+ ?

Erasmus+ este un program al Uniunii Europene dedicat educației, formării, tineretului și sportului.

<div class="boxes">
    <a href="/erasmus/about/structure">Structura programului</a>
    <a href="/erasmus/about/countries">Țările participante</a>
    <a href="/erasmus/about/guide">Ghidul Programului Erasmus+</a>
</div>

# Prioritățile programului

- Incluziune și diversitate
- Transformarea digitală
- Mediul și combaterea schimbărilor climatice
- Participarea la viața democratică

# Obiective

- Sprijină prioritățile și activitățile definite în contextul Spațiului european al educației, în Planul de acțiune pentru educația digitală
- Pune în aplicare agenda pentru competențe în Europa
- Sprijină Pilonul european al drepturilor sociale
- Pune în aplicare Strategia UE pentru tineret 2019-2027
- Dezvoltă dimensiunea europeană în domeniul sportului

# Oportunități

Erasmus+ oferă oportunități de mobilitate și cooperare în următoarele domenii:
- Învățământ superior
- Educație și formare profesională
- Educație școlară (inclusiv îngrijire și educația timpurie)
- Educație pentru adulți
- Tineret
- Sport

# Obiective specifice

- Promovarea **mobilității persoanelor și a grupurilor în scopul învățării**, precum și cooperarea, calitatea, incluziunea și echitatea, excelența, creativitatea și inovarea la nivelul organizațiilor și al politicilor din domeniul educației și formării
- Promovarea **mobilității în scopul învățării nonformale și informale**, participarea activă a tinerilor, precum și cooperarea, calitatea, incluziunea, creativitatea și inovarea la nivelul organizațiilor și al politicilor din domeniul tineretului
- Promovarea **mobilității personalului din domeniul sportului în scopul învățării**, precum și cooperarea, calitatea, incluziunea, creativitatea și inovarea la nivelul organizațiilor sportive și al politicilor din domeniul sportului

# Cadru legal 

* [Erasmus Policy Statement](https://www.upit.ro/_document/112186/erasmus_policy_statement_upit.pdf)
* [Carta Erasmus](https://www.upit.ro/_document/113219/carta_erasmus_limba_romana.pdf)