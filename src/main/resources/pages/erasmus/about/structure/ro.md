Pentru a-și atinge obiectivele, programul Erasmus+ pune în aplicare următoarele acțiuni:

<table-of-contents depth="1" lang="ro"></table-of-contents>

Pentru mai multe detalii, vă rugăm să consultați [Ghidul Programului Erasmus+](https://erasmus-plus.ec.europa.eu/sites/default/files/2021-11/2022-erasmusplus-programme-guide_ro.pdf).

# Acțiunea-Cheie 1 – Mobilitatea persoanelor

Această acțiune-cheie sprijină:

1. Mobilitatea **cursanților** și a **personalului**
2. Activitățile de participare a **tinerilor** (inițiative locale și transnaționale conduse de tineri, pentru a-i ajuta ă se implice și să învețe să participe la viața democratică)
3. Inițiativa **DiscoverEU**: o acțiune care le oferă tinerilor cu vârsta de 18 ani șansa de a avea o experiență pe termen scurt, individuală sau în grup, de călătorie prin Europa
4. Oportunități pentru învățarea **limbilor străine** participanților care desfășoară o activitate de mobilitate în străinătate, prin intermediul platformei de sprijin lingvistic online (OLS) sau prin alte forme suplimentare de sprijin lingvistic
5. Schimburi virtuale în sectorul **învățământului superior** și **al tineretului**: activități interpersonale online care promovează dialogul intercultural și dezvoltarea competențelor non-tehnice, între persoane din țări terțe, din statele membre ale UE sau din țări terțe asociate la program

# Acțiunea-Cheie 2 – Cooperarea între organizații și instituții

Această acțiune-cheie sprijină:

**Parteneriatele pentru cooperare**, inclusiv:

- Parteneriatele de cooperare
- Parteneriatele la scară mică

**Parteneriatele pentru excelență**, inclusiv:

- Universitățile europene
- Centrele de excelență profesională (CEP)
- Academiile Erasmus+ pentru cadrele didactice (Erasmus+ Teacher Academies)
- Acțiunea Erasmus Mundus

**Parteneriatele pentru inovare,** inclusiv:

- Alianțele pentru inovare
- Proiectele orientate spre viitor

**Proiectele de consolidare a capacităților**, inclusiv:

- Proiectele de consolidare a capacităților în domeniul învățământului superior
- Proiectele de consolidare a capacităților în domeniul educației și formării profesionale
- Proiectele de consolidare a capacităților în domeniul tineretului
- Proiectele de consolidare a capacităților în domeniul sportului

**Evenimentele sportive non-profit**

Platformele digitale oferă spații virtuale de colaborare, baze de date pentru găsirea de parteneri, comunități de practică și alte servicii online.
- Platforma eTwinning
- Platforma electronică pentru educația adulților în Europa (EPALE)
- Platforma School Education Gateway (SEG)
- Portalul european pentru tineret

# Acțiunea-Cheie 3 – Sprijin pentru elaborarea de politici și pentru cooperare

Această acțiune-cheie sprijină:

1. Acțiunea „Tineretul european împreună”
2. Acțiuni care vizează pregătirea și sprijinirea punerii în aplicare a agendei de politici a UE în domeniul educației, formării, tineretului și sportului, inclusiv a agendelor sectoriale pentru învățământul superior, pentru educația și formarea profesională, pentru școli și învățământul pentru adulți
3. Realizarea de experimentări în materie de politici europene, conduse de autorități publice de nivel înalt,
4. Acțiuni care vizează colectarea de dovezi și cunoștințe cu privire la sistemele și politicile din domeniile educației, formării, tineretului și sportului la nivel național și european
5. Acțiuni care facilitează transparența și recunoașterea competențelor și calificărilor, precum și transferul de credite
6. Acțiunile care încurajează dialogul în materie de politici cu părțile interesate din Uniunea Europeană și din afara acesteia
7. Cooperarea cu organizațiile internaționale cu expertiză și capacitate analitică recunoscute la scară largă

# Acțiunile Jean Monnet

Acțiunile Jean Monnet vor sprijini:

Acțiunea Jean Monnet în domeniul **învățământului superior**, cu următoarele subacțiuni:

- Modulele Jean Monnet
- Catedrele Jean Monnet
- Centrele de excelență Jean Monnet :

Acțiunea Jean Monnet în alte domenii ale **educației și formării** cu următoarele subacțiuni:

- Învățământul pedagogic: concepe și 19 oferă cadrelor didactice propuneri de formare structurată pe teme legate de UE
- Inițiativa „Cunoașterea UE”

**Dezbaterea de orientare Jean Monnet**:

- Rețelele Jean Monnet în domeniul învățământului superior
- Rețelele pentru alte domenii ale educației și formării, schimbul de bune practici și experiența predării în echipă în cadrul unui grup de țări

**Sprijin pentru instituțiile desemnate**