# Incoming

## Studenți

* Semestrul I: 30 iunie <span class="this-year"></span>
* Semestrul II: 30 noiembrie <span class="this-year"></span>

## Personal didactic și auxiliar

Nominalizările se primesc pe toată durata anului universitar, cu condiția de a fi trimise ce cel puțin **o lună** înainte de începerea perioadei de mobilitate propusă.

# Outgoing

## Studenți

* Semestrul I și practica de vară: selecțiile se desfășoară în luna **aprilie**
* Semestrul II și practica de vară: selecțiile se desfășoară în luna **octombrie**

## Personal didactic și auxiliar

Pentru tot anul universitar: selecțiile se desfășoară în luna **octombrie**.