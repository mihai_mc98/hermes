# Erasmus Student Network History

<abbr title="Erasmus Student Network">ESN</abbr> was founded on the **16th of October 1989** and is the biggest Students' Association in Europe. Its purpose is to **support** și **enhance** students' exchange experience.

# ESN România – Quick Fact-Sheet

1. Foundation Year: **2004**
2. Present in **13** cities
3. We work with over **40** Universities
4. Approx. **400** volunteers
5. We offer support for around **3500** international students each year

# ESN Pitești

ESN Pitești has been department of ESN România since 2020. We collaborate with **The University Centre of Pitești** through the Erasmus+ Office.

We have over **30 volunteers** working in various domains of interest.

To find more information about us, please have a look at our [Presentation Pamphlet](https://www.upit.ro/_document/174081/esn_pites_ti_presentation.pdf).

# Our Mission

- We provide support for the international students of The University Centre of Pitești, following the spirit of the ESN Core Values
- We increase awareness for the importance of intercultural openness and its role in society
- We organise events focused on the **6 ESN Core Values**:
    1. Education and Youth
    2. Culture
    3. Skills and Employability
    4. Social Inclusion
    5. Health and Well-being
    6. Environment Sustainability

# Local Events

ESN Pitești is constantly organising local events, such as:

* Day Trips: Curtea de Argeș – Poenari – Vidraru
* Latin Dance Lessons
* Zoo Scavenger Hunt
* Romanian Language Courses
* Sport Competitions
* Picnics
* _Bad Jokes'_ Night
* Together for Europe
* Erasmus TED Talks

# Contact

<div class="paragraph">

**E-Mail**: [board@pitesti.esn.ro](mailto:board@pitesti.esn.ro)

**Facebook**: [@esn.pitesti](https://www.facebook.com/esn.pitesti/)

**Instagram**: [@esn.pitesti](https://www.instagram.com/esn.pitesti/)

**Website**: [https://pitesti.esn.ro](https://pitesti.esn.ro)

</div>