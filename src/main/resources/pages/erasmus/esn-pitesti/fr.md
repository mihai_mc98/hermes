# Historique du Erasmus Student Network

<abbr title="Erasmus Student Network">ESN</abbr> a été fondée en **16 octobre 1989** et elle est la plus grande association des étudiants de l’Europe. Le but d’ESN est de **soutenir** și **augmenter** les échanges d’expérience des étudiants.

# ESN Roumanie en chiffres

1. Année de fondation : **2004**
2. Présence dans **13** villes
3. Nous collaborons avec plus de **40** Universités
4. Environ **400** volontaires
5. Nous soutenons approx. **3500** étudiants internationaux chaque année

# ESN Pitești

ESN Pitești est un département d’ESN Roumania d’année 2020 et collabore avec **le Centre Universitaire de Pitești** par le Bureau Erasmus+.

Nous avons plus de **30 volontaires** qui travaillent dans des domaines d’intérêt différents.

Pour trouver plus d’informations sur nous, veuillez accéder notre [Brochure](https://www.upit.ro/_document/174081/esn_pites_ti_presentation.pdf). 

# Notre mission

- Nous aidons les étudiants internationaux du Centre Universitaire de Pitești dans l’esprit des Valeurs ESN
- Nous augmentons la conscience sur l’importance de l’ouverture interculturelle et son rôle dans la société
- Nous organisions des événements concentrés sur les **6 Valeurs ESN **:
    1. Education et jeunesse
    2. Culture
    3. Compétences et employabilité
    4. Inclusion sociale
    5. Santé et bien-être
    6. Durabilité environnementale

# Événements locaux

ESN Pitești organise constamment des événements au niveau local, y compris :

* Des excursions: Curtea de Argeș – Poenari – Vidraru
* Des danses latines
* Chasse au trésor au Zoo
* Des cours de langue Roumaine
* Des compétitions sportives
* Pique-niques
* Soirée des _Blagues_
* Together for Europe
* Erasmus TED Talks

# Contact

<div class="paragraph">

**E-Mail**: [board@pitesti.esn.ro](mailto:board@pitesti.esn.ro)

**Facebook**: [@esn.pitesti](https://www.facebook.com/esn.pitesti/)

**Instagram**: [@esn.pitesti](https://www.instagram.com/esn.pitesti/)

**Website**: [https://pitesti.esn.ro](https://pitesti.esn.ro)

</div>