# Istoricul Erasmus Student Network

<abbr title="Erasmus Student Network">ESN</abbr> este cea mai mare asociație studențească din Europa, înființată la **16 octombrie 1989**. Scopul ESN este de a **susține** și **îmbunătăți** schimburile de experiență ale studenților.

# ESN România în cifre

1. An de înființare: **2004**
2. Prezența în **13** orașe
3. Colaborează cu peste **40** de universități
4. Aproximativ **400** de voluntari
5. Oferă ajutor pentru aproximativ **3500** de studenți internaționali în fiecare an

# ESN Pitești

ESN Pitești este secțiune a ESN România din anul 2020 și colaborează cu **Centrul Universitar Pitești** prin intermediul Biroului Erasmus+.

Avem peste **30 de voluntari** ai Centrului Universitar Pitești care lucrează în diferite departamente și domenii de interes.

Pentru a afla mai multe despre noi, vă rugăm să accesați [Broșura noastră](https://www.upit.ro/_document/174081/esn_pites_ti_presentation.pdf).

# Misiunea noastră

- Ajutăm studenții internaționali din cadrul Centrului Universitar din Pitești în spiritul valorilor promovate de ESN
- Creștem gradul de conștientizare cu privire la importanța deschiderii interculturale și al rolului acesteia în societate
- Organizăm evenimente care se încadrează în cele **6 cauze ESN**:
  1. Educație și tineret
  2. Cultură
  3. Abilități și angajabilitate
  4. Incluziune socială
  5. Sănătate și bunăstare
  6. Sustenabilitate mediului
 
# Evenimente locale

ESN Pitești organizează constant evenimente la nivel local, printre care se numără:

* Excursii: Curtea de Argeș – Poenari – Vidraru
* Dansuri Latino
* Vânătoare de comori la Zoo
* Cursuri de limba română
* Competiții sportive
* Picnicuri
* Seara _Bancurilor Seci_
* Together for Europe
* Erasmus TED Talks

# Contact

<div class="paragraph">

**E-Mail**: [board@pitesti.esn.ro](mailto:board@pitesti.esn.ro)

**Facebook**: [@esn.pitesti](https://www.facebook.com/esn.pitesti/)

**Instagram**: [@esn.pitesti](https://www.instagram.com/esn.pitesti/)

**Website**: [https://pitesti.esn.ro](https://pitesti.esn.ro)

</div>