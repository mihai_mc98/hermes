
| Tip de date          | Date instituționale                                 |
|----------------------|-----------------------------------------------------|
| Numele universității | Centrul Universitar Pitești                         |
| Adresă               | Str. Târgu din Vale, nr. 1, Pitești, Argeș, România |
| Cod Erasmus          | RO BUCURES43                                        |
| Număr PIC            | 880 988 630                                         |
| Cod OID              | E 1033 9364                                         |
| Cod ECHE             | 1010-Tran-880988630                                 |
 