# Mobilități

1. [Pentru studenți](/erasmus/mobilities/outgoing/students)
2. [Pentru personalul didactic și auxiliar](/erasmus/mobilities/outgoing/staff)

# Contact

Pentru a afla noutăți despre selecțiile pentru mobilități, ne puteți urmări pe:

<div class="paragraph">

**Facebook**: [Erasmus+](https://www.facebook.com/groups/595500923990705/), [@InternationalRelations.CUPIT](https://www.facebook.com/InternationalRelations.CUPIT) și [@official.cupit](https://www.facebook.com/official.cupit)

**Instagram**: [@official.cupit](https://www.instagram.com/official.cupit)
</div>