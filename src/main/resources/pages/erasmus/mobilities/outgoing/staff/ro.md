Cadrele didactice pot beneficia de mobilități de **predare** sau **formare**.

# Tipuri de mobilități

Există două tipuri de mobilități destinate personalului didactic și auxiliar:
1. de predare
2. de formare

## Mobilitatea de predare

Acest tip de mobilitate permite cadrelor didactice universitare sau personalului din întreprinderi **să predea** la o universitate parteneră din străinătate, în orice domeniu/disciplină academică.

## Mobilitatea de formare

Acest tip de mobilitate sprijină dezvoltarea personală a **personalului didactic și nedidactic** din universități prin:
- **evenimente de formare** în străinătate (cu excepția conferințelor)
- **observarea** directă la locul de muncă/**formare** într-o instituție de învățământ superior parteneră sau într-o altă organizație relevantă din străinătate

# Când pot candida?

Concursul de selecție se va desfășura după un calendar stabilit de comisia de selecție din fiecare facultate.

# Dosarul de mobilitate

Vă rugăm să luați legătura cu [Coordonatorul Erasmus+ din facultate](/erasmus/mobilities#Coordonatori) pentru a vă oferi sprijin în realizarea dosarului de mobilitate:

1. Cerere de deplasare
2. Acord de predare/formare
3. Copie după Cartea de Identitate
4. Detalii personale: număr de telefon, adresă de e-mail, grad didactic, vechime în cadrul Centrului Universitar Pitești
5. Extras de cont în **EUR**

# După mobilitate

La finalul mobilității, fiecare participant va întocmi un **raport de activitate**, care va fi depus la dosarul personal de la Biroul Erasmus+