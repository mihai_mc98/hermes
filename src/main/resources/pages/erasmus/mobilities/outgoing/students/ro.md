În luna octombrie, Facultățile Centrului Universitar Pitești organizează concursul de selecție a mobilităților studențești Erasmus+ (studiu sau practică) pentru anul universitar 2022-2023. Calendarele de concurs pot varia de la facultate la facultate. Vă recomandăm să luați legătura cu Facultatea dumneavoastră sau cu [Coordonatorului Erasmus+ din facultate](/erasmus/mobilities#Coordonatori) pentru mai multe detalii.

<table-of-contents depth="1" lang="ro"></table-of-contents>

# Tipuri de mobilități

Studenții pot beneficia de mobilități:
1. de **studiu**
2. de **plasament**

# Mobilitatea de studiu

* **Scop**: Studierea la o universitate parteneră din **străinătate**
* **Durata**: 3 – 12 luni
* **Perioadă de desfășurare**: în oricare semestru, conform calendarului academic al universităţii gazdă

<span class="note-important">Important:</span> Studenții în an terminal **NU** pot efectua mobilitatea în **semestrul II**, deoarece acesta este dedicat pregătirii lucrării de diplomă !

## Criterii de eligibilitate

1. Candidatul trebuie să fie înmatriculat la Centrul Universitar Pitești
2. Cumulul mobilităților Erasmus+ nu trebuie să depășească 12 luni per ciclu de studiu (i.e. Licență, Masterat, Doctorat)
3. Candidatul trebuie să fie în:
    * anul II la Licență
    * anul I la Masterat/Doctorat

## Grantul Erasmus+

Acoperă **parțial** costurile de transport și de trai, variind după țara de destinație: **€470 – €520/lună**

# Mobilitatea de plasament

* **Scop**: Adaptarea la cerințele pieței muncii și dobândirea de **experiență profesională** în cadrul unei **întreprinderi** sau **organizații** partenere
* **Durată**: 2 – 12 luni
* **Perioadă de desfășurare**: pe toată durata anului academic, inclusiv în timpul verii

## Criterii de eligibilitate

1. trebuie să fiți înmatriculat la Centrul Universitar Pitești
2. cumulul mobilităților Erasmus+ nu trebuie să depășească 12 luni per ciclu de studiu (i.e. Licență, Masterat, Doctorat)
3. Candidatul trebuie să fie în:
    * anul I la Licență
    * primul an după absolvire

## Grantul Erasmus+

Acoperă **parțial** costurile de transport și de trai, variind după țara de destinație: **€670 - €720/lună**

# Locuri disponibile

Proiectul: KA 103 (studiu și plasament)

* [Facultatea de Electronică, Comunicații și Calculatoare (FECC)](https://www.upit.ro/_document/235227/2locuri_disponibile_studenti_ka103_fecc.pdf)
* [Facultatea de Mecanică și Tehnologie (FMT)](https://www.upit.ro/_document/235226/2locuri_disponibile_studenti_ka103_fmt.pdf)
* [Facultatea de Științe Economice și Drept (FȘED)](https://www.upit.ro/_document/235229/2locuri_disponibile_studenti_ka103_fsed.pdf)
* [Facultatea de Științe, Educație Fizică și Informatică (FȘEFI)](https://www.upit.ro/_document/235225/2locuri_disponibile_studenti_ka103_fsefi.pdf)
* [Facultatea de Științe ale Educației, Științe Sociale și Psihologie (FȘEȘSP)](https://www.upit.ro/_document/235228/2locuri_disponibile_studenti_ka103_fsedssp.pdf)
* [Facultatea de Teologie, Litere, Istorie și Arte (FTLIA)](https://www.upit.ro/_document/235230/2locuri_disponibile_studenti_ka103_ftlia.pdf)

# Taxe și sprijin financiar

<span class="note-important">Important:</span> În calitate de student Erasmus+, **NU** plătiți la universitatea gazdă taxe de:
* școlarizare
* înscriere
* examinare
* acces la laboratoare
* acces în bibliotecă

## Sprijin financiar suplimentar

Centrul Universitar Pitești acordă un ajutor financiar **suplimentar** în valoare de **€200/lună** studenților selectați pentru stagiile Erasmus+ care sunt eligibili pentru **burse sociale**.

# Aplicați pentru o mobilitate Erasmus+

1. Consultați lista cu [locurile disponibile](#Locuri-disponibile)
2. Alegeți max. 3 opțiuni
3. Transmiteți dosarul [Coordonatorului Erasmus+ din facultate](/erasmus/mobilities#Coordonatori)

## Criteriile de selecție

1. Rezultate academice **bune** în sesiunile de examen anterioare
2. Scrisoarea de intenție pentru motivarea mobilității

## Dosarul de mobilitate

Vă rugăm să luați legătura cu [Coordonatorul Erasmus+ din facultate](/erasmus/mobilities#Coordonatori) pentru a vă oferi sprijin în realizarea dosarului de mobilitate:

### Documente inițiale

Pentru a demara procesul de selecție trebuie să trimiteți dosarul inițial coordonatorului Erasmus+:

1. [Cererea](https://www.upit.ro/_document/235231/model_cerere_2022_2.docx) în care se vor exprima max. 3 opțiuni
2. [CV actualizat](https://www.upit.ro/_document/37085/model_cv.docx) în format Europass (cu semnătura candidatului)
3. [Scrisoare de intenție](https://www.upit.ro/_document/235232/model_scrisoare_intentie_final_2.docx) în care trebuie precizate explicit:
   1. Obiectivele participării la un stagiu Erasmus+
   2. Motivul pentru alegerea Universității/Facultății/Firmei partenere
   3. Țeluri în cariera personală
   4. Preferința pentru o anumită țară din străinătate
4. Copie după cartea de identitate
5. [Copertă dosar](https://www.upit.ro/_document/37083/model_coperta.docx)

### Completare dosar

Ulterior, dosarul inițial trebuie completat cu:

1. Extrasul din foaia matricolă (Licență/Masterat/Doctorat) – eliberat de secretariatul facultății
2. Adeverință de student (înmatriculat) al Centrului Universitar Pitești
3. Copie după ultima diplomă obținută (Bacalaureat/Licență/Masterat)
4. Copie foaie matricolă de la ultimele studii absolvite (Liceu/Licență/Masterat)
5. Certificat de competență lingvistică, eliberat de un organism autorizat, pentru a atesta cunoașterea limbilor străine prevazute în Acordul bilateral Erasmus+
6. Declarație pe propria răspundere autentificată la notar care să menționeze că studentul nu depășește cumulul de **12 luni** de mobilități Erasmus+ per ciclu de studiu (Licență/Masterat/Doctorat), indiferent de tipul mobilității, forma de învățământ sau instituțiile la care ar mai fost înmatriculat
7. Alte documente necesare facultăților pentru obținerea mobilității

<span class="note">Notă:</span> Certificat de competență lingvistică **nu** este necesar candidaților care studiază respectiva limba străină ca specializare.

### Alte documente

1. Cardul European de Sănătate (pentru studiu în țări UE) sau Asigurare Civilă (pentru plasament/studiu în țări non-UE)
2. Formularul de aplicație al universității gazdă – disponibil pe website-ul acesteia (unde este cazul)
3. Acordul de studiu/plasament
4. Extras de cont în **EUR**

# Documente utile

## Infografice

* [Programul de mobilități Erasmus+](https://www.upit.ro/_document/235381/programul_de_mobilita_t_i_erasmus_octombrie_2022.pdf)

## Dosarul de mobilitate

* [Model cerere dosar](https://www.upit.ro/_document/235231/model_cerere_2022_2.docx)
* [Model CV](https://www.upit.ro/_document/37085/model_cv.docx) format Europass
* [Model Scrisoare de intenție](https://www.upit.ro/_document/235232/model_scrisoare_intentie_final_2.docx)
* [Copertă dosar](https://www.upit.ro/_document/37083/model_coperta.docx)