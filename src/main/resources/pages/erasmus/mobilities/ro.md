Următoarele pagini din secțiune sunt disponibile:

1. [Mobilități Outgoing](/erasmus/mobilities/outgoing)

# Coordonatori

<table>
    <thead>
        <th>Facultate (A – Z)</th>
        <th>Specializări (A – Z)</th>
        <th>Nume Coordonator</th>
        <th>Adresă de e‑mail</th>
    </thead>
    <tbody>
        <tr>
            <td><abbr title="Facultatea de Calculatoare, Comunicații și Electronică">FECC</abbr></td>
            <td>Toate</td>
            <td>Valeriu IONESCU</td>
            <td><a href="mailto:valeriu.ionescu@upit.ro">valeriu.ionescu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Mecanică și Tehnologie">FMT</abbr></td>
            <td>Toate</td>
            <td>Andrei BOROIU</td>
            <td><a href="mailto:andrei.boroiu@upit.ro">andrei.boroiu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Drept și Științe Economice">FSED</abbr></td>
            <td>Drept</td>
            <td>Andra PURAN</td>
            <td><a href="mailto:andra.puran@upit.ro">andra.puran@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Drept și Științe Economice">FSED</abbr></td>
            <td>Științe economice</td>
            <td>Alina HAGIU</td>
            <td><a href="mailto:alina.hagiu@upit.ro">alina.hagiu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Educație fizică, Informatică și Științe">FSEFI</abbr></td>
            <td>Educație fizică și sport</td>
            <td>Ionela NICULESCU</td>
            <td><a href="mailto:ionela.niculescu@upit.ro">ionela.niculescu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Educație fizică, Informatică și Științe">FSEFI</abbr></td>
            <td>Matematică‑Informatică</td>
            <td>Raluca Mihaela GEORGESCU</td>
            <td><a href="mailto:raluca.georgescu@upit.ro">raluca.georgescu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Educație fizică, Informatică și Științe">FSEFI</abbr></td>
            <td>Științe</td>
            <td>Cristian POPESCU</td>
            <td><a href="mailto:cristian.popescu@upit.ro">cristian.popescu@upit.ro</a></td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Psihologie, Științe ale Educației și Științe Sociale">FSESSP</abbr></td>
            <td>Toate</td>
            <td>
                Luiza SÂRBU
                <br>
                Cristina DUMITRU
            </td>
            <td>
                <a href="mailto:luiza.sarbu@upit.ro">luiza.sarbu@upit.ro</a>
                <br>
                <a href="mailto:cristina.dumitru@upit.ro">cristina.dumitru@upit.ro</a>
            </td>
        </tr>
        <tr>
            <td><abbr title="Facultatea de Arte, Istorie, Litere și Teologie">FTLIA</abbr></td>
            <td>Toate</td>
            <td>Cristina ARSENE‑ONU</td>
            <td><a href="mailto:cristina.onu@upit.ro">cristina.onu@upit.ro</a></td>
        </tr>
    </tbody>
</table>
