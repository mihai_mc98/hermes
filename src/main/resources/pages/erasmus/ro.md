<div class="info-box">

## Mobilități Studenți

Selecția studenților pentru mobilitățile Erasmus+ se va desfășura la final de **octombrie 2022**. Pentru mai multe detalii, vă rugăm să vizitați [Mobilități Outgoing (Studenți)](/erasmus/mobilities/outgoing/students) și să luați legătura cu [Coordonatorii Erasmus+](/erasmus/mobilities#Coordonatori). 

</div>

<div class="cards">
    <div class="card">
        <img src="/images/Cards/Spreadsheets around a Table.jpg" alt="Spreadsheets around a Table">
        <div class="card-content">
            <a href="/erasmus/about">Despre Programul Erasmus+</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/EU Flag 2.jpg" alt="EU Flag">
        <div class="card-content">
            <a href="/erasmus/mobilities/outgoing/students">Mobilități Outgoing (Studenți)</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Calendar.jpg" alt="Calendar">
        <div class="card-content">
            <a href="/erasmus/deadlines">Date limită</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Magnifying Glass.jpg" alt="Magnifying Glass">
        <div class="card-content">
            <a href="/erasmus/institutional-information">Date instituționale Erasmus+</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Student holding Books.jpg" alt="Student holding Book">
        <div class="card-content">
            <a href="/erasmus/mobilities">Mobilități Erasmus+</a>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/ESN Pitești.jpg" alt="Erasmus Student Network Pitești">
        <div class="card-content">
            <a href="/erasmus/esn-pitesti">ESN Pitești</a>
        </div>
    </div>
</div>

# Resurse utile

* [Ghidul studentului Erasmus (EN)](https://www.upit.ro/_document/175052/erasmus_guide.pdf)

# Contact

## Coordonator Biroul Erasmus

<div class="paragraph">

Dna. Maria MIROIU (Acorduri bilaterale)

E-mail: [maria.miroiu@upit.ro](mailto:maria.miroiu@upit.ro) sau [maria.miroiu@gmail.com](mailto:maria.miroiu@gmail.com)

Nr. Tel.: [+40 348 453 333](tel:+40348453333)

Adresă: Camera I33, Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România

</div>

## Referent Erasmus

<div class="paragraph">

E-mail: [erasmus@upit.ro](mailto:erasmus@upit.ro)

Nr. Tel.: [+40 348 453 333](tel:+40348453333)

Adresă: Camera I33, Str. Târgu din Vale, nr. 1, 110040, Pitești, Argeș, România

</div>