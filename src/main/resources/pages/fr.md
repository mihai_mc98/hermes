Soyez les bienvenus au Centre des Relations internationales du Centre Universitaire de Pitești !

<div class="cards">
    <div class="card">
        <img src="/images/Cards/EU Flag.jpg" alt="The flag of the European Union">
        <div class="card-content">
            <a href="/erasmus">Erasmus+</a>
            <p>Découvrez nos programmes Erasmus+ et ce qu’on doit faire : de la postulation à l’arrivée</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Students Playing Instruments.jpg" alt="Students Playing Instruments">
        <div class="card-content">
            <a href="/admissions">Admissions</a>
            <p>Apprenez plus sur le processus d’admission des étudiants étrangers au Centre Universitaire de Pitești</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Women in the Traditional Romanian Blouse.jpg" alt="Women in the Traditional Romanian Blouse">
        <div class="card-content">
            <a class="inactive" href="/about-us">Pourquoi UPIT ?</a>
            <p>Découvrez les avantages de devenir étudiant au Centre Universitaire de Pitești</p>
        </div>
    </div>
</div>