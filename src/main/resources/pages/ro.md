Bine ați venit la Centrul pentru Relații Internaționale al Centrului Universitar Pitești!

<div class="cards">
    <div class="card">
        <img src="/images/Cards/EU Flag.jpg" alt="The flag of the European Union">
        <div class="card-content">
            <a href="/erasmus">Erasmus+</a>
            <p>Descoperiți programele noastre Erasmus+ și ceea ce trebuie să faceți: din momentul aplicării, până la sosire</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Students Playing Instruments.jpg" alt="Students Playing Instruments">
        <div class="card-content">
            <a href="/admissions">Admitere</a>
            <p>Aflați mai multe despre procesul de admitere pentru studenții internaționali la Centrul Universitar Pitești</p>
        </div>
    </div>
    <div class="card">
        <img src="/images/Cards/Women in the Traditional Romanian Blouse.jpg" alt="Women in the Traditional Romanian Blouse">
        <div class="card-content">
            <a href="/about-us">De ce CUPIT ?</a>
            <p>Descoperiți care sunt avantajele în a deveni student al Centrului Universitar Pitești</p>
        </div>
    </div>
</div>