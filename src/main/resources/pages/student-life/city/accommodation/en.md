After arrival, students may choose to book a hotel for the first couple of days in Pitești. Alternatively, you may arrange your own private accommodation or stay at our own Halls of Accommodation.
* [Victoria Hotel](http://victoriahotel.ro) – 4 Stars
* [Hotel Muntenia](http://www.hoteluripitesti.ro/ro/hotel-muntenia.php) – 4 Stars
* [Yaky Hotel](https://www.facebook.com/yakycenter.ro/) – 4 Stars
* [Hotel Argeș](http://www.hotelarges.ro) – 3 Stars
* [Casa Teilor](https://casateilor.ro/) – 3 Stars
* [Celly Hotel](https://www.hotelcelly.ro/) – 3 Stars
* [Imibiliare.ro](https://www.imobiliare.ro/agentii/pitesti) for renting private apartments
