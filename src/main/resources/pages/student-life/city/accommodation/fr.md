Après leur arrivée, les étudiants peuvent faire une réservation à un hôtel pour les premiers jours à Pitești. Ou bien, vous pouvez organiser votre propre hébergement privé ou séjourner dans nos propres résidences d’hébergement.
* [Victoria Hotel](http://victoriahotel.ro) – 4 étoiles
* [Hotel Muntenia](http://www.hoteluripitesti.ro/ro/hotel-muntenia.php) – 4 étoiles
* [Yaky Hotel](https://www.facebook.com/yakycenter.ro/) – 4 étoiles
* [Hotel Argeș](http://www.hotelarges.ro) – 3 étoiles
* [Casa Teilor](https://casateilor.ro/) – 3 étoiles
* [Celly Hotel](https://www.hotelcelly.ro/) – 3 étoiles
* [Imobiliare.ro](https://www.imobiliare.ro/agentii/pitesti) pour louer des appartements privés
