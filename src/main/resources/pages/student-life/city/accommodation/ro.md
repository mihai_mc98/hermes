După sosire, studenții pot alege să își facă o rezervare la hotel pentru primele zile în Pitești. De asemenea, puteți să vă aranjați o cazare privată sau să vă cazați în propriile noastre Centre de cazare.

* [Hotel Victoria](http://victoriahotel.ro) – 4 stele
* [Hotel Muntenia](http://www.hoteluripitesti.ro/ro/hotel-muntenia.php) – 4 stele
* [Hotel Yaky ](https://www.facebook.com/yakycenter.ro/) – 4 stele
* [Hotel Argeș](http://www.hotelarges.ro) – 3 stele
* [Casa Teilor](https://casateilor.ro/) – 3 stele
* [Hotel Celly](https://www.hotelcelly.ro/) – 3 stele
* [Imobiliare.ro](https://www.imobiliare.ro/agentii/pitesti) pentru a închiria apartamente private
