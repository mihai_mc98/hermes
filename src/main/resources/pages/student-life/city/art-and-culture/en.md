If you are a culture addict, Pitești has you covered. From Museum to Theatres, nothing is stopping you from enjoying a day-off in our beautiful city. As a bonus, you will also be exercising your Romanian skills, so all the more reason to hop onto a bus and visit what the City Centre has to offer!

<table-of-contents depth="1" lang="en"></table-of-contents>

# Museum of the County of Argeș

<img src="/images/stock/County Museum.jpg" alt="County Museum">

Formally opened 65 years ago, the [Museum](http://www.muzeul-judetean-arges.ro/) is located close to the City Centre, in the former Prefecture of Argeș built in 1898-1899. With numerous collections from various fields, students can discover the Romanian history. The Museum houses treasures dating back from the Roman era, paintings from the XIX and XX centuries, local graphics and a collection of books consisting of 18,000 volumes.

[The Art Gallery](http://www.muzeul-judetean-arges.ro/sectiile-muzeului/galeria-de-arta) has been operating in a XIX<sup>th</sup> century building. It holds 1100+ works of classical and contemporary painting and sculpture.

**The National Gallery of Naïve Art** is the first of its kind in the country in Romania. For more than 30 years, our city has been considered a genuine capital for Romanian naïve art, showcasing paintings, sculptures and graphics.

# "Alexandru Davila" Theatre

<img src="/images/stock/Theatre.jpg" alt="'Alexandru Davila' Theatre">

The Theatre is the only repertory theatre in Central-Southern Romania (Counties of Argeș, Telorman and Olt). With three performance halls, you will find both Drama and Children's plays. In fact, the Theatre has dedicated an entire section for the Youth: The "Așchiuță" Theatre, also known as the "Puppet Show Theatre".

To plan your visit or see the Schedule, visit the Theatre's [website](http://www.teatruldavila.ro/program-spectacole). Ticket prices vary, but students should expect the average ticket to be around 25 RON (€5).

# The Musical Fountain

<img src="/images/stock/Musical Fountain.jpg" alt="Musical Fountain">

Next to the "Alexandru Davila" Theatre, lies [Pitești's Musical Fountain](http://www.informatii-romania.ro/listing/fantana-muzicala/). Inaugurated in 2008 for Europe's Day, it is one-of-a-kind in Eastern Europe, with its 1,000 fixed and mobile water fountains and colourful lights. The streams synchronise to the music, making it a joyful sight in the evenings.

# "Dinicu Golescu" County Library

<img src="/images/stock/County Library.jpg" alt="'Dinicu Golescu' County Library">

For our favourite bookworms, there is the [County Library](https://www.bjarges.ro/). Students will find an impressive collection of important Romanian and foreign works. Amongst the Library most-sought items are reference materials, newspapers, periodicals and audiovisual resources (cassettes and CDs). With a total area of 4,900 m<sup>2</sup>, the new headquarters aims at offering the public wide access to information.

The staff are very kind, so don't hesitate to ask for help if you get lost!

## Europe Direct Centre

The Library also houses the [Europe Direct Centre for the County of Argeș](http://www.edarges.ro/www/). If you are interested in finding out more about how the European Union works and its projects, be sure to pay the Centre a visit. Infographics taking subjects from Agriculture to Finance are available in Romanian, English, French and German.

# Pitești Philharmonic – The "First Violin" of Argeș' Culture

<img src="/images/stock/Philharmonic.jpg" alt="Pitești Philharmonic">

Tailored for our beloved music fans, the Pitești Philharmonic is the go-to place to enjoy a jazz night or a classical symphony played by local orchestra. Named "the first violin of Argeș Culture" by our local newspaper, the Philharmonic has been a real success.

Book your tickets via the Philharmonic's [website](https://filarmonica-pitesti.ro/bilete/). Prices usually range from 5 RON to 50 RON (€1 – €10).

# The Princely Church of Pitești – The "Sfântul Gheorghe" Church

<img src="/images/stock/Princely Church.jpg" alt="'Sfântul Gheorghe' Church">

The [Church](http://www.bisericadomneascapitesti.ro/) was founded by Constantin Șerban, the then-ruler of Wallachia in 1656. Its unique architecture reflects the refinement of the Ecclesiastical Construction Art in Wallachia, in the middle of the XVII<sup>th</sup> century. Located in the "Valise Milea" Square, in the City Centre, it has become one of the most recognisable landmarks of Pitești.
