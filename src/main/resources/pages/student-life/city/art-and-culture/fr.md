Si vous aimez la culture, Pitești vous offre tout ce que vous avez besoin. Des musées aux théâtres, rien ne vous empêche de profiter d’une journée de congé dans notre belle ville. Comme bonus, vous pratiquerez vos connaissances de langue roumaine, vous avez donc une raison de plus de monter dans le bus et de visiter ce que le centre-ville a à offrir !

<table-of-contents depth="1" lang="fr"></table-of-contents>

# Musée départemental d’Argeș

<img src="/images/stock/County Museum.jpg" alt="County Museum">

Officiellement ouvert il y a 65 ans, [le Musée](http://www.muzeul-judetean-arges.ro/) est situé près du centre-ville, dans l’ancienne Préfecture d’Argeș construite entre 1898-1899. Grâce à de nombreuses collections dans divers domaines, les étudiants peuvent découvrir l’histoire de la Roumanie. Le musée abrite des trésors de l’époque romaine, des peintures des XIXe et XXe siècles, des dessins graphiques locaux et une collection de livres de 18.000 volumes.  

[Galerie d’Art](http://www.muzeul-judetean-arges.ro/sectiile-muzeului/galeria-de-arta) a fonctionné dans un bâtiment du XIX<sup>e</sup> siècle. Elle a plus de 1100 œuvres de peinture et de sculpture classiques et contemporaines.

**Galerie Nationale d’Art Naïf** est la première de ce genre en Roumanie. Depuis plus de 30 ans, notre ville est considérée comme une véritable capitale de l’art naïf roumain, exposant des peintures, des sculptures et des dessins graphiques.

# Théâtre "Alexandru Davila"

<img src="/images/stock/Theatre.jpg" alt="’Alexandru Davila’ Theatre">

Le théâtre est la seule institution dans la région centre-sud de la Roumanie (départements d’Argeș, de Teleorman et d’Olt). Doté de trois salles de spectacle, le théâtre met en scène des pièces de théâtre et des spectacles pour les enfants. En fait, le théâtre a consacré une section entière à la jeunesse : le Théâtre « Aşchiuță », également connu sous le nom de « Théâtre de marionnettes ». 

Pour organiser une visite ou voir le programme, visitez [website du Théâtre](http://www.teatruldavila.ro/program-spectacole). Les prix des billets varient, mais le prix moyen d’un billet étudiant est d’environ 25 RON (5 EUR).

# Fontaine musicale

<img src="/images/stock/Musical Fountain.jpg" alt="Musical Fountain">

Près du Théâtre "Alexandru Davila", il y a la [Fontaine Musicale de Pitești](http://www.informatii-romania.ro/listing/fantana-muzicala/). Inaugurée en 2008, à l’occasion de la Journée de l’Europe, elle est unique en Europe de l’Est, avec 1 000 jets d’eau fixes et mobiles et des lumières colorées. Les jets d’eau se synchronisent avec la musique, ce qui fait un spectacle merveilleux en soirée.

# Bibliothèque Départementale "Dinicu Golescu" 

<img src="/images/stock/County Library.jpg" alt="’Dinicu Golescu’ County Library">

Les amateurs de livres peuvent nourrir leur passion à la [Bibliothèque Départementale](https://www.bjarges.ro/). Les étudiants y trouveront une collection impressionnante d’œuvres roumaines et étrangères importantes. Parmi les articles les plus recherchés de la Bibliothèque figurent les documents de référence, les journaux, les périodiques et les ressources audiovisuelles (cassettes et CD). Ayant une superficie totale de 4.900 m<sup>2</sup>, les nouveaux locaux visent à permettre l’accès à l’information pour le grand public.

Le personnel est très gentil, alors, n’hésitez pas à demander de l’aide si vous ne retrouvez plus votre chemin !

## Centre Europe Direct

 La bibliothèque héberge également le [Centre Europe Direct pour le département d’Argeș](http://www.edarges.ro/www/). Si vous souhaitez en savoir plus sur le fonctionnement de l’Union européenne et ses projets, n’oubliez pas de visiter le Centre. Vous avez à votre disposition des infographies en roumain, anglais, français et allemand qui traitent des sujets tels l’agriculture, les finances, etc.

# Philharmonie de Pitești - le « premier violon » de la culture d’Argeș

<img src="/images/stock/Philharmonic.jpg" alt="Pitești Philharmonic">

Conçue pour répondre aux besoins de nos chers mélomanes, la Philharmonie de Pitești est l’endroit idéal pour profiter d’une soirée de jazz ou d’une symphonie classique interprétée par un orchestre local. Surnommé par notre journal local « le premier violon de la culture d’Argeș », la Philharmonie a un véritable succès.

Réservez vos billets via le [site de la Philharmonie](https://filarmonica-pitesti.ro/bilete/). Les prix varient généralement entre 5 RON et 50 RON (1 - 10 euros).

# L’Église royale de Pitești - L’église "Sfântu Gheorghe"

<img src="/images/stock/Princely Church.jpg" alt="’Sfântul Gheorghe’ Church">

[L’Église](http://www.bisericadomneascapitesti.ro/) a été fondée par Constantin Șerban, souverain de Valachie, en 1656. Son architecture unique reflète le raffinement de l’art des constructions ecclésiastiques en Valachie, au milieu du XVII<sup>e</sup> siècle. Située sur la Place « Vasile Milea », dans le centre de la ville, elle est devenue l’un des monuments les plus reconnaissables de Pitești.
