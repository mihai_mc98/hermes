Dacă sunteți iubitori de cultură, Municipiul Pitești vă oferă tot ce vă trebuie. De la muzee la teatre, nimic nu te oprește să te bucuri de o zi liberă în frumosul nostru oraș. Ca bonus, îți vei exersa și cunoștințele de limba română, așa că ai un motiv în plus să te urci în autobuz și să vizitezi ce are de oferit centrul orașului!

<table-of-contents depth="1" lang="ro"></table-of-contents>

# Muzeul Județean Argeș

<img src="/images/stock/County Museum.jpg" alt="County Museum">

Deschis oficial în urmă cu 65 de ani, [Muzeul](http://www.muzeul-judetean-arges.ro/) este situat în apropierea centrului orașului, în fosta Prefectură a Argeșului construită între anii 1898-1899. Prin intermediul numeroaselor colecții din diverse domenii, studenții pot descoperi istoria României. Muzeul adăpostește comori din epoca romană, picturi din secolele XIX și XX, grafică locală și o colecție de carte formată din 18.000 de volume.

[Galeria de Artă](http://www.muzeul-judetean-arges.ro/sectiile-muzeului/galeria-de-arta) a funcționat într-o clădire din secolul al XIX<sup>lea</sup>. Acesta deține peste 1100 de lucrări de pictură și sculptură clasică și contemporană.

**Galeria Națională de Artă Naivă** este prima de acest gen din România. De mai bine de 30 de ani, orașul nostru este considerat o adevărată capitală a artei naive românești, expunând picturi, sculpturi și grafică.

# Teatrul "Alexandru Davila"

<img src="/images/stock/Theatre.jpg" alt="'Alexandru Davila' Theatre">

Teatrul este singura instituție de profil din zona central-sudică a României (județele Argeș, Teleorman și Olt). Dispunând de trei săli de spectacol, teatrul pune în scenă atât piese de teatru, cât și spectacole pentru copii. De altfel, teatrul a consacrat o întreagă secție pentru tineret: Teatrul "Așchiuță" , cunoscut și sub numele de "Teatrul de Păpuși".

Pentru a stabili o vizită sau pentru a vedea programul, vizitați [site-ul web al Teatrului](http://www.teatruldavila.ro/program-spectacole). Prețurile biletelor variază, dar prețul mediu al unui bilet pentru studenți este de aproximativ 25 RON (5 EUR).

# Fântâna muzicală

<img src="/images/stock/Musical Fountain.jpg" alt="Musical Fountain">

Lângă Teatrul "Alexandru Davila", se află [Fântâna Muzicală din Pitești](http://www.informatii-romania.ro/listing/fantana-muzicala/). Inaugurată în 2008, cu ocazia Zilei Europei, este unică în Europa de Est, având 1.000 de jeturi de apă fixe și mobile și lumini colorate. Jeturile de apă se sincronizează cu muzica, ceea ce face ca seara să fie o priveliște încântătoare.

# Biblioteca Județeană "Dinicu Golescu"

<img src="/images/stock/County Library.jpg" alt="'Dinicu Golescu' County Library">

Iubitorii de cărți își pot hrăni pasiunea la [Biblioteca Județeană](https://www.bjarges.ro/). Studenții vor găsi o colecție impresionantă de lucrări importante românești și străine. Printre cele mai căutate articole ale Bibliotecii se numără materialele de referință, ziarele, periodicele și resursele audiovizuale (casete și CD-uri). Cu o suprafață totală de 4.900 m<sup>2</sup>, noul sediu își propune să ofere publicului acces larg la informație.

Personalul este foarte amabil, așa că nu ezitați să cereți ajutor dacă vă rătăciți!

## Centrul Europe Direct

Biblioteca găzduiește, de asemenea, [Centrul Europe Direct pentru județul Argeș](http://www.edarges.ro/www/). Dacă sunteți interesați să aflați mai multe despre cum funcționează Uniunea Europeană și despre proiectele sale, nu uitați să faceți o vizită la Centru. Aveți la dispoziție infografice în limbile română, engleză, franceză și germană care tratează subiecte de la agricultură la finanțe. 

# Filarmonica Pitești - "Prima vioară" a culturii argeșene

<img src="/images/stock/Philharmonic.jpg" alt="Pitești Philharmonic">

Concepută pentru a răspunde nevoilor îndrăgiților noștri melomani, Filarmonica Pitești este locul ideal pentru a te bucura de o seară de jazz sau de o simfonie clasică interpretată de o orchestră locală. Denumită de ziarul nostru local "prima vioară a culturii Argeșului", Filarmonica a înregistrat un real succes.

Rezervați-vă biletele prin intermediul [site-ului Filarmonicii](https://filarmonica-pitesti.ro/bilete/). Prețurile variază, de obicei, între 5 RON și 50 RON (1 - 10 euro).

# Biserica Domnească din Pitești - Biserica "Sfântu Gheorghe"

<img src="/images/stock/Princely Church.jpg" alt="'Sfântul Gheorghe' Church">

[Biserica](http://www.bisericadomneascapitesti.ro/) a fost ctitorită de Constantin Șerban, pe atunci domnitor al Munteniei, în anul 1656. Arhitectura sa unică reflectă rafinamentul artei construcțiilor ecleziastice din Țara Românească, la mijlocul secolului al XVII<sup>lea</sup>. Situată în Piața "Vasile Milea", în centrul orașului, a devenit unul dintre cele mai ușor de recunoscut repere ale Municipiului Pitești.
