# Start Guide

To make sure you have everything you need right from the start, we have prepared a small guide of living The City of Pitești.

<div class="boxes">
    <a href="/student-life/city/getting-around">Getting Around</a>
    <a href="/student-life/city/art-and-culture">Art and Culture</a>
    <a href="/student-life/city/leisure-and-events">Leisure and Events</a>
    <a href="/student-life/city/accommodation">Accommodation</a>
    <a href="/student-life/city/groceries-and-restaurants">Groceries and Restaurants</a>
    <a href="/student-life/city/health-and-emergency-services">Health and Emergency Services</a>
</div>

# Location

We are located [here](https://www.google.com/maps/place/Pitești), in Central-Southern Romania, at **100 km** from the capital, Bucharest. Our city is proud to host diverse communities, amongst which are Greek, French and Arab-speaking people.

# Weather

While the weather in Romania is quite hot during summer, with temperatures over **25ºC**, please make sure to bring a coat with you during winter. The fact that Pitești is situated between high hills and less than **75 km** from [Lake Vidraru](https://www.google.com/maps/place/Lake+Vidraru) in the Carpathian Mountains means that winters get quite cold, with an average temperature of **-2.4ºC** in January.
