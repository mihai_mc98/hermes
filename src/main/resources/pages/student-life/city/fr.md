# Guide

Pour vous assurer que vous avez tout ce dont vous avez besoin dès le début, nous avons préparé un petit guide pour vivre dans la ville de Pitești.

<div class="boxes">
    <a href="/student-life/city/getting-around">Se déplacer</a>
    <a href="/student-life/city/art-and-culture">Art et culture</a>
    <a href="/student-life/city/leisure-and-events">Loisirs et événements</a>
    <a href="/student-life/city/accommodation">Le logement</a>
    <a href="/student-life/city/groceries-and-restaurants">Les courses et les restaurants</a>
    <a href="/student-life/city/health-and-emergency-services">Santé et services d’urgence</a>
</div>

# Emplacement

Nous sommes situés [ici](https://www.google.com/maps/place/Pitești), dans le centre-sud de la Roumanie, à **100 km** de la capitale, Bucarest. Notre ville est fière d’accueillir des communautés variées, comme des Grecs, des Francophones et des Arabophones.

# Climat

Alors qu’il fait assez chaud pendant l’été en Roumanie, avec des températures supérieures à **25 °C**, assurez-vous d’emmener un manteau pour l’hiver. Le fait que la ville de Pitești est située parmi de hautes collines et à moins de **75 km** de [Lake Vidraru](https://www.google.com/maps/place/Lake+Vidraru) dans les Carpates signifie que les hivers sont assez froids, avec une température moyenne de **-2,4ºC** en janvier.
