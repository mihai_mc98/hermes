# Getting Around

Roaming around Pitești is easier than ever with our excellent bus network provided by **Publitrans 2000**. With affordable prices and a wide range of payment options (including on-the-bus card payment), our students can make the most of their stay in the City!
* [Bus Map](http://www.publitrans2000.ro/index.php?content=harta_toate_liniile)
* [Plan your trip](https://moovitapp.com/index/en/public_transit-lines-Pite%C8%99ti-3740-858122) with Moovit
* [Bus Fares](https://www.publitrans2000.ro/index.php?content=bilete_abonamente)
* Single Journey Ticket: **2 RON** (€0.40)

Pitești also offers alternative means of transport:
* [Taxis](https://www.taximetre.ro/taxi/pitesti/): **2-3 RON/km** (€0.40-€0.60 per km)
* Ride-Sharing Apps: [Bolt](https://bolt.eu/ro/cities/pitesti/) and [Uber](https://www.uber.com/global/en/cities/pitesti/)

# Reaching outside cities and beyond

If you ever get bored in Pitești, hop in a coach and arrive in our Capital in less than **90 minutes**! With highway and rail links to the rest of Romania, students can plan their next city-break to neighbouring cities and discover the wonderful sights our country has to offer.
* [Coaches](https://www.autogari.ro/en)
* [Trains](https://www.cfrcalatori.ro/en)
* [Henri Coandă Airport (OTP)](https://www.bucharestairports.ro/en)

## Nearby cities

| City                                                                                                                                     | Time (by car) | Time (by rail)             |
|------------------------------------------------------------------------------------------------------------------------------------------|---------------|----------------------------|
| Golești - home of [The Museum of Golești](https://www.muzeulgolesti.ro/video/33/MUSEUM_MEDIA_PRESENTATION/98)                            | 15 min        | 30 min                     |
| Curtea de Argeș                                                                                                                          | 45 min        | 1 hour                     |
| Bucharest – Capital of Romania                                                                                                           | 90 min        | 2 hours                    |
| Bran - home of the (in)famous Dracula's Castle                                                                                           | 2 hours       | 2 hours (+1 hour by coach) |
| Bâlea Lake and [The Transfăgărășan Motorway](https://www.facebook.com/watch/?v=1218473765190390) - one of Europe's most beautiful sights | 3 hours       | 3h 30min (by coach)        |
| Brașov                                                                                                                                   | 3 hours       | 5 hours                    |
| Constanța                                                                                                                                | 4 hours       | 5 hours 30 min             |
