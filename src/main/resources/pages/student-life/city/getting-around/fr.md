# Transport public 

Se déplacer à Pitești est plus facile que jamais avec notre excellent réseau de buses offert par **Publitrans 2000**. Avec des prix abordables et un large éventail d’options de paiement (y compris le paiement par carte dans le bus), nos étudiants peuvent profiter au maximum de leur séjour dans la ville !

* [Plan des bus](http://www.publitrans2000.ro/index.php?content=harta_toate_liniile)
* [Planifiez votre voyage](https://moovitapp.com/index/en/public_transit-lines-Pite%C8%99ti-3740-858122) avec Moovit
* [Tarifs bus](https://www.publitrans2000.ro/index.php?content=bilete_abonamente)
* Ticket 1 voyage: **2 RON** (0,40 €)

Pitești offre également des moyens de transport alternatifs :
* [Taxi](https://www.taximetre.ro/taxi/pitesti/): **2-3 RON/km** (0,40-0,60 €/km)
* Applications de ride-sharing: [Bolt](https://bolt.eu/ro/cities/pitesti/) et [Uber](https://www.uber.com/global/en/cities/pitesti/)

# Comment se rendre dans d’autres villes 

Si jamais vous vous ennuyez à Pitești, vous montez dans un car et vous êtes dans la capitale en moins de **90 minutes **! Ayant des connexions routières et ferroviaires avec le reste de la Roumanie, les étudiants peuvent planifier leur prochaine escapade citadine dans les villes voisines et découvrir les merveilleuses attractions touristiques que notre pays a à offrir.

* [Cars](https://www.autogari.ro/en)
* [Trains](https://www.cfrcalatori.ro/en)
* [Aéroport Henri Coandă (OTP)](https://www.bucharestairports.ro/en)

## Villes à proximité

| Ville                                                                                                                  | Durée (en voiture) | Durée (en train)           |
|------------------------------------------------------------------------------------------------------------------------|--------------------|----------------------------|
| Golești - siège de [Musée Golești](https://www.muzeulgolesti.ro/video/33/MUSEUM_MEDIA_PRESENTATION/98)                 | 15 min             | 30 min                     |
| Curtea de Argeș                                                                                                        | 45 min             | 1 heure                    |
| Bucarest – Capitale de la Roumanie                                                                                     | 90 min             | 2 heures                   |
| Bran - l’endroit où se trouve le célèbre et effrayant Château de Dracula                                               | 2 heures           | 2 heures (+1 heure de bus) |
| Lac Bâlea et [Transfăgărășan](https://www.facebook.com/watch/?v=1218473765190390) - l’un des plus beaux sites d’Europe | 3 heures           | 3h 30min (en bus)          |
| Brașov                                                                                                                 | 3 heures           | 5 heures                   |
| Constanța                                                                                                              | 4 heures           | 5 heures 30 min            |
