# Transportul în oraș

Să te plimbi prin Pitești este mai ușor ca niciodată cu rețeaua noastră excelentă de autobuze oferită de **Publitrans 2000**. Cu prețuri accesibile și o gamă largă de opțiuni de plată (inclusiv plata cu cardul în autobuz), studenții noștri pot profita la maximum de șederea în oraș!
* [Harta autobuzelor](http://www.publitrans2000.ro/index.php?content=harta_toate_liniile)
* [Planifică-ți călătoria](https://moovitapp.com/index/en/public_transit-lines-Pite%C8%99ti-3740-858122) cu Moovit
* [Tarife de autobuz](https://www.publitrans2000.ro/index.php?content=bilete_abonamente)
* Bilet pentru o singură călătorie: **2 RON** (0,40 €)

Municipiul Pitești oferă și mijloace de transport alternative:
* [Taxiuri](https://www.taximetre.ro/taxi/pitesti/): **2-3 RON/km** (0,40-0,60 €/km)
* Aplicații de ride-sharing: [Bolt](https://bolt.eu/ro/cities/pitesti/) și [Uber](https://www.uber.com/global/en/cities/pitesti/)

# Cum să ajungi în alte orașe și mai departe

Dacă te plictisești vreodată în Pitești, urcă-te într-un autocar și ajungi în capitală în mai puțin de **90 de minute**! Având legături rutiere și feroviare cu restul României, studenții își pot planifica următorul city-break în orașele învecinate și pot descoperi minunatele obiective turistice pe care țara noastră le are de oferit.
* [Autobuze](https://www.autogari.ro/en)
* [Trenuri](https://www.cfrcalatori.ro/en)
* [Aeroportul Henri Coandă (OTP)](https://www.bucharestairports.ro/en)

## Orașe apropiate

| Oraș                                                                                                                                                     | Timp (cu mașina) | Timp (cu trenul)            |
|----------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|-----------------------------|
| Golești - unde se află [Muzeul Golești](https://www.muzeulgolesti.ro/video/33/MUSEUM_MEDIA_PRESENTATION/98)                                              | 15 min           | 30 min                      |
| Curtea de Argeș                                                                                                                                          | 45 min           | 1 oră                       |
| București - Capitala României                                                                                                                            | 90 min           | 2 ore                       |
| Bran - locul unde se află celebrul și înspăimântătorul Castel al lui Dracula                                                                             | 2 ore            | 2 ore (+1 oră cu autocarul) |
| Bâlea Lac și [Șoseaua Transfăgărășan](https://www.facebook.com/watch/?v=1218473765190390) - unul dintre cele mai frumoase obiective turistice din Europa | 3 ore            | 3h 30min (cu autocarul)     |
| Brașov                                                                                                                                                   | 3 ore            | 5 ore                       |
| Constanța                                                                                                                                                | 4 ore            | 5 ore 30 min                |

