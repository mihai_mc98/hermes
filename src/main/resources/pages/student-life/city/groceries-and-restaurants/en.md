# Groceries

Apart from the many local shops, Pitești also houses popular Hypermarket chains that provide a variety of products:
* [Kaufland](https://www.kaufland.ro)
* [Auchan Bradu](https://www.auchan.ro) and [Auchan Bascov](https://www.auchan.ro)
* [Carrefour](https://carrefour.ro/) – located within Jupiter City
* Many [Lidl](https://www.lidl.ro) Shops

# Cafés, Pubs and Restaurants

## La Librairie Mon Cher

The best place to enjoy the best tea in the City. [La librairie Mon Cher](https://www.facebook.com/LibrarieMonCher/) is the secret hideout for all students wishing to enjoy a tranquil afternoon. The Brazilian Nut cocoa is a must-try!

## Bibliothèque Bistro

Dine in an interwar house right in the city centre. [Bibliothèque Bistro](https://www.facebook.com/BibliothequePitești) welcomes its guests from the first coffee of the morning until the end of the day. The menu is extensive and ranges from light appetizers to steaks, burgers and traditional cuisine.

## The Place (by Garden Pub)

Next to the Theatre, there is [The Place](https://www.theplacepitesti.ro/) – home of the best pancakes in the City! Pop in for a quick drink or get a reservation for one of the Pub's iconic nights. Either way, you are bound to have some fun!

## Mansion Pub
A simple, yet very hospitable restaurant close to the City Centre. [Mansion Pub](https://www.mansion-pub.ro/en/) is an elegant restaurant with traditional motifs where you can quench your thirst with a glass of wine or enjoy delicious local cuisine made just right! Even better, they deliver right to your doorstep, making it the perfect option for those evenings of late studying.

## Casa Argeșeană

Delicious Romanian cuisine right in the City Centre! The restaurant is well-known for its live-music events held on Thursdays, Fridays and Saturdays. With a wide range of spirits and great food, [Casa Argeșeană](http://casaargeseana.ro) is the perfect place for a mix of dancing and dining!

## Cornul Vânătorului

Despite being farther from the Centre than most entries in our list, [Cornul Vânătorului](https://cornulvanatorului.ro/cornul-vanatorului-2) is one of City's most iconic restaurants. It is famous for the tasty traditional and international cuisine, as well as for its exquisite wines from the Romanian vineyards. Surrounded by the Trivale Forest, guests will have a unique dining experience!

## Oriental Restaurants

* [The Orient Lounge](https://www.facebook.com/TheOrientLounge)
* [Sultana Restaurant](https://www.facebook.com/restaurantsultana/)
* [Nazar Restaurant](https://www.facebook.com/restaurantnazar)
* [The Great Chinese Restaurant](https://restaurantchinezescpitesti.ro/)
