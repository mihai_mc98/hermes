# Magasins alimentaires

En plus des nombreux magasins locaux, Pitești accueille également des chaînes d’hypermarchés populaires offrant une variété de produits :
* [Kaufland](https://www.kaufland.ro)
* [Auchan Bradu](https://www.auchan.ro) et [Auchan Bascov](https://www.auchan.ro)
* [Carrefour](https://carrefour.ro/) - situé dans l’enceinte de Jupiter City
* De nombreux magasins [Lidl](https://www.lidl.ro)

# Cafés, pubs et restaurants

## La Libraire Mon Cher

Le meilleur endroit pour profiter du meilleur thé de la ville. [La librairie Mon Cher](https://www.facebook.com/LibrarieMonCher/) C’est le refuge secret pour tous les étudiants qui veulent profiter d’un après-midi tranquille. Le cacao aux noix brésiliennes est un produit qu’il faut forcément essayer !

## Bibliothèque Bistro

Dînez dans une maison d’entre-deux-guerres en plein centre-ville. [Bibliothèque Bistro](https://www.facebook.com/BibliothequePitești) accueille ses invités du premier café du matin jusqu’à la fin de la journée. Le menu est riche et propose des entrées légères, des steaks, hamburgers et la cuisine traditionnelle.

## The Place (de Garden Pub)

À côté du théâtre, il y a [The Place](https://www.theplacepitesti.ro/) - la maison des meilleures crêpes de la ville ! Entrez pour un verre rapide ou faites une réservation pour l’une des soirées emblématiques du lieu. Quoi qu’il en soit, vous vous y sentirez certainement bien !

## Mansion Pub
Un restaurant simple, mais très hospitalier, proche du centre-ville. [Mansion Pub](https://www.mansion-pub.ro/en/) est un restaurant élégant avec des motifs traditionnels, où vous pourrez apaiser votre soif avec un verre de vin ou déguster la délicieuse cuisine locale ! De plus, il livre directement à votre domicile, ce qui en fait l’option parfaite pour les soirées où vous étudiez jusqu’à tard.

## Casa Argeșeană

La délicieuse cuisine roumaine en plein centre de la ville ! Le restaurant est célèbre pour ses événements musicaux live qui ont lieu les jeudis, vendredis et samedis. Avec une large gamme de boissons et son excellente cuisine, [Casa Argeșeană](http://casaargeseana.ro) est l’endroit idéal pour une combinaison de danse et de gastronomie !

## Cornul Vânătorului

Bien que plus éloigné du centre que la plupart des restaurants sur notre liste, [Cornul Vânătorului](https://cornulvanatorului.ro/cornul-vanatorului-2) est l’un des restaurants les plus emblématiques de la ville. Il est célèbre pour sa savoureuse cuisine traditionnelle et internationale, ainsi que pour ses vins exquis des vignobles roumains. Entourés par la forêt de Trivale, les clients vivront une expérience culinaire unique !

## Restaurants orientaux

* [The Orient Lounge](https://www.facebook.com/TheOrientLounge)
* [Sultana Restaurant](https://www.facebook.com/restaurantsultana/)
* [Nazar Restaurant](https://www.facebook.com/restaurantnazar)
* [The Great Chinese Restaurant](https://restaurantchinezescpitesti.ro/)
