# Magazine alimentare

În afară de numeroasele magazine locale, Municipiul Pitești găzduiește și lanțuri populare de hipermarketuri care oferă o varietate de produse:
* [Kaufland](https://www.kaufland.ro)
* [Auchan Bradu](https://www.auchan.ro) și [Auchan Bascov](https://www.auchan.ro)
* [Carrefour](https://carrefour.ro/) - situat în incinta Jupiter City
* Numeroase magazine [Lidl](https://www.lidl.ro)

# Cafenele, pub-uri și restaurante

## La Librairie Mon Cher

Cel mai bun loc pentru a savura cel mai bun ceai din oraș. [La Librairie Mon Cher](https://www.facebook.com/LibrarieMonCher/) este refugiul secret pentru toți studenții care doresc să se bucure de o după-amiază liniștită. Cacaua cu nuci braziliană este un produs care trebuie neapărat încercat!

## Bibliothèque Bistro

Luați masa într-o casă interbelică chiar în centrul orașului. [Bibliothèque Bistro](https://www.facebook.com/BibliothequePitesti) își întâmpină oaspeții de la prima cafea de dimineață până la sfârșitul zilei. Meniul este vast și variază de la aperitive ușoare până la fripturi, burgeri și bucătărie tradițională.

## The Place ( de la Garden Pub)

Lângă Teatru, se află [The Place](https://www.theplacepitesti.ro/) - casa celor mai bune clătite din oraș! Intrați pentru o băutură rapidă sau faceți o rezervare pentru una dintre serile emblematice ale localului. Oricum ar fi, cu siguranță vă veți simți bine!

## Mansion Pub
Un restaurant simplu, dar foarte ospitalier, aproape de centrul orașului. [Mansion Pub](https://www.mansion-pub.ro/en/) este un restaurant elegant, cu motive tradiționale, unde vă puteți potoli setea cu un pahar de vin sau vă puteți bucura de preparate delicioase din bucătăria locală, făcute ca la carte! Ba mai mult, livrează direct la domiciliu, ceea ce îl face opțiunea perfectă pentru acele seri în care studiați până târziu.

## Casa Argeșeană

Bucătărie românească delicioasă chiar în centrul orașului! Restaurantul este renumit pentru evenimentele cu muzică live organizate joia, vinerea și sâmbăta. Cu o gamă largă de băuturi spirtoase și mâncare excelentă, [Casa Argeșeană](http://casaargeseana.ro) este locul perfect pentru o combinație de dans și gastronomie!

## Cornul Vânătorului

În ciuda faptului că este mai departe de centru decât majoritatea localurilor din lista noastră, [Cornul Vânătorului](https://cornulvanatorului.ro/cornul-vanatorului-2) este unul dintre cele mai emblematice restaurante din oraș. Este renumit pentru gustoasa bucătărie tradițională și internațională, precum și pentru vinurile sale rafinate din podgoriile românești. Înconjurați de Pădurea Trivale, oaspeții vor avea parte de o experiență culinară unică!

## Restaurante orientale

* [The Orient Lounge](https://www.facebook.com/TheOrientLounge)
* [Sultana Restaurant](https://www.facebook.com/restaurantsultana/)
* [Nazar Restaurant](https://www.facebook.com/restaurantnazar)
* [The Great Chinese Restaurant](https://restaurantchinezescpitesti.ro/)
