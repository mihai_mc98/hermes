<div class="info-box">

## Emergencies

<br>

<span class="note-important">Do **NOT** delay! Call **[112](tel:112)** for medical **emergencies**!</span>

</div>

# Health Start Guide

Your health is very important to us, so we have prepared a small guide for what to do when you are feeling unwell.

While we wish our students never have to pay a visit in the Hospital, sometimes this becomes necessary. If you are admitted into the hospital, please let us know as soon as possible, so we can inform your teachers and/or any other relevant parties.

# Hospitals

* [Argeș County Hospital for Emergencies](http://www.sjupitesti.ro)
* [Paediatric Hospital](http://www.pediatriepitesti.ro/)
* [Nicolae Bălcescu Hospital for Infectious Diseases](http://sjupitesti.ro/index.php/boli-infectioase)
* [Dr. Ion Jianu Military Hospital for Emergencies](https://smupitesti.org/)

# Private Clinics

The following clinics belong to the Private Health sector and may not be covered by your Policy. Please check with your Health Insurance provider first, or you may incur out-of-pocket expenses.

* [Muntenia Hospital](https://www.munteniahospital.ro/)
* [Regina Maria Polyclinics](https://www.reginamaria.ro/clinici/pitesti)
* [Sanovital Centre](https://sanovital.ro/)
* [Santé Clinic](https://www.clinica-sante.ro/sante-pitesti-eliade/)
* [Medas](https://www.med-as.ro)
* [Solomed](https://www.solomed.ro/)
* [Synevo](https://www.synevo.ro/locatii/centru-recoltare-pitesti-2-teiuleanu/)
