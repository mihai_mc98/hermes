<div class="info-box">

## Urgences

<br>

<span class="note-important"> **N’attendez plus** ! Appelez le **[112](tel:112)** pour les **urgences médicales** !</span>

</div>

# Guide d’initiation pour votre santé

Votre santé est très importante pour nous, nous avons donc préparé un petit guide sur ce qu’il faut faire lorsque vous ne vous sentez pas bien.

Bien que nous voulions que nos étudiants n’aient jamais besoin de se rendre à l’hôpital, cela devient parfois nécessaire. Si vous êtes hospitalisé, veuillez nous en informer dès que possible afin que nous puissions informer vos enseignants et/ou toute autre personne intéressée.

# Hôpitaux

* [Hôpital d’urgence départemental Argeș](http://www.sjupitesti.ro)
* [Hôpital de Pédiatrie](http://www.pediatriepitesti.ro/)
* [Hôpital Maladies Infectieuses Nicolae Bălcescu](http://sjupitesti.ro/index.php/boli-infectioase)
* [Hôpital Militaire d’Urgence Dr. Ion Jianu](https://smupitesti.org/)

# Cliniques privées

Ces cliniques appartiennent au secteur privé de la santé et il est possible que votre assurance n’en couvre pas les frais. Veuillez d’abord vérifier auprès de votre fournisseur d’assurance maladie, sinon vous risquez de supporter les coûts de votre propre poche.

* [Hôpital Muntenia](https://www.munteniahospital.ro/)
* [Policlinique Regina Maria](https://www.reginamaria.ro/clinici/pitesti)
* [Centre Sanovital](https://sanovital.ro/)
* [Clinique Santé](https://www.clinica-sante.ro/sante-pitesti-eliade/)
* [Medas](https://www.med-as.ro)
* [Solomed](https://www.solomed.ro/)
* [Synevo](https://www.synevo.ro/locatii/centru-recoltare-pitesti-2-teiuleanu/)
