<div class="info-box">

## Urgențe

<br>

<span class="note-important">**NU** amânați! Sunați la **[112](tel:112)** pentru **urgențe medicale**!</span>

</div>

# Ghid de inițiere privind sănătatea dumneavoastră

Sănătatea dvs. este foarte importantă pentru noi, așa că am pregătit un mic ghid pentru ce trebuie să faceți atunci când nu vă simțiți bine.

Deși ne dorim ca studenții noștri să nu fie niciodată nevoiți să facă o vizită la spital, uneori acest lucru devine necesar. Dacă sunteți internat în spital, vă rugăm să ne anunțați cât mai curând posibil, astfel încât să vă putem informa profesorii și/sau orice alte persoane interesate.

# Spitale

* [Spitalul Județean de Urgență Argeș](http://www.sjupitesti.ro)
* [Spitalul de Pediatrie](http://www.pediatriepitesti.ro/)
* [Spitalul de Boli Infecțioase Nicolae Bălcescu](http://sjupitesti.ro/index.php/boli-infectioase)
* [Spitalul Militar de Urgență Dr. Ion Jianu](https://smupitesti.org/)

# Clinici private

Următoarele clinici aparțin sectorului privat de sănătate și este posibil să nu fie acoperite de polița dumneavoastră de asigurări. Vă rugăm să verificați mai întâi cu furnizorul dumneavoastră de asigurări de sănătate, altfel s-ar putea să suportați costurile din propriul buzunar.

* [Spitalul Muntenia](https://www.munteniahospital.ro/)
* [Policlinica Regina Maria](https://www.reginamaria.ro/clinici/pitesti)
* [Centrul Sanovital](https://sanovital.ro/)
* [Clinica Santé](https://www.clinica-sante.ro/sante-pitesti-eliade/)
* [Medas](https://www.med-as.ro)
* [Solomed](https://www.solomed.ro/)
* [Synevo](https://www.synevo.ro/locatii/centru-recoltare-pitesti-2-teiuleanu/)
