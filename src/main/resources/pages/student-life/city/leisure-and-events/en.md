Taking a break from time to time is very much encouraged, so why not use the time to treat yourself a little ? From Spa Centres to our beautiful Trivale Forest, Pitești will surprise you with its beautiful landscapes and history.

<table-of-contents depth="1" lang="en"></table-of-contents>

# Events

The [Town Hall](http://www.primariapitesti.ro/) organises a number of events throughout the year for its citizens. The most popular ones are:
* **The Tulip Symphony** each April/May
* **Pitești Days** in May
* **Autumn Fair** in October
* **Trivale Fest** in October
* **"Zavaidoc" National Festival of Old Fiddle Music** in November
* **"Carpathians" International Folklore Festival** in December

# Discover Nature

* [Trivale Forest](https://www.youtube.com/watch?v=CaWvLUdk6X4) – our biggest park
* [Argeș Meadows Park](https://www.youtube.com/watch?v=1DqJZGWTpvI)
* [Ștrand Park](https://www.youtube.com/watch?v=dPQB6n3BrM8)
* [Expo Park](https://www.youtube.com/watch?v=jP2h-pa_SnE)
* [Pitești Zoo](https://www.youtube.com/watch?v=JuwXQrORabI) – Tickets are 4 RON (€0.80) for students

# Gym, Sports and Spa Centres

* [Trivale Swimming Pool](https://www.facebook.com/terasamagic.trivale/)
* [Magic Swimming pool](https://www.magicgt.ro/piscina-coffe-lounge-magic-gt-pitesti/)
* Shaker Beach Budeasa – a beach with its very own private pool, seating area with canopies, massage and jacuzzi
* [Andaluz Riding Centre](https://www.facebook.com/Andaluz.echitatie)
* [Hall of Sports](https://www.facebook.com/centruldetineret/)
* [Nicolae Dobrin Stadium](https://www.facebook.com/StadionNicolaeDobrin)
* [Victoria Spa & Wellness](https://victoriahotel.ro/spa/)
* [Verity Spa, Wellness & Sport](https://www.facebook.com/VeritySpaPitești/)
* [Impact Fitness Studio](https://www.facebook.com/impactfitness2017/)
* [Xtreme Fitness Pitești](https://www.xtremefitness.ro/abonamente/)

# Cinema

* [Cinema City](https://www.cinemacity.ro)
* [Cinema Trivale](https://www.cinematrivale.ro)
* [Sebastian Papaiani Cinema](https://www.facebook.com/Cinema-Sebastian-Papaiani-555243477831357/)

# Going Shopping

* [Jupiter City Shopping Centre](https://www.jupitercity.ro/) – the largest mall in the County, where every visit turns into a complete experience, an ideal destination for shopping!
* [VIVO Pitești Mall](https://vivo-shopping.com/en/pitesti) – the host of popular clothing brands, with direct access to the supermarket, pharmacies, IT equipment and cafés
* [Jumbo](https://www.facebook.com/jumbopitesti/) – The Toy Land
* [Mihai Eminescu Bookshop](http://librariaeminescupitesti.ro/) – the home of essential goods for students
* [Cărturești](https://carturesti.ro/blog/librarie/carturesti-pitesti/) and [Diverta](https://www.dol.ro) – the go-to destinations for finding that perfect book 
