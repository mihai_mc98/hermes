Une pause faite de temps en temps fait des miracles, alors, pourquoi ne pas utiliser ce temps pour vous détendre un peu ? Avec les centres Spa ou notre belle Forêt Trivale, Pitești vous surprendra par ses beaux paysages et par son histoire.

<table-of-contents depth="1" lang="fr"></table-of-contents>

# Événements

[Mairie](http://www.primariapitesti.ro/) organise une série d’événements tout au long de l’année pour ses citoyens. Les plus populaires sont :
* **Symphonie des Tulipes** chaque année en avril/mai.
* **Journées de Piteşti** en mai
* **Foire d’automne** en octobre
* **Trivale Fest** en octobre
* **Festival National de Musique Ancienne pour Violon "Zavaidoc "** en novembre
* **Festival International de Folklore "Carpați"** en décembre

# Découvrez la nature

* [Forêt Trivale](https://www.youtube.com/watch?v=CaWvLUdk6X4) - notre plus grand parc
* [Parc Lunca Argeșului](https://www.youtube.com/watch?v=1DqJZGWTpvI)
* [Parc Ștrand](https://www.youtube.com/watch?v=dPQB6n3BrM8)
* [Expo Park](https://www.youtube.com/watch?v=jP2h-pa_SnE)
* [Zoo de Piteşti](https://www.youtube.com/watch?v=JuwXQrORabI) - Les billets coûtent 4 RON (0,80 €) pour les élèves et les étudiants

# Centres de gymnastique, sports et spa

* [Trivale Swimming Pool](https://www.facebook.com/terasamagic.trivale/)
* [Magic Swimming pool](https://www.magicgt.ro/piscina-coffe-lounge-magic-gt-pitesti/)
* Shaker Beach Budeasa - une plage avec sa propre piscine privée, zone détente avec lits baldaquins, massage et jacuzzi
* [Andaluz Riding Centre](https://www.facebook.com/Andaluz.echitatie)
* [Salle de sport](https://www.facebook.com/centruldetineret/)
* [Stade Nicolae Dobrin](https://www.facebook.com/StadionNicolaeDobrin)
* [Victoria Spa & Wellness](https://victoriahotel.ro/spa/)
* [Verity Spa, Wellness & Sport](https://www.facebook.com/VeritySpaPitești/)
* [Impact Fitness Studio](https://www.facebook.com/impactfitness2017/)
* [Xtreme Fitness Pitești](https://www.xtremefitness.ro/abonamente/)

# Cinéma
* [Cinéma City](https://www.cinemacity.ro)
* [Cinéma Trivale](https://www.cinematrivale.ro)
* [Sebastian Papaiani Cinema](https://www.facebook.com/Cinema-Sebastian-Papaiani-555243477831357/)

# Achats

* [Jupiter City Shopping Centre](https://www.jupitercity.ro/) - le plus grand centre commercial du département, où chaque visite se transforme dans une expérience complète, une destination idéale pour faire du shopping !
* [VIVO Pitești Mall](https://vivo-shopping.com/en/pitesti) - rassemble les marques de vêtements populaires avec un accès direct au supermarché, aux pharmacies, à l’équipement informatique et aux cafés
* [Jumbo](https://www.facebook.com/jumbopitesti/) - Le royaume des jouets.
* [Librairie Mihai Eminescu](http://librariaeminescupitesti.ro/) - le coin des produits essentiels pour les étudiants
* [Cărturești](https://carturesti.ro/blog/librarie/carturesti-pitesti/) et [Diverta](https://www.dol.ro) - les destinations préférées pour trouver le livre parfait
