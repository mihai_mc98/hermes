Este foarte binevenită o pauză din când în când, așa că de ce să nu folosiți timpul pentru a vă răsfăța puțin? De la centrele Spa la frumoasa noastră pădure Trivale, Municipiul Pitești vă va surprinde cu peisajele sale frumoase și cu istoria sa.

<table-of-contents depth="1" lang="ro"></table-of-contents>

# Evenimente

[Primăria](http://www.primariapitesti.ro/) organizează o serie de evenimente pe tot parcursul anului pentru cetățenii săi. Cele mai populare sunt:
* **Simfonia Lalelelor** în fiecare an în aprilie/mai.
* **Zilele Piteștiului** în luna mai
* **Târgul de Toamnă** în octombrie
* **Trivale Fest** în octombrie
* **Festivalul Național de Muzică Veche de Vioară "Zavaidoc"** în noiembrie
* **Festivalul Internațional de Folclor "Carpați"** în decembrie

# Descoperă natura

* [Pădurea Trivale](https://www.youtube.com/watch?v=CaWvLUdk6X4) - cel mai mare parc al nostru
* [Parcul Lunca Argeșului](https://www.youtube.com/watch?v=1DqJZGWTpvI)
* [Parcul Ștrand](https://www.youtube.com/watch?v=dPQB6n3BrM8)
* [Expo Park](https://www.youtube.com/watch?v=jP2h-pa_SnE)
* [Grădina Zoologică din Pitești](https://www.youtube.com/watch?v=JuwXQrORabI) - Biletele costă 4 RON (0,80 €) pentru elevi și studenți

# Centre de gimnastică, sport și spa

* [Trivale Swimming Pool](https://www.facebook.com/terasamagic.trivale/)
* [Magic Swimming pool](https://www.magicgt.ro/piscina-coffe-lounge-magic-gt-pitesti/)
* Shaker Beach Budeasa - o plajă cu propria piscină privată, zonă de relaxare cu baldachine, masaj și jacuzzi
* [Andaluz Riding Centre](https://www.facebook.com/Andaluz.echitatie)
* [Sala de sport](https://www.facebook.com/centruldetineret/)
* [Stadionul Nicolae Dobrin](https://www.facebook.com/StadionNicolaeDobrin)
* [Victoria Spa & Wellness](https://victoriahotel.ro/spa/)
* [Verity Spa, Wellness & Sport](https://www.facebook.com/VeritySpaPitesti/)
* [Impact Fitness Studio](https://www.facebook.com/impactfitness2017/)
* [Xtreme Fitness Pitești](https://www.xtremefitness.ro/abonamente/)

# Cinema
* [Cinema City](https://www.cinemacity.ro)
* [Cinema Trivale](https://www.cinematrivale.ro)
* [Sebastian Papaiani Cinema](https://www.facebook.com/Cinema-Sebastian-Papaiani-555243477831357/)

# La cumpărături

* [Jupiter City Shopping Centre](https://www.jupitercity.ro/) - cel mai mare mall din județ, unde fiecare vizită se transformă într-o experiență completă, o destinație ideală pentru cumpărături!
* [VIVO Pitești Mall](https://vivo-shopping.com/en/pitesti) - gazda brandurilor populare de îmbrăcăminte, cu acces direct la supermarket, farmacii, echipamente IT și cafenele
* [Jumbo](https://www.facebook.com/jumbopitesti/) - Tărâmul jucăriilor
* [Librăria Mihai Eminescu](http://librariaeminescupitesti.ro/) - gazda produselor esențiale pentru studenți
* [Cărturești](https://carturesti.ro/blog/librarie/carturesti-pitesti/) și [Diverta](https://www.dol.ro) - destinațiile preferate pentru a găsi cartea perfectă 
