# Ghid de ședere

Pentru a fi siguri că aveți tot ce vă trebuie încă de la început, am pregătit un mic ghid de ședere în orașul Pitești.

<div class="boxes">
    <a href="/student-life/city/getting-around">Transportul în oraș</a>
    <a href="/student-life/city/art-and-culture">Artă și cultură</a>
    <a href="/student-life/city/leisure-and-events">Agrement și evenimente</a>
    <a href="/student-life/city/accommodation">Cazare</a>
    <a href="/student-life/city/groceries-and-restaurants">Magazine alimentare și restaurante</a>
    <a href="/student-life/city/health-and-emergency-services">Servicii de sănătate și de urgență</a>
</div>

# Locație

Ne aflăm [aici](https://www.google.com/maps/place/Pitești), în zona central-sudică a României, la **100km** de capitala București. Orașul nostru se bucură de o comunitate diversă, printre care se numără persoane vorbitoare de limbă greacă, franceză și arabă.

# Vremea

În timp ce vremea în România este destul de caldă în timpul verii, cu temperaturi de peste **25ºC**, vă rugăm să vă asigurați că aveți cu dumneavoastră un palton în timpul iernii. Poziționarea între dealuri înalte și la mai puțin de **75 km** de [Lacul Vidraru](https://www.google.com/maps/place/Lake+Vidraru) din Munții Carpați face ca iernile în Pitești să fie destul de reci, cu o temperatură medie de **-2,4ºC** în ianuarie.
