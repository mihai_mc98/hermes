"use strict";

export {closeBanner}

import {setCookie} from "./cookies.js";

function closeBanner(banner_id, cookie_value) {

    // Get the element
    const banner = document.getElementById(banner_id);

    // Set cookie so that this is never shown again
    setCookie(banner_id, cookie_value, 365);

    // Hide the button
    banner.style.display = "none";
}