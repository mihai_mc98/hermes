"use strict";

export {nextSlide, prevSlide, slidesDispatch}

// TODO – THIS SHOULD BE A CYCLIC ARRAY !!!
let slides;
let navDots;
let currentSlideIndex = 0;
let newSlideIndex = currentSlideIndex + 1;
const transitionDelay = 500;
const transitionFunction = "ease-out";
const nextSlideTimeout = 10000;

function nextSlide() {
    showSlides(1);
}

function prevSlide() {
    showSlides(-1);
}

// TODO – Refactor this
// Direction == 1 => Forward/next
// Direction == -1 => Backwards/previous
function showSlides(direction = 1) {

    newSlideIndex = (((currentSlideIndex + direction) % slides.length) + slides.length) % slides.length;

    if(newSlideIndex !== currentSlideIndex) {

        // Transition slides on transform
        slides[currentSlideIndex].style.transition = `transform ${transitionDelay}ms ${transitionFunction}`;
        slides[newSlideIndex].style.transition = `transform ${transitionDelay}ms ${transitionFunction}`;

        // Position the slide accordingly
        slides[newSlideIndex].style.right = `${-direction * 100}%`;

        // Activate the Slide
        slides[newSlideIndex].classList.add("carousel-slide-active");

        /*
         * NOTE – So that the transition works, we must trigger a REFLOW.
         *        This is because the next/previous slide is no longer "display: none"
         *
         *        Some useful references:
         *        - https://stackoverflow.com/questions/49269614/why-does-reflow-need-to-be-triggered-for-css-transitions
         *        - https://gist.github.com/paulirish/5d52fb081b3570c81e3a
         *        - https://stackoverflow.com/questions/8210560/css-transitions-do-not-work-when-assigned-trough-javascript
         */
        void slides[newSlideIndex].offsetHeight;

        // Translate the slides (+ implicit transition)
        slides[currentSlideIndex].style.transform = `translateX(${-direction * 100}%)`;
        slides[newSlideIndex].style.transform = `translateX(${-direction * 100}%)`;

        // Because "ontransitionend" is NOT reliable, set a delay after the transition ends
        setTimeout(function () {

            // Remove transitions
            slides[currentSlideIndex].style.removeProperty("transition");
            slides[newSlideIndex].style.removeProperty("transition");

            // Remove artificial offsets
            slides[newSlideIndex].style.removeProperty("right");
            slides[newSlideIndex].style.removeProperty("transform");
            slides[currentSlideIndex].style.removeProperty("transform");

            // Make current Slide Inactive
            slides[currentSlideIndex].classList.remove("carousel-slide-active");

            // Update navDots
            navDots[newSlideIndex].classList.remove("far")
            navDots[newSlideIndex].classList.add("fas");

            navDots[currentSlideIndex].classList.remove("fas");
            navDots[currentSlideIndex].classList.add("far");

            currentSlideIndex = newSlideIndex;
        }, transitionDelay);
    }
}

// TODO – Needs to be refactored into something better than this hack!
function slidesDispatch(slideNumber) {
    if((slides.length + slideNumber - currentSlideIndex) % slides.length === 1)
        showSlides(+1);
    else
        showSlides(-1);
}

// function main() {

// When the DOM is loaded and parsed
document.addEventListener("DOMContentLoaded", function () {

    // Prevent the default behaviour of the carousel's navigation links (arrows and dots)
    const navigationLinks = document.querySelectorAll(".carousel-navigation-arrows, .carousel-navigation-dots-dot");
    for (let navigationLink of navigationLinks) {
        navigationLink.addEventListener("click", function (e) {
            e.preventDefault();
        }, false);
    }

    // Get all the slides and navDots
    slides = document.getElementsByClassName("carousel-slide");
    navDots = document.querySelectorAll(".carousel-navigation-dots-dot > i");
});

// Auto-Transition Carousel
// FIXME – This is a dirty fix – `slides` should be used instead !!!
if(document.getElementsByClassName("carousel-slide").length !== 0)
    setInterval(nextSlide, nextSlideTimeout);

// }