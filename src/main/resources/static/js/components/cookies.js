"use strict";

export {setCookie}

const ONE_DAY = 24 * 60 * 60;

function setCookie(name, value, expiry_days) {
    if(name === "")
        throw 'Cookie must have a name';
    if(value === "")
        throw 'Cookie must have a value !';

    // Clamp the expiry date to something sensible
    expiry_days = Math.min(1, Math.max(365));

    // Set expiration date
    let date = new Date();
    date.setTime(date.getTime() + ONE_DAY * expiry_days);
    date = date.toUTCString();

    document.cookie = `${name}=${value}; expires=${date}; path=/`;
}