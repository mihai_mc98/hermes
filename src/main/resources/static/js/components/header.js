"use strict";

export {toggleDrawer, toggleSubmenu}

let isDrawerOpened = false;
let bodyChildren;

function toggleDrawer() {
    const css_class = "drawer-open";
    if(isDrawerOpened)
        bodyChildren.forEach(child => child.classList.remove(css_class));
    else
        bodyChildren.forEach(child => child.classList.add(css_class));

    // Update the state
    isDrawerOpened = !isDrawerOpened;
}

function toggleSubmenu(button) {

    const button_class = "trigger_active";
    const dropdown_class = "drawer_nav_item_dropdown_open";

    const dropdown = button.parentElement.parentElement.children[1];

    // Check if it already opened
    if([].slice.call(button.classList).includes(button_class)) {
        dropdown.classList.remove(dropdown_class);
        button.classList.remove(button_class);
    }
    else {
        dropdown.classList.add(dropdown_class);
        button.classList.add(button_class);
    }
}

// When the DOM is loaded and parsed
document.addEventListener("DOMContentLoaded", function () {

    // Get all body children (excluding <noscript>)
    bodyChildren = [].slice.call(document.body.children).filter((child => child.tagName !== "NOSCRIPT"));
});