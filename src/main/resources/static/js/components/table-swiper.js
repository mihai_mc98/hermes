"use strict";

const table_swiper = true;
export {table_swiper}

document.addEventListener("DOMContentLoaded", function () {
    const tables = document.getElementsByClassName("table_wrapper");
    for(let table of tables)
        // Only enable the `table_swiper` if the table overflowed
        if(table.offsetWidth !== table.scrollWidth) {

            // Colour first row's border
            let first_row = table.getElementsByTagName("tr")[0];
            for(let cell of first_row.children)
                cell.classList.add("table_swiper_enabled");

            // Enable the slider
            let table_swiper = table.getElementsByClassName("table_swiper")[0];
            table_swiper.classList.add("table_swiper_enabled");
            table_swiper.style.transform = `translateY(calc(${first_row.offsetHeight}px - 50%))`;
        }
});