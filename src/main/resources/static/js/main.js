"use strict";

import {prevSlide, nextSlide, slidesDispatch} from "./components/carousel.js";
import {toggleDrawer, toggleSubmenu} from "./components/header.js";
import {closeBanner} from "./components/banner.js";
import {table_swiper} from "./components/table-swiper.js";

// FIXME – These ought to be done in an other way !!!
window.nextSlide = nextSlide;
window.prevSlide = prevSlide;
window.slidesDispatch = slidesDispatch;
window.toggleDrawer = toggleDrawer;
window.toggleSubmenu = toggleSubmenu;
window.closeBanner = closeBanner;

/**
 * For all the HTML elements in the DOM with class name `this-year`, set the current year
 *
 * FIXME: this needs to be somewhere else, but it will do for now
 */
(() => { // Immediately-Invoked Function Expression (IIFE)

    const date = new Date();
    const calendar_year = date.getFullYear();
    [].slice.call(document.getElementsByClassName('this-year'))
      .forEach(span => span.textContent = calendar_year.toString());

    // End of current academic year (starts in October)
    const academic_year = date.getMonth() < 9 ? calendar_year : calendar_year + 1;
    [].slice.call(document.getElementsByClassName('academic-year'))
        .forEach(span => span.textContent = academic_year);

})();

/* Only register a service worker if it's supported */
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js');
    });
}