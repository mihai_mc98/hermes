module.exports = {
	globDirectory: 'target/classes/static',
	globPatterns: [
		'**/*.{json,md,css,jpg,ico,svg,png,js,html,scss}'
	],
	swDest: 'target/classes/static/service-worker.js',
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/,
		/^source/
	]
};